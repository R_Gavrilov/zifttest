package tests;

import lib.CoreTestCase;
import lib.CoreTestCaseForParallelSessions;
import lib.VideoRecorder;
import lib.ui.factories.*;
import org.junit.Test;
import lib.ui.*;

public class ScreenTimeTests extends CoreTestCaseForParallelSessions {

    @Test
    public void testScreenTimeToday() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_third);
            CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
            ChildProfilePageObject.setDefaultScreenTimeValue();
            String screenTimeLeftText = ChildProfilePageObject.getScreenTimeLeftElementText();
             assertEquals("we see unexpected value in screenTimeLeft",
                    "\u200BUnlimited", screenTimeLeftText);
            ChildProfilePageObject.editScreenTimeClick();
            String screenTimeTodayLeftElementText = ScreenTimeManagementPageObject.getScreenTimeTodayLeftElementText();
            assertEquals("we see unexpected value in screenTimeTodayLeft",
                    "\u200BUnlimited", screenTimeTodayLeftElementText);
            ScreenTimeManagementPageObject.checkThatPlusMinusButtonsDisabled();
            ScreenTimeManagementPageObject.unlimCheckBoxClick();
            ScreenTimeManagementPageObject.waitForCheckboxUnlimitedOff();
            Integer expectedScreenTimeTodayLeftInSeconds1 =
                    ScreenTimeManagementPageObject.expectedScreenTimeTodayLeftInSeconds();
            Integer actualScreenTimeTodayLeftInSeconds1 =
                    ScreenTimeManagementPageObject.actualSecondsOfScreenTimeLeft();
            assertEquals("we see unexpected value in screenTimeTodayLeft",
                    expectedScreenTimeTodayLeftInSeconds1, actualScreenTimeTodayLeftInSeconds1);
            ScreenTimeManagementPageObject.checkThatPlusMinusButtonsEnabled();
            String textBetweenPlusMinus = ScreenTimeManagementPageObject.getOverrideForTodayText();
            assertEquals("we see unexpected value in screenTimeTodayLeft",
                    "08h 00m", textBetweenPlusMinus);
            ScreenTimeManagementPageObject.initPlusMinusElement();
            ScreenTimeManagementPageObject.initPlusMinusButtonClick();
            ScreenTimeManagementPageObject.initPlusMinusButtonClick();
            CommonNavigationUI.initBackClick();
            String screenTimeLeftText1 = ChildProfilePageObject.getScreenTimeLeftElementText();
            assertEquals("we see unexpected value in screenTimeLeft",
                    "07h 30m", screenTimeLeftText1);
            CommonNavigationUI.initBackClick();
            FeedPageObject.initTongueTopClick();
            FamilyOverviewPageObject FamilyOverviewPageObject = FamilyOverviewPageObjectFactory.get(driver_third);
            String screenTimeLeftText2 = FamilyOverviewPageObject.getScreenTimeLeftElementText();
            assertEquals("we see unexpected value in screenTimeLeft",
                    "07h 30m", screenTimeLeftText2);
            FamilyOverviewPageObject.initAvatarClick();
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject.unlimCheckBoxClick();
            ScreenTimeManagementPageObject.waitForCheckboxUnlimitedOn();
            String screenTimeTodayLeftElementText2 = ScreenTimeManagementPageObject.getScreenTimeTodayLeftElementText();
            assertEquals("we see unexpected value in screenTimeTodayLeft",
                    "\u200BUnlimited", screenTimeTodayLeftElementText2);
            ScreenTimeManagementPageObject.checkThatPlusMinusButtonsDisabled();
            CommonNavigationUI.initBackClick();
            String screenTimeLeftText3 = ChildProfilePageObject.getScreenTimeLeftElementText();
            assertEquals("we see unexpected text in screenTimeLeft",
                    "\u200BUnlimited", screenTimeLeftText3);
            CommonNavigationUI.initBackClick();
            String screenTimeLeftText4 = FamilyOverviewPageObject.getScreenTimeLeftElementText();
            assertEquals("we see unexpected text in screenTimeLeft",
                    "\u200BUnlimited", screenTimeLeftText4);
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test
    public void testScreenTime() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
//            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
//            CommonNavigationUI.waitUntilBlockButtonEnabled("Block");
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ChildProfilePageObject.waitForDataLoading();
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
            CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_third);
            ScreenTimeManagementPageObject.openScreenTimeLimits();
            ScreenTimeLimitsPageObject ScreenTimeLimitsPageObject = ScreenTimeLimitsPageObjectFactory.get(driver_third);
            ScreenTimeLimitsPageObject.setUnlimitedScreenTimeForWeek();
            String textInSpecificTimeElement = ScreenTimeLimitsPageObject.getWeeklyScreenTimeElementText();
            assertEquals("we see unexpected text in weeklyScreenTimeValue",
                    "Unlimited", textInSpecificTimeElement);
            ScreenTimeLimitsPageObject.verifyThatNumPadElementDisabled("1");
            ScreenTimeLimitsPageObject.verifyThatNumPadElementDisabled("9");
            ScreenTimeLimitsPageObject.initUnlimCheckBoxClick();
            ScreenTimeLimitsPageObject.waitForUnlimitedCheckboxAttributeChangeToFalse();
            ScreenTimeLimitsPageObject.initSpecificTimeElement();
            String textInSpecificTimeElement1 = ScreenTimeLimitsPageObject.getWeeklyScreenTimeElementText();
            assertEquals("we see unexpected text in weeklyScreenTimeValue",
                    "08h 00m", textInSpecificTimeElement1);
            ScreenTimeLimitsPageObject.verifyThatNumPadElementEnabled("2");
            ScreenTimeLimitsPageObject.verifyThatNumPadElementEnabled("0");
            ScreenTimeLimitsPageObject.enterValue010();

            ScreenTimeLimitsPageObject.initBackSpaceClick();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            ScreenTimeManagementPageObject.initDailyAllocationSaturdayElement();
            String dailyAllocationSaturdayText = ScreenTimeManagementPageObject.getDailyAllocationSaturdayText();
            assertEquals("we see unexpected text in dailyAllocationSaturdayElement",
                    "Sat\n01m", dailyAllocationSaturdayText);
            ScreenTimeManagementPageObject.openScreenTimeLimits();
            ScreenTimeLimitsPageObject.selectAllDaysToApplyTheseLimits();
            ScreenTimeLimitsPageObject.enter24h00m();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            ScreenTimeManagementPageObject.initDailyAllocationMondayElement();
            String dailyAllocationMondayText = ScreenTimeManagementPageObject.getDailyAllocationMondayText();
            assertEquals("we see unexpected text in dailyAllocationMondayElement",
                    "Mon\nUnltd", dailyAllocationMondayText);
            ScreenTimeManagementPageObject.initDailyAllocationTuesdayElement();
            String dailyAllocationTuesdayText = ScreenTimeManagementPageObject.getDailyAllocationTuesdayText();
            assertEquals("we see unexpected text in dailyAllocationTuesdayElement",
                    "Tue\nUnltd", dailyAllocationTuesdayText);
            ScreenTimeManagementPageObject.initDailyAllocationWednesdayElement();
            String dailyAllocationWednesdayText = ScreenTimeManagementPageObject.getDailyAllocationWednesdayText();
            assertEquals("we see unexpected text in dailyAllocationWednesdayElement",
                    "Wed\nUnltd", dailyAllocationWednesdayText);
            ScreenTimeManagementPageObject.initDailyAllocationThursdayElement();
            String dailyAllocationThursdayText = ScreenTimeManagementPageObject.getDailyAllocationThursdayText();
            assertEquals("we see unexpected text in dailyAllocationThursdayElement",
                    "Thu\nUnltd", dailyAllocationThursdayText);
            ScreenTimeManagementPageObject.initDailyAllocationFridayElement();
            String dailyAllocationFridayText = ScreenTimeManagementPageObject.getDailyAllocationFridayText();
            assertEquals("we see unexpected text in dailyAllocationFridayElement",
                    "Fri\nUnltd", dailyAllocationFridayText);
            ScreenTimeManagementPageObject.initDailyAllocationSaturdayElement();
            String dailyAllocationSaturdayText1 = ScreenTimeManagementPageObject.getDailyAllocationSaturdayText();
            assertEquals("we see unexpected text in dailyAllocationSaturdayElement",
                    "Sat\nUnltd", dailyAllocationSaturdayText1);
            ScreenTimeManagementPageObject.initDailyAllocationSundayElement();
            String dailyAllocationSundayText = ScreenTimeManagementPageObject.getDailyAllocationSundayText();
            assertEquals("we see unexpected text in dailyAllocationSundayElement",
                    "Sun\nUnltd", dailyAllocationSundayText);
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test
    public void testBoundaryValuesForScreenTimeToday() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
            CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_third);
            ScreenTimeManagementPageObject.turnSwitcherOFF();
            ScreenTimeManagementPageObject.longPressAndWaitUntilMinusButtonDisabled();
            Integer actualSecondsOfScreenTimeOverride = ScreenTimeManagementPageObject.actualSecondsOfScreenTimeOverride();
            Integer expectedSecondsOfScreenTimeOverride = 0;
            Integer actualSecondsOfScreenTimeLeft = ScreenTimeManagementPageObject.actualSecondsOfScreenTimeLeft();
            Integer expectedSecondsOfScreenTimeLeft = 0;
            assertEquals("actualSecondsOfScreenTimeOverride != 0",
                    expectedSecondsOfScreenTimeOverride, actualSecondsOfScreenTimeOverride);
            assertEquals("actualSecondsOfScreenTimeLeft != 0",
                    expectedSecondsOfScreenTimeLeft, actualSecondsOfScreenTimeLeft);
            ScreenTimeManagementPageObject.longPressAndWaitUntilPlusButtonDisabled();
            ScreenTimeManagementPageObject.waitForCheckboxUnlimitedOn();
            String screenTimeLeftText = ScreenTimeManagementPageObject.getScreenTimeTodayLeftElementText();
            assertEquals("we see unexpected value in screenTimeLeft",
                    "\u200BUnlimited", screenTimeLeftText);
            String overrideForTodayText = ScreenTimeManagementPageObject.getOverrideForTodayText();
            System.out.println("overrideForTodayText: " + overrideForTodayText);
            assertEquals("we see unexpected value in OverrideForToday", overrideForTodayText, "");
            ScreenTimeManagementPageObject.checkThatPlusMinusButtonsDisabled();
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test
    public void testBoundaryValuesForWeeklyScreenTime() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
            CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_third);
            ScreenTimeManagementPageObject.turnSwitcherON();
            ScreenTimeManagementPageObject.openScreenTimeLimits();
            ScreenTimeLimitsPageObject ScreenTimeLimitsPageObject = ScreenTimeLimitsPageObjectFactory.get(driver_third);
            ScreenTimeLimitsPageObject.setUnlimitedScreenTimeForWeek();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            ScreenTimeManagementPageObject.openScreenTimeLimits();
            ScreenTimeLimitsPageObject.turnSwitcherOFF();
            ScreenTimeLimitsPageObject.enter00h00m();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            ScreenTimeLimitsPageObject.waitForAlertAndClickOK();
            ScreenTimeLimitsPageObject.enter99h99m();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            String saturdayElementText =
                    ScreenTimeManagementPageObject.getDailyAllocationSaturdayText();
            assertEquals("we see unexpected text in saturdayElement",
                    "Sat\nUnltd", saturdayElementText);
            String screenTimeLeftText = ScreenTimeManagementPageObject.getScreenTimeTodayLeftElementText();
            assertEquals("we see unexpected value in screenTimeLeft",
                    "\u200BUnlimited", screenTimeLeftText);
            ScreenTimeLimitsPageObject.waitForCheckboxUnlimitedOn();
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test
    public void testMatchWeeklyAndDailyScreenTime() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
            CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_third);
            ScreenTimeManagementPageObject.turnSwitcherON();
            ScreenTimeManagementPageObject.openScreenTimeLimits();
            ScreenTimeLimitsPageObject ScreenTimeLimitsPageObject = ScreenTimeLimitsPageObjectFactory.get(driver_third);
            ScreenTimeLimitsPageObject.setUnlimitedScreenTimeForWeek();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            ScreenTimeLimitsPageObject.turnSwitcherOFF();
            String screenTimeTodayLeftElementText = ScreenTimeManagementPageObject.getScreenTimeTodayLeftElementText();
            assertEquals("we see unexpected value in screenTimeTodayLeft",
                    "\u200B08h 00m", screenTimeTodayLeftElementText);
            ScreenTimeLimitsPageObject.turnSwitcherON();
            ScreenTimeManagementPageObject.goToTheCurrentDayInWeeklyLimits();
            ScreenTimeLimitsPageObject.turnSwitcherOFF();
            ScreenTimeLimitsPageObject.enter19h19m();
            ScreenTimeLimitsPageObject.setNewScreentimeClick();
            ScreenTimeManagementPageObject.turnSwitcherOFF();
            String screenTimeTodayLeftElementText1 = ScreenTimeManagementPageObject.getScreenTimeTodayLeftElementText();
            assertEquals("we see unexpected value in screenTimeTodayLeft",
                    "\u200B19h 19m", screenTimeTodayLeftElementText1);
        } finally {
            recorder.stopRecording(methodName);
        }
    }

}
