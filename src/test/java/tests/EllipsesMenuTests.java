package tests;

import lib.*;
import lib.ui.AuthPageObject;
import lib.ui.FeedPageObject;
import lib.ui.PasswordChangePageObject;
import lib.ui.factories.AuthPageObjectFactory;
import lib.ui.factories.FeedPageObjectFactory;
import lib.ui.factories.PasswordChangePageObjectFactory;
import org.junit.Test;

public class EllipsesMenuTests extends CoreTestCaseForParallelSessions {


        @Test
        public void testChangePassword() throws Exception {
                String methodName = new Object() {
                }.getClass().getEnclosingMethod().getName();
                VideoRecorder recorder = new VideoRecorder();
                recorder.startRecording();
                try {
                        AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver);
                        AuthPageObject.authFor("test114@esc.tech");
                        FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver);
                        FeedPageObject.initEllipsesMenuElementClick();
                        FeedPageObject.initGeneralSettingsClick();
                        FeedPageObject.initChangePasswordClick();
                        PasswordChangePageObject PasswordChangePageObject = PasswordChangePageObjectFactory.get(driver);
                        PasswordChangePageObject.enterCurrentPassword("111111");
                        PasswordChangePageObject.enterNewPassword("11111111111");
                        PasswordChangePageObject.repeatNewPassword("111111");
                        PasswordChangePageObject.doneButtonClick();
                        PasswordChangePageObject.waitForErrorMessage();
                        String errorMessage = PasswordChangePageObject.getTextInErrorMessage();
                        if (Platform.getInstance().isAndroid()) {
                                assertEquals("errorMessage text does not match expected ",
                                        "Passwords don't match", errorMessage);
                        } else {
                                assertEquals("errorMessage text does not match expected ",
                                        "Password and confirmation doesn't match", errorMessage);
                                PasswordChangePageObject.confirmIOSAlert();
                        }
                        PasswordChangePageObject.clearNewPasswordInputField();
                        PasswordChangePageObject.clearRepeatPasswordInputField();
                        PasswordChangePageObject.clearCurrentInputField();
                        PasswordChangePageObject.enterNewPassword("1111111111");
                        PasswordChangePageObject.repeatNewPassword("1111111111");
                        PasswordChangePageObject.enterCurrentPassword("1111111");
                        PasswordChangePageObject.doneButtonClick();
                        PasswordChangePageObject.waitForErrorMessage();
                        String errorMessage2 = PasswordChangePageObject.getTextInErrorMessage();
                        if (Platform.getInstance().isAndroid()) {
                                assertEquals("errorMessage text does not match expected ",
                                        "Invalid current password", errorMessage2);
                        } else {
                                assertEquals("errorMessage text does not match expected ",
                                        "Current password does not match", errorMessage2);
                                PasswordChangePageObject.confirmIOSAlert();
                        }
                        PasswordChangePageObject.clearCurrentInputField();
                        PasswordChangePageObject.enterCurrentPassword("111111");
                        PasswordChangePageObject.doneButtonClick();
                        if (Platform.getInstance().isIOS()) {
                                PasswordChangePageObject.confirmIOSAlert();
                        } else {
                                System.out.println();
                        }
                        FeedPageObject.initLogoutClick();
                        AuthPageObject.switchToSignIn();
                        AuthPageObject.sendKeysInEmailField("test114@esc.tech");
                        AuthPageObject.sendKeysInPassField("111111");
                        AuthPageObject.loginButtonClick();
                        if (Platform.getInstance().isIOS()) {
                                System.out.println();
                        } else {
                                AuthPageObject.waitForErrorMessage();
                                String textInErrorMessage = AuthPageObject.textInErrorMessage();
                                assertEquals("errorMessage text does not match expected ",
                                        "Unknown error: Incorrent email and/or password!", textInErrorMessage);
                        }
                        AuthPageObject.clearPasswordField();
                        AuthPageObject.sendKeysInPassField("1111111111");
                        AuthPageObject.loginButtonClick();
                        FeedPageObject.initEllipsesMenuElementClick();
                        FeedPageObject.initGeneralSettingsClick();
                        FeedPageObject.initChangePasswordClick();
                        PasswordChangePageObject.enterCurrentPassword("1111111111");
                        PasswordChangePageObject.enterNewPassword("111111");
                        PasswordChangePageObject.repeatNewPassword("111111");
                        PasswordChangePageObject.doneButtonClick();
                        if (Platform.getInstance().isAndroid()) {
                                System.out.println();
                        } else {
                                PasswordChangePageObject.confirmIOSAlert();
                        }
                        FeedPageObject.initLogoutClick();
                        AuthPageObject.switchToSignIn();
                        AuthPageObject.sendKeysInEmailField("test114@esc.tech");
                        AuthPageObject.sendKeysInPassField("111111");
                        AuthPageObject.loginButtonClick();
                        FeedPageObject.waitForEllipsesMenuElement();
                } finally {
                        recorder.stopRecording(methodName);
                }

        }
}
