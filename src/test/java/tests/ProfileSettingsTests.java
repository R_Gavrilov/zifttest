package tests;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import javafx.print.PageLayout;
import lib.CoreTestCase;
import lib.CoreTestCaseForParallelSessions;
import lib.Platform;
import lib.VideoRecorder;
import lib.ui.factories.AuthPageObjectFactory;
import lib.ui.factories.ChildProfilePageObjectFactory;
import lib.ui.factories.FeedPageObjectFactory;
import lib.ui.factories.ProfileSettingsPageObjectFactory;
import org.junit.Test;
import lib.ui.AuthPageObject;
import lib.ui.ChildProfilePageObject;
import lib.ui.FeedPageObject;
import lib.ui.ProfileSettingsPageObject;
import org.openqa.selenium.WebElement;

public class ProfileSettingsTests extends CoreTestCaseForParallelSessions {

    @Test
// ios
    public void testEditingChildProfileName() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ChildProfilePageObject.initAvatarClick();
            ProfileSettingsPageObject ProfileSettingsPageObject = ProfileSettingsPageObjectFactory.get(driver_third);
            ProfileSettingsPageObject.clearOldProfileName();
            ProfileSettingsPageObject.enterNewChildName("Elizabeth");
            if(Platform.getInstance().isAndroid()) {
                this.hideKeyboard();
                ProfileSettingsPageObject.tapDoneButton();
                String newChildName = ChildProfilePageObject.getNewChildName();
                assertEquals("we see unexpected child name", "Elizabeth", newChildName);
                ChildProfilePageObject.initAvatarClick();
                ProfileSettingsPageObject.clearOldProfileName();
                ProfileSettingsPageObject.enterNewChildName("Richie");
                this.hideKeyboard();
                ProfileSettingsPageObject.tapDoneButton();
                String newChildName2 = ChildProfilePageObject.getNewChildName();
                assertEquals("we see unexpected child name", "Richie", newChildName2);
            } else {
                ProfileSettingsPageObject.tapDoneButton();
                String newChildName = ChildProfilePageObject.getNewChildName();
                assertEquals("we see unexpected child name", "Elizabeth", newChildName);
                ChildProfilePageObject.initAvatarClick();
                ProfileSettingsPageObject.clearOldProfileName();
                ProfileSettingsPageObject.enterNewChildName("Richie");
                ProfileSettingsPageObject.tapDoneButton();
                String newChildName2 = ChildProfilePageObject.getNewChildName();
                assertEquals("we see unexpected child name", "Richie", newChildName2);
            }
        } finally {
            recorder.stopRecording(methodName);
        }
//добавить assert на overview
    }

    @Test

    public void testEditingChildProfileGender() throws Exception {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ChildProfilePageObject.initAvatarClick();
            ProfileSettingsPageObject ProfileSettingsPageObject = ProfileSettingsPageObjectFactory.get(driver_third);
            ProfileSettingsPageObject.tapGenderFButton();
            ProfileSettingsPageObject.tapDoneButton();
            ChildProfilePageObject.initAvatarClick();
            String selectedGender = ProfileSettingsPageObject.getSelectedGender();
            assertEquals("we see unexpected enable sex button", "F", selectedGender);
            ProfileSettingsPageObject.tapGenderMButton();
            ProfileSettingsPageObject.tapDoneButton();
            ChildProfilePageObject.initAvatarClick();
            String selectedGender2 = ProfileSettingsPageObject.getSelectedGender();
            assertEquals("we see unexpected enable sex button", "M", selectedGender2);
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test

    public void testEditingChildProfileAge() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ChildProfilePageObject.initAvatarClick();
            ProfileSettingsPageObject ProfileSettingsPageObject = ProfileSettingsPageObjectFactory.get(driver_third);
            ProfileSettingsPageObject.tapEditAgeButton();
            ProfileSettingsPageObject.tapOnElementFromDropdownList("2");
            ProfileSettingsPageObject.tapDoneButton();
            ChildProfilePageObject.initAvatarClick();
            String selectedAge1 = ProfileSettingsPageObject.getSelectedAgeElement();
            assertEquals("we see unexpected age", "2", selectedAge1);
            ProfileSettingsPageObject.tapEditAgeButton();
            ProfileSettingsPageObject.tapOnElementFromDropdownListWithScroll("21");
            ProfileSettingsPageObject.tapDoneButton();
            ChildProfilePageObject.initAvatarClick();
            String selectedAge2 = ProfileSettingsPageObject.getSelectedAgeElement();
            assertEquals("we see unexpected age", "21", selectedAge2);
        } finally {
            recorder.stopRecording(methodName);
        }
    }

}
