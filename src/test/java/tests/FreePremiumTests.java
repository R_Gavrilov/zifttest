package tests;


import lib.CoreTestCase;
import lib.CoreTestCaseForParallelSessions;
import lib.VideoRecorder;
import lib.ui.factories.*;
import org.junit.Test;
import lib.ui.*;


public class FreePremiumTests extends CoreTestCaseForParallelSessions {

//    @Test
//    public void testSubscribeUnsubscribe() throws Exception {
//        String methodName = new Object() {
//        }.getClass().getEnclosingMethod().getName();
//        VideoRecorder recorder = new VideoRecorder();
//        recorder.startRecording();
//        try {
//            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver);
//            if (Platform.getInstance().isAndroid()) {
//                AuthPageObject.authFor("testsub1@esc.tech");
//            } else {
//                AuthPageObject.authFor("testzpa1057@esc.tech");
//            }
//            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver);
//            FeedPageObject.initEllipsesMenuElementClick();
//            Thread.sleep(5000);
//            FeedPageObject.initGeneralSettingsClick();
//            if (Platform.getInstance().isAndroid()) {
//                FeedPageObject.initSubscriptionClick();
//            } else {
//                FeedPageObject.goPremiumClick();
//            }
//            SubscriptionPageObject SubscriptionPageObject = SubscriptionPageObjectFactory.get(driver);
//            SubscriptionPageObject.initSubscribeButtonClick();
//            try {
//                SubscriptionPageObject.initSubscribeButtonPopUpClick();
//                SubscriptionPageObject.enterPasswordForSamsungtesteight();
//                SubscriptionPageObject.initVerifyClick();
//            } catch (TimeoutException e){
//                CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
//                CommonNavigationUI.tapOnElement("BUY");
//            }
//            SubscriptionPageObject.initUnsubscribeClick();
//            SubscriptionPageObject.waitForPageLoad();
//            SubscriptionPageObject.swipeToCancelSubscriptionButton();
//            SubscriptionPageObject.initCancelSubscriptionClick();
//            SubscriptionPageObject.initDeclinetoAnswerClick();
//            SubscriptionPageObject.initContinueButtonClick();
//            SubscriptionPageObject.initCancelSubscriptionButtonClick();
//            SubscriptionPageObject.waitForRestoreButtonPresent();
//        } finally {
//            recorder.stopRecording(methodName);
//        }
//    }

    @Test
    public void testSubscribeUnsubscribe() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver);
            AuthPageObject.authFor("testsub1@esc.tech");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver);
            FeedPageObject.initEllipsesMenuElementClick();
            Thread.sleep(5000);
            FeedPageObject.initGeneralSettingsClick();
            FeedPageObject.initSubscriptionClick();
            SubscriptionPageObject SubscriptionPageObject = SubscriptionPageObjectFactory.get(driver);
            SubscriptionPageObject.initSubscribeButtonClick();
            SubscriptionPageObject.makePremiumSubscription();
            SubscriptionPageObject.initUnsubscribeClick();
            SubscriptionPageObject.waitForPageLoad();
            SubscriptionPageObject.swipeToCancelSubscriptionButton();
            SubscriptionPageObject.initCancelSubscriptionClick();
            SubscriptionPageObject.initDeclinetoAnswerClick();
            SubscriptionPageObject.initContinueButtonClick();
            SubscriptionPageObject.initCancelSubscriptionButtonClick();
            SubscriptionPageObject.waitForRestoreButtonPresent();
        } finally {
            recorder.stopRecording(methodName);
        }
    }

//    @Test

//    public void testFeatureAvailabilityFreePremium() throws Exception {
//        String methodName = new Object() {
//        }.getClass().getEnclosingMethod().getName();
//        VideoRecorder recorder = new VideoRecorder();
//        recorder.startRecording();
//        try {
//            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver);
//            AuthPageObject.authFor("test86a@esc.tech");
//            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver);
//            FeedPageObject.initTongueTopClick();
//            FamilyOverviewPageObject FamilyOverviewPageObject = FamilyOverviewPageObjectFactory.get(driver);
//            FamilyOverviewPageObject.initCalendarClick();
//            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
//            CommonNavigationUI.keepZiftFree();
//            FamilyOverviewPageObject.initPlayPauseClick();
//            CommonNavigationUI.waitForElementPresentByText("No Internet");
//            int amountOfElementsInActionSheet = FamilyOverviewPageObject.amountOfElementsInPlayPauseActionSheet();
//            assertTrue("amountOfElementsInActionSheet != 3", amountOfElementsInActionSheet == 3);
//            String elementTextIndex0 = CommonNavigationUI.elementTextFromActionSheetWithIndex(0);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Resume Standard", elementTextIndex0);
//            String elementTextIndex1 = CommonNavigationUI.elementTextFromActionSheetWithIndex(2);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Pause Device", elementTextIndex1);
//            this.navigateBack();
//            FamilyOverviewPageObject.initTongueButtomClick();
//            FeedPageObject.initChildProfileClick();
//            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver);
////checkTabs(Free)
//            ChildProfilePageObject.switchToAppsTab();
//            CommonNavigationUI.waitForBlurDestinationElementPresent();
//            CommonNavigationUI.waitForBlurSeeMorePresent();
//            ChildProfilePageObject.switchToSearchesTab();
//            CommonNavigationUI.waitForBlurDestinationElementPresent();
//            CommonNavigationUI.waitForBlurSeeMorePresent();
//            ChildProfilePageObject.switchToScreenTimeTab();
//            CommonNavigationUI.waitForBlurDestinationElementPresent();
//            CommonNavigationUI.waitForBlurSeeMorePresent();
//            ChildProfilePageObject.switchToBlocksAlertsTab();
//            CommonNavigationUI.waitForBlurDestinationElementPresent();
//            CommonNavigationUI.waitForBlurSeeMorePresent();
//            ChildProfilePageObject.switchToLocationTab();
//            CommonNavigationUI.waitForBlurDestinationElementPresent();
//            CommonNavigationUI.waitForBlurSeeMorePresent();
//            ChildProfilePageObject.initCalendarClick();
//            CommonNavigationUI.keepZiftFree();
//            ChildProfilePageObject.initPlayPauseClick();
//            CommonNavigationUI.waitForElementPresentByText("No Internet");
//            int amountOfElementsInActionSheetChildProfile = ChildProfilePageObject.amountOfElementsInPlayPauseActionSheet();
//            assertTrue
//                    ("amountOfElementsInActionSheet != 3", amountOfElementsInActionSheetChildProfile == 3);
//            String playPauseActionSheetTextIndex0 = CommonNavigationUI.elementTextFromActionSheetWithIndex(0);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Resume Standard", playPauseActionSheetTextIndex0);
//            String playPauseActionSheetTextIndex1 = CommonNavigationUI.elementTextFromActionSheetWithIndex(2);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Pause Device", playPauseActionSheetTextIndex1);
//            this.navigateBack();
//// restrictions
//            ChildProfilePageObject.initRestrictionsClick();
//            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver);
//            RestrictionsPageObject.initContentFilterClick();
//            CommonNavigationUI.keepZiftFree();
//            CommonNavigationUI.initBackClick();
//            RestrictionsPageObject.initWebsiteSettingsClick();
//
//            CommonNavigationUI.keepZiftFree();
//            RestrictionsPageObject.initAppSettingsClick();
//
//            CommonNavigationUI.keepZiftFree();
//            CommonNavigationUI.initBackClick();
////subscribe
//            CommonNavigationUI.initBackClick();
//            FeedPageObject.initEllipsesMenuElementClick();
//            FeedPageObject.initGeneralSettingsClick();
//            FeedPageObject.initSubscriptionClick();
//
//            SubscriptionPageObject SubscriptionPageObject = SubscriptionPageObjectFactory.get(driver);
//            SubscriptionPageObject.initSubscribeButtonClick();
//            SubscriptionPageObject.makePremiumSubscription();
//            SubscriptionPageObject.waitForUnsubscribeButton();
////checkChildProfilePremiumSchedule/PlayPause
//            CommonNavigationUI.initBackClick();
//            CommonNavigationUI.initBackClick();
//            FeedPageObject.initChildProfileClick();
//            ChildProfilePageObject.initCalendarClick();
//            CommonNavigationUI.waitForElementPresentByText("Run timer");
//            int amountOfElementsInActionSheetCalendarPremium = CommonNavigationUI.amountOfElementsInCalendarActionSheet();
//            assertTrue
//                    ("amountOfElementsInActionSheet != 2", amountOfElementsInActionSheetCalendarPremium == 2);
//            String calendarActionSheetTextIndex0 = CommonNavigationUI.elementTextFromActionSheetWithIndex(0);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Resume Schedule", calendarActionSheetTextIndex0);
//            this.navigateBack();
//            ChildProfilePageObject.initPlayPauseClick();
//            CommonNavigationUI.waitForElementPresentByText("No Internet");
//            int playPauseActionSheetElementsPremium = ChildProfilePageObject.amountOfElementsInPlayPauseActionSheet();
//            assertTrue
//                    ("amountOfElementsInActionSheet != 3", playPauseActionSheetElementsPremium == 3);
//            String premiumPlayPauseActionSheetTextIndex0 = CommonNavigationUI.elementTextFromActionSheetWithIndex(0);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Resume Standard", premiumPlayPauseActionSheetTextIndex0);
//            String premiumPlayPauseActionSheetTextIndex1 = CommonNavigationUI.elementTextFromActionSheetWithIndex(2);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Pause Device", premiumPlayPauseActionSheetTextIndex1);
//            this.navigateBack();
////checkTabs(Premium)
//
//            ChildProfilePageObject.switchToAppsTab();
//            CommonNavigationUI.waitForBlurDestinationElementNotPresent();
//            CommonNavigationUI.waitForBlurSeeMoreNotPresent();
//            ChildProfilePageObject.switchToSearchesTab();
//            CommonNavigationUI.waitForBlurDestinationElementNotPresent();
//            CommonNavigationUI.waitForBlurSeeMoreNotPresent();
//            ChildProfilePageObject.switchToScreenTimeTab();
//            CommonNavigationUI.waitForBlurDestinationElementNotPresent();
//            CommonNavigationUI.waitForBlurSeeMoreNotPresent();
//            ChildProfilePageObject.switchToBlocksAlertsTab();
//            CommonNavigationUI.waitForBlurDestinationElementNotPresent();
//            CommonNavigationUI.waitForBlurSeeMoreNotPresent();
//            ChildProfilePageObject.switchToLocationTab();
//            CommonNavigationUI.waitForBlurDestinationElementNotPresent();
//            CommonNavigationUI.waitForBlurSeeMoreNotPresent();
////checkRestrictionsPremium
//            ChildProfilePageObject.initRestrictionsClick();
//            RestrictionsPageObject.initContentFilterClick();
//            CommonNavigationUI.initBackClick();
//            RestrictionsPageObject.initWebsiteSettingsClick();
//            WebsiteSettingsPageObject WebsiteSettingsPageObject = WebsiteSettingsPageObjectFactory.get(driver);
//            WebsiteSettingsPageObject.waitForDescriptionElement();
//            WebsiteSettingsPageObject.waitForAddBlockElement();
//            CommonNavigationUI.initBackClick();
//            RestrictionsPageObject.initAppSettingsClick();
//            AppSettingsPageObject AppSettingsPageObject = AppSettingsPageObjectFactory.get(driver);
//            CommonNavigationUI.waitForBlockElementPresent("Amazon group");
//            CommonNavigationUI.waitForAllowElementPresent("Amazon group");
//            AppSettingsPageObject.waitForFilterElementsPresent();
//            CommonNavigationUI.initBackClick();
//            CommonNavigationUI.initBackClick();
//            CommonNavigationUI.initBackClick();
////FamilyOverviewPremiumSchedule/PlayPause
//            FeedPageObject.initTongueTopClick();
//            CommonNavigationUI.initCalendarClick();
//            CommonNavigationUI.waitForElementPresentByText("Run timer");
//            int amountOfElementsInActionSheetCalendarPremiumFamily = CommonNavigationUI.amountOfElementsInCalendarActionSheet();
//            assertTrue(
//                    "amountOfElementsInActionSheet != 2", amountOfElementsInActionSheetCalendarPremiumFamily == 2);
//            String familyOverviewCalendarActionSheetTextIndex0 = CommonNavigationUI.elementTextFromActionSheetWithIndex(0);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Resume Schedule", familyOverviewCalendarActionSheetTextIndex0);
//            this.navigateBack();
//            FamilyOverviewPageObject.initPlayPauseClick();
//            CommonNavigationUI.waitForElementPresentByText("No Internet");
//            int amountOfElementsInActionSheetPlayPausePremiumFamily = FamilyOverviewPageObject.amountOfElementsInPlayPauseActionSheet();
//            assertTrue
//                    ("amountOfElementsInActionSheet != 3", amountOfElementsInActionSheetPlayPausePremiumFamily == 3);
//            String familyOverviewPlayPauseElementTextIndex0 = CommonNavigationUI.elementTextFromActionSheetWithIndex(0);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Resume Standard", familyOverviewPlayPauseElementTextIndex0);
//            String familyOverviewPlayPauseElementTextIndex1 = CommonNavigationUI.elementTextFromActionSheetWithIndex(2);
//            assertEquals("We see unexpected actionSheet1 button!",
//                    "Pause Device", familyOverviewPlayPauseElementTextIndex1);
//            this.navigateBack();
//            FamilyOverviewPageObject.initTongueButtomClick();
////unsubscribe
//            FeedPageObject.initEllipsesMenuElementClick();
//            FeedPageObject.initGeneralSettingsClick();
//            FeedPageObject.initSubscriptionClick();
//            SubscriptionPageObject.initUnsubscribeClick();
//            SubscriptionPageObject.waitForPageLoad();
//            SubscriptionPageObject.swipeToCancelSubscriptionButton();
//            SubscriptionPageObject.initCancelSubscriptionClick();
//            SubscriptionPageObject.initDeclinetoAnswerClick();
//            SubscriptionPageObject.initContinueButtonClick();
//            SubscriptionPageObject.initCancelSubscriptionButtonClick();
//            SubscriptionPageObject.waitForRestoreButtonPresent();
//        } finally {
//            recorder.stopRecording(methodName);
//        }
//    }


    @Test

    public void testCheckLinks() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver);
            AuthPageObject.skipWelcomeMessage();
            AuthPageObject.getStartedClick();
            AuthPageObject.termsOfServiceClick();
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
            CommonNavigationUI.waitForElementPresentByTextContains("Terms and Conditions");

//android.view.View[normalize-space(@text)='Zift Software LLC Terms of Service'] - не работает!

            this.navigateBack();
            AuthPageObject.privacyPolicyClick();
            CommonNavigationUI.waitForElementPresentByTextContains("Privacy Policy");
            this.navigateBack();
            AuthPageObject.switchToSignIn();
            AuthPageObject.forgotPasswordClick();
            CommonNavigationUI.waitForElementPresentByTextContains("Password Reset Request");
            this.navigateBack();
            AuthPageObject.sendKeysInEmailField("test87@gmail.com");
            this.hideKeyboard();
            AuthPageObject.sendKeysInPassField("111111");
            this.hideKeyboard();
            AuthPageObject.loginButtonClick();

            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver);
            FeedPageObject.appAdvisorClick();
            CommonNavigationUI.waitForElementPresentByTextContains("App Advisor");
            this.navigateBack();

            FeedPageObject.initEllipsesMenuElementClick();
            FeedPageObject.initHelpElementClick();

            CommonNavigationUI.waitForElementPresentByTextContains("Support");
            this.navigateBack();
            FeedPageObject.initEllipsesMenuElementClick();
            FeedPageObject.initAboutClick();
            AuthPageObject.privacyPolicyClick();
            CommonNavigationUI.waitForElementPresentByTextContains("Privacy Policy");
            this.navigateBack();
//            FeedPageObject.parentPortalClick();
            CommonNavigationUI.tapOnElement("Parent Portal");
            CommonNavigationUI.waitForElementPresentByTextContains("Parent Portal");
            this.navigateBack();

            CommonNavigationUI.initBackClick();

//            FeedPageObject.initEllipsesMenuElementClick();
//            FeedPageObject.waitForGeneralSettingsElementPresent();
//            FeedPageObject.initGeneralSettingsClick();
//            FeedPageObject.waitForSubscriptionElementPresent();
//
//            FeedPageObject.initSubscriptionClick();
//
//            SubscriptionPageObject SubscriptionPageObject = SubscriptionPageObjectFactory.get(driver);
//            SubscriptionPageObject.initTermsOfUseClick();
//            CommonNavigationUI.waitForElementPresentByContentDescContains("Terms and Conditions");
//            this.navigateBack();
//            SubscriptionPageObject.initPrivacyPolicyClick();
//            CommonNavigationUI.waitForElementPresentByContentDescContains("Privacy Policy");
//            this.navigateBack();
//            CommonNavigationUI.initBackClick();
//            CommonNavigationUI.initBackClick();

            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver);
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver);
            RestrictionsPageObject.initContentFilterClick();
            ContentFilterPageObject ContentFilterPageObject = ContentFilterPageObjectFactory.get(driver);
            ContentFilterPageObject.initHowItWorksLinkClick();
            CommonNavigationUI.waitForElementPresentByTextContains("Content Filter Categories");
            ContentFilterPageObject.initCloseAlert();
            CommonNavigationUI.initBackClick();
            RestrictionsPageObject.initWebsiteSettingsClick();
            WebsiteSettingsPageObject WebsiteSettingsPageObject = WebsiteSettingsPageObjectFactory.get(driver);
            WebsiteSettingsPageObject.contentFilterSettingsLinkClick();
            CommonNavigationUI.waitForElementPresentByTextContains("Content Filter");

        } finally {
            recorder.stopRecording(methodName);
        }


    }
}



