package tests;

import lib.CoreTestCase;
import lib.CoreTestCaseForParallelSessions;
import lib.VideoRecorder;
import lib.ui.factories.*;
import org.junit.Test;
import lib.ui.*;

public class RestrictionsTests extends CoreTestCaseForParallelSessions {

    @Test

    public void testAppSettingsAllowBlock() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_fourth);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_fourth);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_fourth);
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver_fourth);
            RestrictionsPageObject.initAppSettingsClick();
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_fourth);
            CommonNavigationUI.waitForElementPresentByText("After School");
            CommonNavigationUI.swipeUpToElementPresent("BuzzVideo");
            CommonNavigationUI.switchToBlock("BuzzVideo");
            CommonNavigationUI.waitForAttributeOfTheBlockButtonStayTrue("BuzzVideo");
            CommonNavigationUI.switchToAllow("BuzzVideo");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("BuzzVideo");
//      AppSettingsPageObject.initSwitchTabInstalled();
            CommonNavigationUI.swipeUpToElementPresent("ESPN");
            CommonNavigationUI.switchToBlock("ESPN");
            CommonNavigationUI.waitForAttributeOfTheBlockButtonStayTrue("ESPN");
            CommonNavigationUI.swipeDownToElementPresent("Clash Royale");
            CommonNavigationUI.switchToBlock("Clash Royale");
            CommonNavigationUI.waitForAttributeOfTheBlockButtonStayTrue("Clash Royale");
            CommonNavigationUI.switchToAllow("Clash Royale");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("Clash Royale");
            CommonNavigationUI.swipeUpToElementPresent("ESPN");
            CommonNavigationUI.switchToAllow("ESPN");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("ESPN");
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test

    public void testAppSettingsExpandingCell() throws Exception {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_fourth);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_fourth);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_fourth);
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver_fourth);
            RestrictionsPageObject.initAppSettingsClick();
            AppSettingsPageObject AppSettingsPageObject = AppSettingsPageObjectFactory.get(driver_fourth);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_fourth);
            CommonNavigationUI.expandCell("After School");
            CommonNavigationUI.waitForAfterSchoolDescription();
            CommonNavigationUI.swipeUpToElementPresent("Clash of Clans");
            CommonNavigationUI.expandCell("Camera");
            CommonNavigationUI.waitForElementPresentByTextContains(
                    "We have no review for this app yet",
                    "Visit the Net Nanny® App Advisor to learn about new App and technology friends");
            CommonNavigationUI.swipeUpToElementPresent("Email");
            CommonNavigationUI.swipeDownToElementPresent("Calculator");
            CommonNavigationUI.waitForElementPresentByTextContains(
                    "We have no review for this app yet",
                    "Visit the Net Nanny® App Advisor to learn about new App and technology friends");
            AppSettingsPageObject.switchToIOSFilter();
            CommonNavigationUI.swipeDownToElementPresent("Amazon");
            CommonNavigationUI.expandCell("Amazon");
            CommonNavigationUI.waitForAmazonGroupDescription();
            CommonNavigationUI.swipeUpToElementPresent("Discord");
            CommonNavigationUI.expandCell("Clash of Clans");
            CommonNavigationUI.waitForElementPresentByTextContains(
                    "We have no review for this app yet",
                    "Visit the Net Nanny® App Advisor to learn about new App and technology friends");
            CommonNavigationUI.swipeUpToElementPresent("FriendO");
            CommonNavigationUI.swipeDownToElementPresent("Chatous");
            CommonNavigationUI.waitForElementPresentByTextContains(
                    "We have no review for this app yet",
                    "Visit the Net Nanny® App Advisor to learn about new App and technology friends");
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test

    public void testContentFilter() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_fourth);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_fourth);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_fourth);
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver_fourth);
            RestrictionsPageObject.initContentFilterClick();
            ContentFilterPageObject ContentFilterPageObject = ContentFilterPageObjectFactory.get(driver_fourth);
            ContentFilterPageObject.waitUntilBlockButtonEnabled("Abortion");
            ContentFilterPageObject.switchToBlock("Abortion");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueBlockButton("Abortion");
            ContentFilterPageObject.switchToAllow("Abortion");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueAllowButton("Abortion");
            ContentFilterPageObject.switchToAllow("Adult Novelty");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueAllowButton("Adult Novelty");
            ContentFilterPageObject.switchToAlert("Adult Novelty");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueAlertButton("Adult Novelty");
            ContentFilterPageObject.switchToAlert("Anime");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueAlertButton("Anime");
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_fourth);
            CommonNavigationUI.swipeUpToElementPresent("Weapons");
            ContentFilterPageObject.switchToBlock("Tobacco");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueBlockButton("Tobacco");
            CommonNavigationUI.swipeDownToElementPresent("CATEGORIES");
            ContentFilterPageObject.switchToBlock("Anime");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueBlockButton("Anime");
            CommonNavigationUI.swipeUpToElementPresent("Weapons");
            ContentFilterPageObject.switchToAllow("Tobacco");
            ContentFilterPageObject.waitForSelectedAttributeChangeToTrueAllowButton("Tobacco");
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test

    public void testAddUrlFromWebsiteSettings() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_fourth);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_fourth);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_fourth);
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver_fourth);
            RestrictionsPageObject.initWebsiteSettingsClick();
            WebsiteSettingsPageObject WebsiteSettingsPageObject = WebsiteSettingsPageObjectFactory.get(driver_fourth);
            WebsiteSettingsPageObject.initAddBlockButtonClick();
            WebsiteSettingsPageObject.enterUrl("pornhub.com");
            WebsiteSettingsPageObject.initOkButtonClick();
            WebsiteSettingsPageObject.waitForUrlPresent("pornhub.com");
            int amountOfElementsInAlwaysBlockBeforeMoving = WebsiteSettingsPageObject.getAmountOfBlockElements();
            assertTrue("amountOfElementsInAlwaysBlock != 2",
                    amountOfElementsInAlwaysBlockBeforeMoving == 2);
            WebsiteSettingsPageObject.initAddAllowButtonClick();
            WebsiteSettingsPageObject.enterUrl("pornhub.com");
            WebsiteSettingsPageObject.initOkButtonClick();
            WebsiteSettingsPageObject.waitForPornhubIsAlreadyAddedToElement();

            WebsiteSettingsPageObject.initMoveToAlwaysAllowListClick();
            WebsiteSettingsPageObject.waitForUrlToBeMovedToAlwaysAllow();
            int amountOfElementsInAlwaysBlockAfterMoving = WebsiteSettingsPageObject.getAmountOfBlockElements();
            assertTrue("amountOfElementsInAlwaysBlock != 1",
                    amountOfElementsInAlwaysBlockAfterMoving == 1);
            WebsiteSettingsPageObject.waitForUrlPresent("pornhub.com");
            int amountOfElementsInAlwaysAllowAfterMoving = WebsiteSettingsPageObject.getAmountOfAllowElements();
            assertTrue("amountOfElementsInAlwaysAllow != 1",
                    amountOfElementsInAlwaysAllowAfterMoving == 1);
            WebsiteSettingsPageObject.deletePornhubFromAlwaysAllow();
            WebsiteSettingsPageObject.waitForAlwaysAlowListNotPresent();
        } finally {
            recorder.stopRecording(methodName);
        }
    }



}
