package tests;

import lib.CoreTestCase;
import lib.Platform;
import lib.VideoRecorder;
import lib.ui.*;
import lib.ui.factories.*;
import org.junit.Test;
import org.openqa.selenium.TimeoutException;

import java.time.Duration;

import static org.hamcrest.CoreMatchers.containsString;


import static org.junit.Assert.assertThat;

public class ModesTests extends CoreTestCase {

    @Test
    public void testModes() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_second);
            ChildProfilePageObject.initCalendarClick();
            ChildProfilePageObject.editRunTimerClick();
            RuleTimerPageObject RuleTimerPageObject = RuleTimerPageObjectFactory.get(driver_second);
            RuleTimerPageObject.initRuleSelection();
            RuleTimerPageObject.selectRule("No Internet");
            RuleTimerPageObject.enterValue12h00m();
            String selectedRule = RuleTimerPageObject.getSelectedRuleText();
            assertEquals("we see unexpected selected rule", "No Internet", selectedRule);
            String selectedTime = RuleTimerPageObject.getSelectedTimeText();
            if (Platform.getInstance().isAndroid()) {
                assertEquals("we see unexpected selected time", "12h 00m", selectedTime);
            } else {
                assertThat(selectedTime, containsString("12h:00m"));
            }
            RuleTimerPageObject.initStartTimerClick();
            ChildProfilePageObject.waitForSelectionMethodElementPresent("TIMER REMAINING");
            ChildProfilePageObject.waitForSelectionCurrentRule("No Internet");
            ChildProfilePageObject.initPlayPauseClick();
            ChildProfilePageObject.initPlayPauseResumeStandardClick();

            ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
            ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
            ChildProfilePageObject.initCalendarClick();
            ChildProfilePageObject.initCalendarResumeScheduleClick();

            ChildProfilePageObject.waitForSelectionMethodElementPresent("FROM SCHEDULE");
            ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
            ChildProfilePageObject.initPlayPauseClick();
            ChildProfilePageObject.initPlayPausePauseDeviceClick();

            ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
            ChildProfilePageObject.waitForSelectionCurrentRule("Paused");
            ChildProfilePageObject.initCalendarClick();
            String CalendarActionSheetRunTimerElementText = ChildProfilePageObject.getCalendarActionSheetRunTimerElementText();

/*        assertTrue("we see unexpected text in CalendarActionSheetRunTimerElement",
                CalendarActionSheetRunTimerElementText.contains("Run timer (No Internet for)"));*/

            if (Platform.getInstance().isAndroid()) {
                assertThat(CalendarActionSheetRunTimerElementText, containsString("Run timer (No Internet"));
            } else {
                assertThat(CalendarActionSheetRunTimerElementText, containsString("Run Timer (No Internet for 12h 0m)"));
            }
            ChildProfilePageObject.initCalendarActionSheetRunTimerClick();

            ChildProfilePageObject.waitForSelectionMethodElementPresent("TIMER REMAINING");
            ChildProfilePageObject.waitForSelectionCurrentRule("No Internet");
            ChildProfilePageObject.initCalendarClick();
            ChildProfilePageObject.editRunTimerClick();

            RuleTimerPageObject.initRuleSelection();
            RuleTimerPageObject.selectRule("Standard");

            RuleTimerPageObject.enterValue12h00m();

            String selectedRule2 = RuleTimerPageObject.getSelectedRuleText();
            assertEquals("we see unexpected selected rule",
                    "Standard", selectedRule2);
            String selectedTime2 = RuleTimerPageObject.getSelectedTimeText();
            if (Platform.getInstance().isAndroid()) {
                assertEquals("we see unexpected selected time",
                        "12h 00m", selectedTime2);
            } else {
                assertEquals("we see unexpected selected time",
                        "12h:00m", selectedTime2);
            }
            RuleTimerPageObject.initStartTimerClick();
            ChildProfilePageObject.waitForSelectionMethodElementPresent("TIMER REMAINING");
            ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
            ChildProfilePageObject.initCalendarClick();
            ChildProfilePageObject.initCalendarResumeScheduleClick();

            ChildProfilePageObject.waitForSelectionMethodElementPresent("FROM SCHEDULE");
            ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
        } finally {
            recorder.stopRecording(methodName);
        }

    }


    //andr+ios
    @Test
    public void testPlayPauseModes() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_second);
            ChildProfilePageObject.initPlayPauseClick();
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            CommonNavigationUI.tapOnElement("No Internet");
            ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
            ChildProfilePageObject.waitForSelectionCurrentRule("No Internet");
            ChildProfilePageObject.initPlayPauseClick();
            CommonNavigationUI.tapOnElement("Pause Device");
            ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
            ChildProfilePageObject.waitForSelectionCurrentRule("Paused");
            ChildProfilePageObject.initPlayPauseClick();
            CommonNavigationUI.waitForElementPresentByText("Paused");
            CommonNavigationUI.tapOnElement("Resume Standard");
            ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
            ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
            ChildProfilePageObject.initPlayPauseClick();
            CommonNavigationUI.waitForElementPresentByText("Pause Device");
        } finally {
            recorder.stopRecording(methodName);
        }
    }


    @Test
    public void testRuleTimer() throws Exception {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_second);
            ChildProfilePageObject.initPlayPauseClick();
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            try {
                CommonNavigationUI.tapOnElement("Standard");
            } catch (TimeoutException e) {
                CommonNavigationUI.tapOnElement("Resume Standard");
            }
            ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
            ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
            ChildProfilePageObject.initCalendarClick();
            ChildProfilePageObject.editRunTimerClick();
            RuleTimerPageObject RuleTimerPageObject = RuleTimerPageObjectFactory.get(driver_second);
            RuleTimerPageObject.initRuleSelection();
            RuleTimerPageObject.selectRule("No Internet");
            RuleTimerPageObject.enterValue12h00m();
            String selectedRule = RuleTimerPageObject.getSelectedRuleText();
            assertEquals("we see unexpected selected rule", "No Internet", selectedRule);
            String selectedTime = RuleTimerPageObject.getSelectedTimeText();
            if (Platform.getInstance().isAndroid()) {
                assertEquals("we see unexpected selected time", "12h 00m", selectedTime);
            } else {
                assertThat(selectedTime, containsString("12h:00m"));
            }
            RuleTimerPageObject.initStartTimerClick();
            ChildProfilePageObject.waitForSelectionMethodElementPresent("TIMER REMAINING");
            ChildProfilePageObject.waitForSelectionCurrentRule("No Internet");
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    @Test
    public void testExhaustedMode() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_second);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_second);
            ScreenTimeManagementPageObject.turnSwitcherON();
            ScreenTimeManagementPageObject.selectNoInternetExhaustedMode();
            ScreenTimeManagementPageObject.turnSwitcherOFF();
            ScreenTimeManagementPageObject.longPressAndWaitUntilMinusButtonDisabled();
            CommonNavigationUI.initBackClick();
            ChildProfilePageObject.waitForSelectionMethodElementPresent("SCREEN TIME IS OVER");
            ChildProfilePageObject.waitForSelectionCurrentRule("No Internet");
            ChildProfilePageObject.editScreenTimeClick();
            ScreenTimeManagementPageObject.selectPauseDeviceExhaustedMode();
            CommonNavigationUI.initBackClick();
            ChildProfilePageObject.waitForSelectionMethodElementPresent("SCREEN TIME IS OVER");
            ChildProfilePageObject.waitForSelectionCurrentRule("Paused");
        } finally {
            recorder.stopRecording(methodName);
        }
    }


    @Test
    public void testScheduleMode() throws Exception {
        AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
        AuthPageObject.authFor("test87@gmail.com");
        FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
        FeedPageObject.initChildProfileClick();
        ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_second);
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
        CommonNavigationUI.waitForElementPresentByTextContains("00h", "00m");
        ChildProfilePageObject.initPlayPauseClick();
        CommonNavigationUI.tapOnElement("Standard");
        ChildProfilePageObject.waitForSelectionMethodElementPresent("APPLIED MANUALLY");
        ChildProfilePageObject.waitForSelectionCurrentRule("Standard");
        ChildProfilePageObject.editScreenTimeClick();
        ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver_second);
        ScreenTimeManagementPageObject.turnSwitcherON();
        CommonNavigationUI.initBackClick();
        ChildProfilePageObject.initCalendarClick();
        ChildProfilePageObject.editScheduleClick();
        SchedulePageObject SchedulePageObject = SchedulePageObjectFactory.get(driver_second);
        SchedulePageObject.goToTheCurrentDaySchedule();
        CommonNavigationUI.waitForElementPresentByTextContains("Schedule Detail");
        SchedulePageObject.deleteTimeBlock();
        SchedulePageObject.addTimeBlockNoInternet();
        SchedulePageObject.setTimeBlockTo24Hours(Duration.ofMillis(500));
        CommonNavigationUI.initBackClick();
        CommonNavigationUI.initBackClick();
        ChildProfilePageObject.initCalendarClick();
        CommonNavigationUI.tapOnElement("Resume Schedule");
        ChildProfilePageObject.waitForSelectionMethodElementPresent("FROM SCHEDULE");
        ChildProfilePageObject.waitForSelectionCurrentRule("No Internet");
    }
}

