
package tests;

import lib.CoreTestCase;
import lib.CoreTestCaseForParallelSessions;
import lib.Platform;
import lib.VideoRecorder;
import lib.ui.factories.*;
import org.junit.Test;
import lib.ui.*;

public class ReportTabsTests extends CoreTestCaseForParallelSessions {


    @Test

    public void testAddUrlFromBlocksAlerts() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver_third);
            RestrictionsPageObject.initWebsiteSettingsClick();
            WebsiteSettingsPageObject WebsiteSettingsPageObject = WebsiteSettingsPageObjectFactory.get(driver_third);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
            CommonNavigationUI.waitForElementPresentByText("test.com");
            int amountOfElementsInAlwaysBlockBeforeDelete = WebsiteSettingsPageObject.getAmountOfBlockElements();
            assertTrue("amountOfElementsInAlwaysBlock != 1",
                    amountOfElementsInAlwaysBlockBeforeDelete == 1);
            int amountOfElementsInAlwaysAllowBeforeDelete = WebsiteSettingsPageObject.getAmountOfAllowElements();
            assertTrue("amountOfElementsInAlwaysBlock != 0",
                    amountOfElementsInAlwaysAllowBeforeDelete == 0);
            CommonNavigationUI.initBackClick();
            CommonNavigationUI.initBackClick();
            ChildProfilePageObject.switchToBlocksAlertsTab();
            BlocksAndAlertsObject BlocksAndAlertsObject = BlocksAlertsPageObjectFactory.get(driver_third);
            BlocksAndAlertsObject.gearIconClick("Viewed: Porn", "playboy.com");
            BlocksAndAlertsObject.selectAlwaysBlockRadioButton();
            BlocksAndAlertsObject.initApplyClick();
            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject.initWebsiteSettingsClick();
            WebsiteSettingsPageObject.waitForTestComUrlPresent();
            int amountOfElementsInAlwaysBlock = WebsiteSettingsPageObject.getAmountOfBlockElements();
            assertTrue("amountOfElementsInAlwaysBlock != 2",
                    amountOfElementsInAlwaysBlock == 2);
            CommonNavigationUI.initBackClick();
            CommonNavigationUI.initBackClick();
            BlocksAndAlertsObject.gearIconClick("Blocked: Nudity", "fuq.com");
            BlocksAndAlertsObject.selectAlwaysAllowRadioButton();
            BlocksAndAlertsObject.initApplyClick();

            ChildProfilePageObject.initRestrictionsClick();
            RestrictionsPageObject.initWebsiteSettingsClick();
            WebsiteSettingsPageObject.waitForTestComUrlPresent();
            int amountOfElementsInAlwaysBlock1 = WebsiteSettingsPageObject.getAmountOfBlockElements();
            assertTrue("amountOfElementsInAlwaysBlock != 2",
                    amountOfElementsInAlwaysBlock1 == 2);

            int amountOfElementsInAlwaysAllow = WebsiteSettingsPageObject.getAmountOfAllowElements();
            assertTrue("amountOfElementsInAlwaysBlock != 1",
                    amountOfElementsInAlwaysAllow == 1);

            WebsiteSettingsPageObject.initDeleteIconClick("fuq.com");
            WebsiteSettingsPageObject.waitForSelectedUrlNotPresent("fuq.com");
            WebsiteSettingsPageObject.initDeleteIconClick("playboy.com");
            WebsiteSettingsPageObject.waitForSelectedUrlNotPresent("playboy.com");
            int amountOfElementsInAlwaysBlockAfterDelete = WebsiteSettingsPageObject.getAmountOfBlockElements();
            assertTrue("amountOfElementsInAlwaysBlock != 1",
                    amountOfElementsInAlwaysBlockAfterDelete == 1);

            int amountOfElementsInAlwaysAllowAfterDelete = WebsiteSettingsPageObject.getAmountOfAllowElements();
            assertTrue("amountOfElementsInAlwaysBlock != 0",
                    amountOfElementsInAlwaysAllowAfterDelete == 0);
        } finally {
            recorder.stopRecording(methodName);
        }

    }

    @Test

    public void testSearchesReportTab() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
            FeedPageObject.initChildProfileClick();
            ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
            ChildProfilePageObject.switchToSearchesTab();
            SearchesTabObject SearchesTabPageObject = SearchesTabObjectFactory.get(driver_third);
            String textInElementWithIndex0 = SearchesTabPageObject.getElementTextWithIndex0();
            assertEquals("We see unexpected date",
                    "3/30/19", textInElementWithIndex0);
            String textInElementWithIndex1 = SearchesTabPageObject.getElementTextWithIndex1();
            assertEquals("We see unexpected search event",
                    "black, metal", textInElementWithIndex1);
            String textInElementWithIndex1DateDevice = SearchesTabPageObject.getElementTextWithIndex1DateDevice();
            assertEquals("We see unexpected date/deviceModel",
                    "2:46 PM · SM-G531H", textInElementWithIndex1DateDevice);
            SearchesTabPageObject.waitForLoupeElementPresent("black, metal");
//            SearchesTabPageObject.swipeToSearchEvent("burzum");
//            String textInElementWithIndex8 = SearchesTabPageObject.getElementTextWithIndex8AfterSwipe();
//            assertEquals("We see unexpected search event",
//                    "korrozia, metalla", textInElementWithIndex8);
//            String textInElementWithIndex6DateDevice = SearchesTabPageObject.getElementTextWithIndex6DateDeviceAfterSwipe();
//            assertEquals("We see unexpected date/deviceModel",
//                    "6:23 AM · SM-G531H", textInElementWithIndex6DateDevice);
//            SearchesTabPageObject.waitForLoupeElementPresent("burzum");
        } finally {
            recorder.stopRecording(methodName);
        }

    }


    @Test
    public void testBlocksAlertsTab() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {

            if (Platform.getInstance().isAndroid()) {
                AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
                AuthPageObject.authFor("test87@gmail.com");
                FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
                FeedPageObject.initChildProfileClick();
                ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
                ChildProfilePageObject.switchToBlocksAlertsTab();
                BlocksAndAlertsObject BlocksAndAlertsObject = BlocksAlertsPageObjectFactory.get(driver_third);
                BlocksAndAlertsObject.waitForBlocksAlertsTabElementsPresent();

            String textInTitleElementWithIndex0 = BlocksAndAlertsObject.getElementTitleTextFromBlocksAlertsList(0);
            assertEquals("We see unexpected date", "3/30/19", textInTitleElementWithIndex0);
                String textInTitleElementWithIndex1 = BlocksAndAlertsObject.getElementTitleTextFromBlocksAlertsList(1);
                assertEquals("We see unexpected title",
                        "Viewed: Porn", textInTitleElementWithIndex1);
                String textInTitleElementWithIndex2 = BlocksAndAlertsObject.getElementTitleTextFromBlocksAlertsList(2);
                assertEquals("We see unexpected title", "Blocked: Nudity", textInTitleElementWithIndex2);


                String textInLinkElementWithIndex0 = BlocksAndAlertsObject.getElementLinkTextFromBlocksAlertsList(0);
                assertTrue("we see unexpected text in LinkElementWithIndex0",
                        textInLinkElementWithIndex0.contains("playboy.com"));
                String textInLinkElementWithIndex1 = BlocksAndAlertsObject.getElementLinkTextFromBlocksAlertsList(1);
                assertTrue("we see unexpected text in LinkElementWithIndex1",
                        textInLinkElementWithIndex1.contains("playboy.com"));


            String textInSubtitleElementWithIndex0 = BlocksAndAlertsObject.getElementSubtitleTextFromBlocksAlertsList(0);
            assertEquals("We see unexpected subtitle", "2:44 PM · SM-G531H", textInSubtitleElementWithIndex0);
            String textInSubtitleElementWithIndex1 = BlocksAndAlertsObject.getElementSubtitleTextFromBlocksAlertsList(1);
            assertEquals("We see unexpected subtitle", "2:44 PM · SM-G531H", textInSubtitleElementWithIndex1);


                BlocksAndAlertsObject.waitForIconW("Viewed: Porn", "playboy.com");
                BlocksAndAlertsObject.waitForIconW("Blocked: Nudity", "playboy.com");

                BlocksAndAlertsObject.waitForAlertExclamationIcon("Viewed: Porn", "playboy.com");
                BlocksAndAlertsObject.waitForAlertExclamationIcon("Blocked: Nudity", "playboy.com");


                BlocksAndAlertsObject.waitForGearIcon("Viewed: Porn", "playboy.com");
                BlocksAndAlertsObject.waitForGearIcon("Blocked: Nudity", "playboy.com");
            } else if (Platform.getInstance().isIOS()) {

                AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
                AuthPageObject.authFor("test87@gmail.com");
                FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
                FeedPageObject.initChildProfileClick();
                Thread.sleep(3000);
                ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
                ChildProfilePageObject.switchToBlocksAlertsTab();
                BlocksAndAlertsObject BlocksAlertsPageObject = BlocksAlertsPageObjectFactory.get(driver_third);
                BlocksAlertsPageObject.waitForIOSAlertIconW();
            }
        } finally {
            recorder.stopRecording(methodName);
        }

    }

}




/*      @Test

        public void testAppReportTabGear() throws Exception {

        AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_third);
        AuthPageObject.authFor("test87@gmail.com");
        FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
        FeedPageObject.initChildProfileClick();
        ChildProfilePageObject ChildProfilePageObject = ChildProfilePageObjectFactory.get(driver_third);
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
        CommonNavigationUI.waitForElementPresentByText("YT Kids");
        CommonNavigationUI.swipeUpToElementPresent("YouTube");
        CommonNavigationUI.waitForElementPresentByText("YouTube");
        ChildProfilePageObject.initGearElementClick("YouTube");
        ChildProfilePageObject.initBlockRadioButtonSelect();
        ChildProfilePageObject.confirmRadioButtonSelection();
        this.backgroundApp(1);
        ChildProfilePageObject.waitForChildNameElement();
        ChildProfilePageObject.initRestrictionsClick();
        RestrictionsPageObject RestrictionsPageObject = RestrictionsPageObjectFactory.get(driver_third);
        RestrictionsPageObject.initAppSettingsClick();
        AppSettingsPageObject AppSettingsPageObject = AppSettingsPageObjectFactory.get(driver_third);
        AppSettingsPageObject.initSwitchTabInstalled();
        CommonNavigationUI.swipeUpToElementPresent("YouTube");
        AppSettingsPageObject.getXpathBlockElement("YouTube");
        AppSettingsPageObject.initSelectedBlockElement();
        String youtubeBlockValue = AppSettingsPageObject.getBlockAttributeValue();
        assertEquals("we see unexpected attribute value", "true", youtubeBlockValue);
        CommonNavigationUI.initBackClick();
        CommonNavigationUI.initBackClick();
        CommonNavigationUI.swipeUpToElementPresent("YouTube");
        CommonNavigationUI.waitForElementPresentByText("YouTube");
        ChildProfilePageObject.initGearElementClick("YouTube");
        ChildProfilePageObject.initAllowRadioButtonSelect();
        ChildProfilePageObject.confirmRadioButtonSelection();
        this.backgroundApp(1);
        ChildProfilePageObject.waitForChildNameElement();
        ChildProfilePageObject.initRestrictionsClick();
        RestrictionsPageObject.initAppSettingsClick();
        AppSettingsPageObject.initSwitchTabInstalled();
        CommonNavigationUI.swipeUpToElementPresent("YouTube");
        AppSettingsPageObject.getXpathBlockElement("YouTube");
        AppSettingsPageObject.initSelectedBlockElement();
        String youtubeAllowValue = AppSettingsPageObject.getBlockAttributeValue();
        assertEquals("we see unexpected attribute value", "false", youtubeAllowValue);

    }

        @Test

        public void testAppReportTabInstallDate() {

        AuthPageObject AuthPageObject = new AuthPageObject(driver_third);
        AuthPageObject.authFor("test87@gmail.com");
        FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
        FeedPageObject.initChildProfileClick();
        AppsReportTabObject AppsReportTabObject = new AppsReportTabObject(driver_third);
        AppsReportTabObject.waitForAppsTabElementsPresent();
        String elementWithIndex0 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected date", "2018.08.27", elementWithIndex0);
        String textInElementWithIndex1BeforeSwipe = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application", "YT Kids", textInElementWithIndex1BeforeSwipe);
        String textInElementWithIndex2BeforeSwipe = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected date", "2018.08.17", textInElementWithIndex2BeforeSwipe);
        String textInElementWithIndex3BeforeSwipe = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application", "Telegram", textInElementWithIndex3BeforeSwipe);
        String textInElementWithIndex4BeforeSwipe = AppsReportTabObject.getElementTextFromAppsList(4);
        assertEquals("We see unexpected date", "2018.07.19", textInElementWithIndex4BeforeSwipe);
        String textInElementWithIndex5BeforeSwipe = AppsReportTabObject.getElementTextFromAppsList(5);
        assertEquals("We see unexpected application", "DNA2Style", textInElementWithIndex5BeforeSwipe);
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("DNA2Style");
        String textInElementFromAppReportTabListWithIndex1After1Swipe = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected date", "2018.02.19", textInElementFromAppReportTabListWithIndex1After1Swipe);
        String textInElementFromAppReportTabListWithIndex2After1Swipe = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","UBANK",textInElementFromAppReportTabListWithIndex2After1Swipe);
        String textInElementFromAppReportTabListWithIndex4After1Swipe = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Магазин",textInElementFromAppReportTabListWithIndex4After1Swipe);
        String textInElementFromAppReportTabListWithIndex5After1Swipe = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected date","2017.09.04",textInElementFromAppReportTabListWithIndex5After1Swipe);
        String textInElementFromAppReportTabListWithIndex6After1Swipe = AppsReportTabObject.getElementTextFromAppsList(4);
        assertEquals("We see unexpected application","YouTube",textInElementFromAppReportTabListWithIndex6After1Swipe);
        CommonNavigationUI.hardSwipe(1020);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("YouTube");
        String textInElementFromAppReportTabListWithIndex4After2Swipe = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","SuperSU",textInElementFromAppReportTabListWithIndex4After2Swipe);
        String textInElementFromAppReportTabListWithIndex6After2Swipe = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Ситимобил",textInElementFromAppReportTabListWithIndex6After2Swipe);
        String textInElementFromAppReportTabListWithIndex7After2Swipe = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected date", "2017.08.14", textInElementFromAppReportTabListWithIndex7After2Swipe);
        String textInElementFromAppReportTabListWithIndex8After2Swipe = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Clock",textInElementFromAppReportTabListWithIndex8After2Swipe);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Clock");
        String textInElementFromAppReportTabListWithIndex2After3Swipe = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Camera",textInElementFromAppReportTabListWithIndex2After3Swipe);
        String textInElementFromAppReportTabListWithIndex4After3Swipe = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Email",textInElementFromAppReportTabListWithIndex4After3Swipe);
        String textInElementFromAppReportTabListWithIndex6After3Swipe = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Smart Manager",textInElementFromAppReportTabListWithIndex6After3Swipe);
        String textInElementFromAppReportTabListWithIndex8After3Swipe = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Settings",textInElementFromAppReportTabListWithIndex8After3Swipe);
        CommonNavigationUI.hardSwipe(1050);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Settings");
        String textInElementFromAppReportTabListWithIndex2After4Swipe = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Видео",textInElementFromAppReportTabListWithIndex2After4Swipe);
        String textInElementFromAppReportTabListWithIndex4After4Swipe = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Radio",textInElementFromAppReportTabListWithIndex4After4Swipe);
        String textInElementFromAppReportTabListWithIndex6After4Swipe = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Calendar",textInElementFromAppReportTabListWithIndex6After4Swipe);
        String textInElementFromAppReportTabListWithIndex8After4Swipe = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Internet",textInElementFromAppReportTabListWithIndex8After4Swipe);
        CommonNavigationUI.hardSwipe(980);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Internet");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","My Files",textInElementFromAppReportTabListWithIndex2AfterSwipe5);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Voice Recorder",textInElementFromAppReportTabListWithIndex4AfterSwipe5);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Calculator",textInElementFromAppReportTabListWithIndex6AfterSwipe5);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Memo",textInElementFromAppReportTabListWithIndex8AfterSwipe5);
        CommonNavigationUI.hardSwipe(980);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Memo");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Gallery", textInElementFromAppReportTabListWithIndex2AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex3AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected date","2017.04.13",textInElementFromAppReportTabListWithIndex3AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Play Music",textInElementFromAppReportTabListWithIndex4AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Настройки Google",textInElementFromAppReportTabListWithIndex6AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex7AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(4);
        assertEquals("We see unexpected date","2017.03.11",textInElementFromAppReportTabListWithIndex7AfterSwipe6);
        CommonNavigationUI.hardSwipe(1100);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Настройки Google");
        String textInElementFromAppReportTabListWithIndex0AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected date","2017.03.11",textInElementFromAppReportTabListWithIndex0AfterSwipe7);
        String textInElementFromAppReportTabListWithIndex1AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Facebook",textInElementFromAppReportTabListWithIndex1AfterSwipe7);
        String textInElementFromAppReportTabListWithIndex2AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(2);assertEquals("We see unexpected application","Google+",textInElementFromAppReportTabListWithIndex2AfterSwipe7);
        String textInElementFromAppReportTabListWithIndex3AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Gmail",textInElementFromAppReportTabListWithIndex3AfterSwipe7);
        CommonNavigationUI.hardSwipe(1100);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Gmail");
        String textInElementFromAppReportTabListWithIndex0AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected date","2016.04.13",textInElementFromAppReportTabListWithIndex0AfterSwipe8);
        String textInElementFromAppReportTabListWithIndex1AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Chrome",textInElementFromAppReportTabListWithIndex1AfterSwipe8);
        String textInElementFromAppReportTabListWithIndex42fterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Play Store",textInElementFromAppReportTabListWithIndex42fterSwipe8);
        String textInElementFromAppReportTabListWithIndex3AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Play Games",textInElementFromAppReportTabListWithIndex3AfterSwipe8);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(4);
        assertEquals("We see unexpected application","Galaxy Apps",textInElementFromAppReportTabListWithIndex4AfterSwipe8);
        CommonNavigationUI.hardSwipe(1200);
        CommonNavigationUI.hardSwipe(1200);
        String textInElementFromAppReportTabListWithIndex1AfterSwipe9 = AppsReportTabObject.getElementTextFromAppsList(1);assertEquals("We see unexpected application","Новости",textInElementFromAppReportTabListWithIndex1AfterSwipe9);
        String textInElementFromAppReportTabListWithIndex2AfterSwipe9 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Drive",textInElementFromAppReportTabListWithIndex2AfterSwipe9);
        String textInElementFromAppReportTabListWithIndex3AfterSwipe9 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Play Books",textInElementFromAppReportTabListWithIndex3AfterSwipe9);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe9 = AppsReportTabObject.getElementTextFromAppsList(4);
        assertEquals("We see unexpected application","Play Фильмы",textInElementFromAppReportTabListWithIndex4AfterSwipe9);
    }

    @Test

    public void testAppReportTabAlphabetically() {

        AuthPageObject AuthPageObject = new AuthPageObject(driver_third);
        AuthPageObject.authFor("test87@gmail.com");
        FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_third);
        FeedPageObject.initChildProfileClick();
        AppsReportTabObject AppsReportTabObject = new AppsReportTabObject(driver_third);
        AppsReportTabObject.waitForAppsTabElementsPresent();
        AppsReportTabObject.switchToAlphabetically();
        AppsReportTabObject.waitForAppsTabElementsPresent();
        String textInElementFromAppReportTabListWithIndex0AfterSwipe0 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Calculator",textInElementFromAppReportTabListWithIndex0AfterSwipe0);
        String textInElementFromAppReportTabListWithIndex2AfterSwipe0 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Calendar",textInElementFromAppReportTabListWithIndex2AfterSwipe0);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe0 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Camera",textInElementFromAppReportTabListWithIndex4AfterSwipe0);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe0 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Chrome",textInElementFromAppReportTabListWithIndex6AfterSwipe0);
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_third);
        CommonNavigationUI.hardSwipe(1020);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Chrome");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe1 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Clock",textInElementFromAppReportTabListWithIndex2AfterSwipe1);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe1 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","DNA2Style",textInElementFromAppReportTabListWithIndex4AfterSwipe1);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe1 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Drive",textInElementFromAppReportTabListWithIndex6AfterSwipe1);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe1 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Email",textInElementFromAppReportTabListWithIndex8AfterSwipe1);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Email");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe2 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Facebook",textInElementFromAppReportTabListWithIndex2AfterSwipe2);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe2 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Galaxy Apps",textInElementFromAppReportTabListWithIndex4AfterSwipe2);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe2 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Gallery",textInElementFromAppReportTabListWithIndex6AfterSwipe2);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe2 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Gmail",textInElementFromAppReportTabListWithIndex8AfterSwipe2);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Gmail");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe3 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Google+",textInElementFromAppReportTabListWithIndex2AfterSwipe3);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe3 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Internet",textInElementFromAppReportTabListWithIndex4AfterSwipe3);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe3 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Memo",textInElementFromAppReportTabListWithIndex6AfterSwipe3);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe3 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","My Files",textInElementFromAppReportTabListWithIndex8AfterSwipe3);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("My Files");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe4 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","Play Books",textInElementFromAppReportTabListWithIndex2AfterSwipe4);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe4 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Play Games",textInElementFromAppReportTabListWithIndex4AfterSwipe4);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe4 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Play Music",textInElementFromAppReportTabListWithIndex6AfterSwipe4);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe4 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Play Store",textInElementFromAppReportTabListWithIndex8AfterSwipe4);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Play Store");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application", "Play Фильмы",textInElementFromAppReportTabListWithIndex2AfterSwipe5);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Radio",textInElementFromAppReportTabListWithIndex4AfterSwipe5);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Settings",textInElementFromAppReportTabListWithIndex6AfterSwipe5);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe5 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Smart Manager",textInElementFromAppReportTabListWithIndex8AfterSwipe5);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Smart Manager");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","SuperSU",textInElementFromAppReportTabListWithIndex2AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Telegram",textInElementFromAppReportTabListWithIndex4AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","UBANK",textInElementFromAppReportTabListWithIndex6AfterSwipe6);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe6 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Voice Recorder",textInElementFromAppReportTabListWithIndex8AfterSwipe6);
        CommonNavigationUI.hardSwipe(1000);
        AppsReportTabObject.swipeUpToElementNotPresentAccuracy("Voice Recorder");
        String textInElementFromAppReportTabListWithIndex2AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(0);
        assertEquals("We see unexpected application","YouTube",textInElementFromAppReportTabListWithIndex2AfterSwipe7);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","YT Kids",textInElementFromAppReportTabListWithIndex4AfterSwipe7);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe7 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Видео",textInElementFromAppReportTabListWithIndex6AfterSwipe7);
        CommonNavigationUI.hardSwipe(1200);
        CommonNavigationUI.hardSwipe(1200);
        String textInElementFromAppReportTabListWithIndex2AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(1);
        assertEquals("We see unexpected application","Магазин",textInElementFromAppReportTabListWithIndex2AfterSwipe8);
        String textInElementFromAppReportTabListWithIndex4AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(2);
        assertEquals("We see unexpected application","Настройки Google",textInElementFromAppReportTabListWithIndex4AfterSwipe8);
        String textInElementFromAppReportTabListWithIndex6AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(3);
        assertEquals("We see unexpected application","Новости",textInElementFromAppReportTabListWithIndex6AfterSwipe8);
        String textInElementFromAppReportTabListWithIndex8AfterSwipe8 = AppsReportTabObject.getElementTextFromAppsList(4);
        assertEquals("We see unexpected application","Ситимобил",textInElementFromAppReportTabListWithIndex8AfterSwipe8);

    }*/





