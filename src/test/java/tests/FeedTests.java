package tests;

import lib.CoreTestCase;
import lib.CoreTestCaseForParallelSessions;
import lib.Platform;
import lib.VideoRecorder;
import lib.ui.*;
import lib.ui.factories.*;
import org.junit.Test;
import org.openqa.selenium.TimeoutException;


public class FeedTests extends CoreTestCaseForParallelSessions {

    //+ios
    @Test
    public void testOpenURL() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            if (Platform.getInstance().isAndroid()) {
                FeedPageObject.waitForFeedList();
                CommonNavigationUI.waitUntilBlockButtonEnabled("Block");
                CommonNavigationUI.swipeUpToElementPresent("fuq.com");
                CommonNavigationUI.tapOnElement("fuq.com");
                CommonNavigationUI.waitForTheTextOnTheOpenWebPage("Most Popular Porn Categories");
                this.navigateBack();
                CommonNavigationUI.waitForElementPresentByTextContains("playboy.com");
            } else {
                CommonNavigationUI.waitForIOSElementPresentByText("Chrome");
                FeedPageObject.swipeToFuqCom();
                CommonNavigationUI.tapOnElement("Blocked: Porn Nudity www.fuq.com");
                CommonNavigationUI.waitForElementPresentByTextContains("Most Popular Porn Categories");
                CommonNavigationUI.tapOnElement("Done");
                CommonNavigationUI.waitForElementPresentByTextContains(
                        "Blocked: Porn www.playboy.com");
            }
        } finally {
            recorder.stopRecording(methodName);
        }
    }

    //+ios
    @Test
    public void testExpandingCell() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            if (Platform.getInstance().isAndroid()) {
                FeedPageObject.waitForFeedList();
                CommonNavigationUI.waitUntilBlockButtonEnabled("Block");
                CommonNavigationUI.swipeUpToElementPresent("kubernetes");

            } else {
                CommonNavigationUI.waitForIOSElementPresentByText("Chrome");
            }
            CommonNavigationUI.expandCell("Chrome");
            CommonNavigationUI.waitForChromeDescription();
            if (Platform.getInstance().isAndroid()) {
                CommonNavigationUI.swipeUpToElementPresent("faust");
            } else {
                CommonNavigationUI.swipeToIOSElementQuick();
            }
            CommonNavigationUI.swipeDownToElementPresent("Chrome");
            CommonNavigationUI.waitForChromeDescription();
        } finally {
            recorder.stopRecording(methodName);
        }
    }


    @Test
    public void testBlockingApplications() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            if (Platform.getInstance().isAndroid()) {
                FeedPageObject.waitForFeedList();
                CommonNavigationUI.waitUntilBlockButtonEnabled("Block");
            } else {
                CommonNavigationUI.waitForIOSElementPresentByText("Chrome");
            }
            CommonNavigationUI.switchToAllow("Chrome");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("Chrome");
            CommonNavigationUI.swipeUpToElementPresent("Go to App Settings");
            CommonNavigationUI.tapOnElement("Go to App Settings");
            CommonNavigationUI.waitForTheElementToAppearInTheAppSettings("After School");
            CommonNavigationUI.swipeUpToElementPresent("Chrome");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("Chrome");
            CommonNavigationUI.initBackClick();
            CommonNavigationUI.switchToBlock("Chrome");
            CommonNavigationUI.waitForAttributeOfTheBlockButtonStayTrue("Chrome");
            CommonNavigationUI.swipeUpToElementPresent("Go to App Settings");
            CommonNavigationUI.tapOnElement("Go to App Settings");
            CommonNavigationUI.waitForElementPresentByText("After School");
            CommonNavigationUI.swipeUpToElementPresent("Chrome");
            CommonNavigationUI.waitForAttributeOfTheBlockButtonStayTrue("Chrome");
            CommonNavigationUI.switchToAllow("Chrome");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("Chrome");
            CommonNavigationUI.initBackClick();
            CommonNavigationUI.switchToAllow("Chrome");
            CommonNavigationUI.waitForAttributeOfTheAllowButtonStayTrue("Chrome");
        } finally {
            recorder.stopRecording(methodName);
        }
    }


    // + ios
    @Test
    public void testSearchEvent() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            if (Platform.getInstance().isAndroid()) {
                FeedPageObject.waitForFeedList();
                CommonNavigationUI.waitUntilBlockButtonEnabled("Block");
                CommonNavigationUI.swipeUpToElementPresent("Searched: faust");
                CommonNavigationUI.waitForElementPresentByTextContains("Searched: richard, wagner");
                CommonNavigationUI.waitForElementPresentByTextContains("Richie", "SM-G531H");
                CommonNavigationUI.waitForElementPresentByTextContains("3/30/19");
            } else {
                CommonNavigationUI.waitForIOSElementPresentByText("Chrome");
                FeedPageObject.swipeToChaikovskySearchItem();
                CommonNavigationUI.waitForIOSElementPresentByText("Search: chaikovsky");
                CommonNavigationUI.waitForIOSElementPresentByText("Richie • 2019.01.25 at 11:20 AM • SM-G531H");
            }
        } finally {
            recorder.stopRecording(methodName);
        }

    }

    @Test
    public void testAppUsageСellAppsVisibility() throws Exception {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        VideoRecorder recorder = new VideoRecorder();
        recorder.startRecording();
        try {
            AuthPageObject AuthPageObject = AuthPageObjectFactory.get(driver_second);
            AuthPageObject.authFor("test87@gmail.com");
            FeedPageObject FeedPageObject = FeedPageObjectFactory.get(driver_second);
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver_second);
            if (Platform.getInstance().isAndroid()) {
                FeedPageObject.waitForFeedList();
                CommonNavigationUI.waitUntilBlockButtonEnabled("Block");
            } else {
                CommonNavigationUI.waitForIOSElementPresentByText("Chrome");
            }
            CommonNavigationUI.waitForElementPresentByTextContains(
                    "Richie app usage summary", "Mar 30 1:33 PM - 5:33 PM");
            CommonNavigationUI.waitForElementPresentByTextContains(
                    "These apps appear to have been used since the last app usage report");
            CommonNavigationUI.waitForElementPresentByText("Chrome");
            CommonNavigationUI.waitForElementPresentByText("Go to App Settings");
         } finally {
            recorder.stopRecording(methodName);
        }


    }
}
