package lib.ui;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

abstract public class AuthPageObject extends MainPageObject {

    public AuthPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

            allowButton,
            termsOfServiceElement,
            privacyPolicyElement,
            getStartedButton,
            signInTabElement,
            emailFieldElement,
            passwordFieldElement,
            loginButtonElement,
            forgotPasswordElement,
            textInEmailField,
            textInPasswordField,
            indicatorSpace,
            errorMessageElement,
            someElement;



    public void waitForErrorMessage()
    {
        this.waitForElementPresent(
                errorMessageElement, "errorMessageElement not present!", 20);
    }

    public void clearPasswordField()
    {
        this.waitForElementAndClear(passwordFieldElement, "passwordFieldElement not found or not cleared", 20);
    }

    public WebElement errorMessage()
    {
        return this.waitForElementPresent(errorMessageElement, "errorMessageElement not present", 20);
    }
    public String textInErrorMessage()
    {
        return errorMessage().getAttribute("text");
    }


    public void authFor(String account) throws Exception {

        if (Platform.getInstance().isAndroid()) {
            try {
                this.waitForElementAndClick(
                        someElement,
                        "welcome message not present", 15);
            } catch
            (TimeoutException e) {
                System.out.println();
            }
            this.waitForElementAndClick(getStartedButton, "button 'GET STARTED' not found", 60);
            this.waitForElementAndClick(signInTabElement, "signInTab not found!", 60);
            this.waitForElementAndSendKeys(emailFieldElement, account, "field with email not found", 60);
            driver.hideKeyboard();
            this.waitForElementPresent(textInEmailField, "textInEmailField not present");
            this.waitForElementAndSendKeys(passwordFieldElement, "111111", "field with password not found", 20);
            driver.hideKeyboard();
            this.waitForElementPresent(textInPasswordField, "getTextInPasswordField not present!");
            this.waitForElementAndClick(loginButtonElement, "LoginButton not found", 20);


        } else if (Platform.getInstance().isIOS()) {
            this.waitForElementAndClick(allowButton, "allowButton not found!", 30);
            try {
                this.waitForElementVisibilityAndClick(
                        someElement,
                        "welcome message not present", 10);
            } catch
            (TimeoutException e) {
                System.out.println();
            }
            this.waitForElementAndClick(getStartedButton, "button 'GET STARTED' not found", 60);
            Thread.sleep(1000);
            this.waitForElementAndClick(signInTabElement, "signInTab not found!", 60);
            this.waitForElementAndSendKeys(emailFieldElement, account, "field with email not found", 60);
            this.waitForElementPresent(textInEmailField, "textInEmailField not present");
            this.waitForElementAndSendKeys(passwordFieldElement, "111111", "field with password not found", 20);
            this.waitForElementPresent(textInPasswordField, "getTextInPasswordField not present!");
            this.waitForElementAndClick(loginButtonElement, "LoginButton not found", 20);
        }

    }

    public void skipWelcomeMessage()
    {
        try{
            this.waitForElementVisibilityAndClick(
                    someElement,
                    "welcome message not present", 10);
        }catch
        (TimeoutException e) {
            System.out.println();
        }
    }






    public void getStartedClick()
    {
        this.waitForElementAndClick(getStartedButton, "button 'GET STARTED' not found", 60);
    }
    public void termsOfServiceClick()
    {
        this.waitForElementAndClick(termsOfServiceElement, "Terms of Service not found!", 20);
    }
    public void privacyPolicyClick()
    {
        this.waitForElementAndClick(privacyPolicyElement, "'privacyPolicy' button not found!", 20);
    }
    public void switchToSignIn()
    {
        this.waitForElementAndClick(signInTabElement, "signInTab not found!", 20);
    }
    public void forgotPasswordClick()
    {
        this.waitForElementAndClick(forgotPasswordElement, "'forgotPassword' button not found!", 20);
    }
    public void sendKeysInEmailField(String email)
    {
        this.waitForElementAndSendKeys((emailFieldElement), email, "field with email not found", 20);
    }
    public void sendKeysInPassField(String value)
    {
        this.waitForElementAndSendKeys(passwordFieldElement, value, "field with password not found", 20);
    }
    public void loginButtonClick()
    {
        this.waitForElementAndClick(loginButtonElement, "LoginButton not found", 20);
    }







}


/*
        MainPageObject.waitForElementAndClick
                (By.xpath("//*[@text='GET STARTED']"), "button 'GET STARTED' not found", 20);

                MainPageObject.waitForElementAndClick
                (By.id("com.contentwatch.ghoti.cp2.parent:id/signInTab"), "signInTab not found!", 20);

                MainPageObject.waitForElementAndSendKeys
                (By.xpath("//*[@text='Enter e-mail']"), "test24.09.2@esc.tech", "field with email not found", 20);
                driver.hideKeyboard();


                MainPageObject.waitForElementAndSendKeys
                (By.id("com.contentwatch.ghoti.cp2.parent:id/signInPassword"), "111111", "field with password not found", 20);
                driver.hideKeyboard();

                MainPageObject.waitForElementAndClick
                (By.id("com.contentwatch.ghoti.cp2.parent:id/signIn"), "LoginButton not found", 20);*/
