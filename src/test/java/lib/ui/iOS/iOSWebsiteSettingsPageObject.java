package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.WebsiteSettingsPageObject;

public class iOSWebsiteSettingsPageObject extends WebsiteSettingsPageObject {
    public iOSWebsiteSettingsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {

        testComUrl = "xpath://*[@text='test.com']";

        deleteIconTemplate = "xpath://*[@text='fuq.com']/following-sibling::android.widget.ImageView[1]";
        selectedUrl = "xpath://*[@text='fuq.com']";
        addBlock = "id:com.contentwatch.ghoti.cp2.parent:id/addBlock";
        addAllow = "id:com.contentwatch.ghoti.cp2.parent:id/addAllow";
        okButton = "xpath://android.widget.Button[@text='OK']";
        pornhubUrl = "xpath://android.widget.TextView[@text='pornhub.com']";
        inputField = "id:com.contentwatch.ghoti.cp2.parent:id/edit";
        pornhubIsAlreadyAddedToElement = "xpath://*[contains(@text, 'pornhub.com is already added to')]";
        moveToAlwaysAllowListButton = "xpath://*[@text='Move to “Always Allow” list']";
        descriptionElement=  "id:com.contentwatch.ghoti.cp2.parent:id/description";
        addBlockElement= "id:com.contentwatch.ghoti.cp2.parent:id/addBlock";

        amountOfBlockElements = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/alwaysBlock']/android.widget.LinearLayout";
        amountOfAllowElements = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/alwaysAllow']/android.widget.LinearLayout";

        alwaysBlockList = "id:com.contentwatch.ghoti.cp2.parent:id/alwaysBlock";
        alwaysAllowList = "id:com.contentwatch.ghoti.cp2.parent:id/alwaysAllow";
        deleteButton = "xpath://*[@text='pornhub.com']/following-sibling::android.widget.ImageView[1]";
        urlTemplate = "xpath://android.widget.TextView[@text='{value}']";
    }
}
