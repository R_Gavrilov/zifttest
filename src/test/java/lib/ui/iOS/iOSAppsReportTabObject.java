package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.AppsReportTabObject;

public class iOSAppsReportTabObject extends AppsReportTabObject {
    public iOSAppsReportTabObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        appListElement = "xpath://*[@text='{value}']/../../../*";
        tabAlphabetically = "id:com.contentwatch.ghoti.cp2.parent:id/tabAlphabetically";
        elementsFromAppsList = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/list']//android.widget.TextView";
    }
}
