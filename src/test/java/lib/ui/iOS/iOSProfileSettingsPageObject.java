package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ProfileSettingsPageObject;

public class iOSProfileSettingsPageObject extends ProfileSettingsPageObject {
    public iOSProfileSettingsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        femaleGenderButton = "id:F";
        textField = "classChain:**/XCUIElementTypeTextField[1]";
        doneButtonElement = "classChain:**/XCUIElementTypeButton[`name == 'Done'`][1]";
        selectedGenderElement = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/sexMale']/../android.widget.Button[@selected='true']";
        maleGenderButton = "id:com.contentwatch.ghoti.cp2.parent:id/sexMale";
        ageButton = "classChain:**/XCUIElementTypeTextField[2]";
        elementFromDropDownList_TPL = "xpath://*[@text='{value}']";
        avatarElement = "id:com.contentwatch.ghoti.cp2.parent:id/avatar";
        elementFromDropDownListWithScroll_TPL = "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                ".scrollIntoView(new UiSelector().text(\"{value2}\"))";
        iosPicker = "predicateString:type == 'XCUIElementTypePickerWheel'";
        pickerDoneButton = "classChain:**/XCUIElementTypeButton[`name == 'Done'`][2]";

    }
}
