package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.SearchesTabObject;

public class iOSSearchesTabObject extends SearchesTabObject {
    public iOSSearchesTabObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        searchEvent = "xpath://*[@text='6:19 AM · SM-G531H']";
        loupeElement = "xpath://*[@text='8:01 AM · SM-G531H']/../..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/icon']";
    }
}
