package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ContentFilterPageObject;

public class iOSContentFilterPageObject extends ContentFilterPageObject {
    public iOSContentFilterPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static
    {
        templateAllowButton = "xpath://*[@text='%s']/following-sibling::android.widget.TextView[@text='Allow']";
        templateBlockButton = "xpath://*[@text='%s']/following-sibling::android.widget.TextView[@text='Block']";
        templateAlertButton = "xpath://*[@text='%s']/following-sibling::android.widget.TextView[@text='Alert']";
        howItWorksLink = "id:com.contentwatch.ghoti.cp2.parent:id/howItWorks";
        closeAlertElement = "id:com.contentwatch.ghoti.cp2.parent:id/close";
        weaponsElement = "id:Weapons";
        weaponsBlockElement = "xpath://XCUIElementTypeCell[14]/XCUIElementTypeButton[@name='Block']";
    }
}
