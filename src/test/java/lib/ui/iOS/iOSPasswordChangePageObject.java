package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.PasswordChangePageObject;

public class iOSPasswordChangePageObject extends PasswordChangePageObject {
    public iOSPasswordChangePageObject(AppiumDriver driver) {
        super(driver);
    }

    static {


        currentPasswordField = "xpath://XCUIElementTypeSecureTextField[1]";
        newPasswordField = "xpath://XCUIElementTypeSecureTextField[2]";
        repeatPasswordField = "xpath://XCUIElementTypeSecureTextField[3]";
        doneButton = "id:Done";
        errorElement = "xpath://*[@name='Fail']//XCUIElementTypeStaticText[2]";
        confirmIOSElementAlert = "id:OK";
        newPasswordClearElement = "xpath://*[@name='RemoveWebsiteSettings'][2]";
        repeatPasswordClearElement = "xpath://*[@name='RemoveWebsiteSettings'][3]";
        currentPasswordClearElement = "xpath://*[@name='RemoveWebsiteSettings'][1]";

    }


}
