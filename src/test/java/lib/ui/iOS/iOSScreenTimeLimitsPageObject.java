package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ScreenTimeLimitsPageObject;

public class iOSScreenTimeLimitsPageObject extends ScreenTimeLimitsPageObject {
    public iOSScreenTimeLimitsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        weeklyScreenTimeValue = "id:com.contentwatch.ghoti.cp2.parent:id/specificTime";
        numPadElementTemplate = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/numPad']/*[@text='%s']";
        checkBoxElement = "id:com.contentwatch.ghoti.cp2.parent:id/unlimited";
        numPadButton0 = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/numPad']/*[@text='0']";
        numPadButton1 = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/numPad']/*[@text='1']";
        backSpaceButton = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/numPad']/*[@ NAF ='true']";
        setNewScreenTimeButton = "id:com.contentwatch.ghoti.cp2.parent:id/apply";
        mondayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay1Specific";
        tuesdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay2Specific";
        wednesdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay3Specific";
        thursdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay4Specific";
        fridayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay5Specific";
        saturdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay6Specific";
        sundayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay0Specific";
        numPadButton2 = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/numPad']/*[@text='2']";
        numPadButton4 = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/numPad']/*[@text='4']";
    }
}
