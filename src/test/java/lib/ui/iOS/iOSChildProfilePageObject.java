package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ChildProfilePageObject;

public class iOSChildProfilePageObject extends ChildProfilePageObject {
    public iOSChildProfilePageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
                avatarElement = "xpath://*[@name='SettingsChildScreen']/..";
                childNameElement = "xpath://XCUIElementTypeNavigationBar/XCUIElementTypeStaticText";
                initGearElement = "xpath://*[@text='{value}']//..//../android.widget.ImageView";
                blockRadioButton = "xpath://*[@text='Block']";
                allowRadioButton = "xpath://*[@text='Allow']";
                okButtonElement = "xpath://android.widget.Button[@text='OK']";
                childSettingsElement = "id:SettingsHeaderIcon";
                calendarButton = "id:RulePlannerHeaderIcon";
                editTimerRunningButton = "xpath:(//XCUIElementTypeButton[@name='pencil gray'])[2]";
                ruleSelectionMethodElement = "id:com.contentwatch.ghoti.cp2.parent:id/origination";
                currentRuleTitle = "id:com.contentwatch.ghoti.cp2.parent:id/currentRuleTitle";
                playPauseButton = "id:ic play pause large";
                resumeStandardElement = "xpath://XCUIElementTypeStaticText[(@name='Standard') or (@name='Resume Standard')]";
                calendarResumeScheduleElement = "id:Resume Schedule";
                initPlayPausePauseDeviceElement = "id:Pause Device";
                calendarActionSheetRunTimerElement = "xpath://*[contains(@name,'Timer Running') or contains(@name,'Run timer') or contains(@name,'Run Timer (No Internet')]";
                ruleSelectionMethodElement2 = "xpath://XCUIElementTypeStaticText[contains(@name,'%s')]";
                selectionCurrentRuleElement = "xpath://XCUIElementTypeStaticText[@name='%s']";
                screenTimeTodayLeftElement = "id:com.contentwatch.ghoti.cp2.parent:id/screenTimeLeft";

                blocksAlertsTab = "xpath://*[@name='BlocksTabIcon']//..";
                searchesTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[1]";
                appsTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[1]";
                screenTimeTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[2]";
                locationTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[4]";
                editScreenTime = "id:com.contentwatch.ghoti.cp2.parent:id/editScreenTime";

                elementsInPlayPauseActionSheet = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/design_bottom_sheet']/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout";
                selectionCurrentRuleElement2 = "";
                selectionMethodElement = "";
    }

    }
