package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.RuleTimerPageObject;

public class iOSRuleTimerPageObject extends RuleTimerPageObject {
    public iOSRuleTimerPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
        selectRuleElement = "xpath:(//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton)[1]";
        ruleTemplate = "xpath://XCUIElementTypeStaticText[@name='%s']";
        numPadButton1 = "id:1";
        numPadButton2 = "id:2";
        numPadButton0 = "id:0";
//        selectedRuleElement = "xpath://*[(@name='Paused') or (@name='No Internet')  or (@name='Standard')]";

        selectedTimeElement = "xpath:(//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton)[2]";
        startTimerButton = "id:Start Timer!";
        ruleXpath = "";
    }
}
