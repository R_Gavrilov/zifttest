package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.FeedPageObject;

public class iOSFeedPageObject extends FeedPageObject {
    public iOSFeedPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static
    {
            tongueTopElement = "id:backTongue";
            childProfileElement = "classChain:**/XCUIElementTypeCollectionView/XCUIElementTypeCell";
            EllipsesMenuButton = "id:EllipsesMenu";
            generalSettingsElement = "id:General Settings";
            subscriptionElement = "id:Subscription";
            helpElement = "id:Help";
            aboutElement = "id:About";
            parentPortalElement = "id:Parent Portal";
            changePasswordElement = "id:Change Password";
            logoutElement = "id:Logout";
            fuqComXpath = "xpath://XCUIElementTypeCell[13]";
            chaikovskySearchItem = "xpath://*[contains(@name,'chaikovsky')]/..";
            chaikovskySearchItemIcon = "xpath://XCUIElementTypeCell[6]/*[@name='FeedSearchIcon']";


    }
}
