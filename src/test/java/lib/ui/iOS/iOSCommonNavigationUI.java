package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.CommonNavigationUI;

public class iOSCommonNavigationUI extends CommonNavigationUI {
    public iOSCommonNavigationUI(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        someElement = "id:{value}";

//        someElementIOS = "xpath://*[contains(@name,'{value}')]";
        backElement = "id:com.contentwatch.ghoti.cp2.parent:id/back";
        ziftElementTextTemplate = "id:{value}";
//        ziftElementTextTemplate = "xpath://*[contains(@name,'{value}')]";
        blurDestination = "id:com.contentwatch.ghoti.cp2.parent:id/blurDestination";
        blurSeeMore = "id:com.contentwatch.ghoti.cp2.parent:id/blurSeeMore";
        keepZiftFreeButton = "id:cancel-btn";
        calendarButton = "id:com.contentwatch.ghoti.cp2.parent:id/schedule";

        elementsInCalendarActionSheet =
                "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/design_bottom_sheet']/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout";
        cellElement = "";
        templateCellElement = "id:%s";
//        templateCellElement = "xpath://*[@name='%s']";
        photoSharingIOS = "id:Photo Sharing";
        locationTrackingIOS = "id:Location Tracking";
        matureContentIOS = "id:Mature Content";
        chatIOS = "id:Chat";
        liveStreamingIOS = "id:Live Streaming";
        strangerDangerIOS = "id:Stranger Danger";
        inAppPurchasesIOS = "id:In App Purchases";
        someElementForContainsMethod1 = "xpath://*[contains(@name,'{value}')";
        someElementForContainsMethod2 = " and contains(@name,'{value2}')]";
    }

}
