package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.RestrictionsPageObject;

public class iOSRestrictionsPageObject extends RestrictionsPageObject {
    public iOSRestrictionsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        appSettingsElement = "id:com.contentwatch.ghoti.cp2.parent:id/appSettings";
        contentFilterElement = "id:Content Filter";
        urlSettingsElement= "id:com.contentwatch.ghoti.cp2.parent:id/urlSettings";
    }
}
