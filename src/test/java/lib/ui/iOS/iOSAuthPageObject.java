package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.AuthPageObject;

public class iOSAuthPageObject extends AuthPageObject {
    public iOSAuthPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static
    {
                allowButton = "id:Allow";
                getStartedButton = "id:GET STARTED";
                signInTabElement = "xpath://XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton";
                emailFieldElement = "predicateString:type == 'XCUIElementTypeTextField'";
                textInEmailField = "classChain:**/XCUIElementTypeTextField[`value CONTAINS 'esc.tech' OR value CONTAINS 'gmail.com'`]";
                passwordFieldElement = "predicateString:type == 'XCUIElementTypeSecureTextField'";
                textInPasswordField = "classChain:**/XCUIElementTypeSecureTextField[`value == '••••••'`]";
                loginButtonElement = "id:Login";
                indicatorSpace = "xpath://*[@value='page 1 of 7']";
                someElement = "xpath://*[@name='Continue']";
    }
}
