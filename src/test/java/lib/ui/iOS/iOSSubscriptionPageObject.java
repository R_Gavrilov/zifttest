package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.SubscriptionPageObject;

public class iOSSubscriptionPageObject extends SubscriptionPageObject {
    public iOSSubscriptionPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        subscribeButton = "id:Subscribe for $4.99/month";
        existingAppleId = "id:Use Existing Apple ID";
        subscribeButtonPopUp = "xpath://android.widget.Button[@content-desc='Subscribe']";
        inputField = "id:com.android.vending:id/input";
        unsubscribeButton = "id:com.contentwatch.ghoti.cp2.parent:id/cancelSubscription";
        cancelSubscriptionButton = "xpath://android.widget.Button[@text='CANCEL SUBSCRIPTION']";
        declineToAnswer = "xpath://android.widget.RadioButton[@text='Decline to answer']";
        continueButton = "xpath://android.widget.Button[@text='CONTINUE']";
        cancelSubscriptionButton2 = "xpath://android.widget.Button[@text='CANCEL SUBSCRIPTION']";
        restoreButton = "xpath://android.widget.Button[@text='RESTORE']";
        ziftPremiumElement = "xpath://*[@text='Zift Premium']";
        termsOfUseElement = "id:com.contentwatch.ghoti.cp2.parent:id/termsOfUse";
        privacyPolicyElement = "id:com.contentwatch.ghoti.cp2.parent:id/privacyPolicy";
        updateElement = "xpath://*[@text='UPDATE']";
    }
}
