package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ScreenTimeManagementPageObject;

public class iOSScreenTimeManagementPageObject extends ScreenTimeManagementPageObject {
    public iOSScreenTimeManagementPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {

        unlimCheckBoxElement = "id:com.contentwatch.ghoti.cp2.parent:id/unlimited";
        screenTimeTodayLeftElement = "id:com.contentwatch.ghoti.cp2.parent:id/todayLeft";
        plusMinusButtonTemplate = "id:com.contentwatch.ghoti.cp2.parent:id/%s";
        screenTimeOverride = "id:com.contentwatch.ghoti.cp2.parent:id/text";
        saturdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay6";
        mondayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay1";
        tuesdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay2";
        wednesdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay3";
        thursdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay4";
        fridayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay5";
        sundayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay0";
    }
}
