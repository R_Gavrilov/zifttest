package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.BlocksAndAlertsObject;

public class iOSBlocksAlertsPageObject extends BlocksAndAlertsObject {
    public iOSBlocksAlertsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static
    {
        iconW1 = "xpath://*[@name='{value}']";
        iconW2 = "/following-sibling::*[contains(@text, '{value2}')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/icon']";
        exclamationIconElement1 = "xpath://*[@text='{value}']";
        exclamationIconElement2 = "/following-sibling::*[contains(@text, '{value2}')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/mark']";
        gearIconElement1 = "xpath://*[@text='{value}']";
        gearIconElement2 = "/following-sibling::*[contains(@text, '{value2}')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/properties']";
        //*[@text='Viewed: Porn']/following-sibling::*[contains(@text, 'www.playboy.com')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/mark']
        gearIcon = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/list']/android.widget.LinearLayout[@index='1']//android.widget.ImageView[@index='2']";
        alwaysBlock = "id:com.contentwatch.ghoti.cp2.parent:id/alwaysBlock";
        applyButton = "xpath://*[@text='APPLY']";
        elementsFromBlocksAlertsList = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/list']//android.widget.TextView";
        iOSAlertIconW = "xpath://*[@name='BlocksAlertLink']";
    }

}

