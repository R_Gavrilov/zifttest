package lib.ui;

import io.appium.java_client.AppiumDriver;
import lib.ui.factories.CommonNavigationUiFactory;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

abstract public class ScreenTimeLimitsPageObject extends MainPageObject {

    public ScreenTimeLimitsPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String
            weeklyScreenTimeValue,
            numPadElementTemplate,
            checkBoxElement,
            numPadButton0,
            numPadButton1,
            numPadButton9,
            backSpaceButton,
            setNewScreenTimeButton,
            mondayElement,
            tuesdayElement,
            wednesdayElement,
            thursdayElement,
            fridayElement,
            saturdayElement,
            sundayElement,
            numPadButton2,
            numPadButton4,
            OKAlertButton;


    private static String numPadButton = "";

/*     public static void getNumPadElementXpath(String value)
       {
            String s = String.format(numPadElementTemplate, value);
            numPadButton = s;
}*/


    public void setUnlimitedScreenTimeForWeek() {
        this.turnSwitcherON();
        this.waitForElementAndClick(sundayElement, "", 10);
        this.waitForElementAndClick(mondayElement, "", 10);
        this.waitForElementAndClick(tuesdayElement, "", 10);
        this.waitForElementAndClick(wednesdayElement, "", 10);
        this.waitForElementAndClick(thursdayElement, "", 10);
        this.waitForElementAndClick(fridayElement, "", 10);
//        this.setNewScreentimeClick();
    }

    public boolean waitForCheckboxUnlimitedOn() {
        return this.waitForUnlimitedCheckboxAttributeTrue(checkBoxElement, "attribute 'checked' != true", 5);
    }

    public boolean waitForCheckboxUnlimitedOff() {
        return this.waitForUnlimitedCheckboxAttributeFalse(checkBoxElement, "attribute 'checked' != false", 5);
    }

    public void unlimCheckBoxClick()
    {
        this.waitForElementAndClick(checkBoxElement, "'unlimCheckBox' not found!", 20);
    }

    public void turnSwitcherON() {
        try {
            this.waitForCheckboxUnlimitedOn();
        } catch (TimeoutException e) {
            this.unlimCheckBoxClick();
            this.waitForCheckboxUnlimitedOn();
        }
    }

    public void turnSwitcherOFF() {
        try {
            this.waitForCheckboxUnlimitedOff();
        } catch (TimeoutException e) {
            this.unlimCheckBoxClick();
            this.waitForCheckboxUnlimitedOff();
        }
    }

    public boolean verifyThatNumPadElementDisabled(String element) {
        String s = String.format(numPadElementTemplate, element);
        numPadButton = s;
        return this.waitForElementAttributeEnabledFalse((numPadButton), "attribute 'Enabled' != false", 20);
    }
    public boolean verifyThatNumPadElementEnabled(String element) {
        String s = String.format(numPadElementTemplate, element);
        numPadButton = s;
        return this.waitForElementAttributeEnabledTrue((numPadButton), "attribute 'Enabled' != true", 20);
    }
/*    public WebElement initNumPadButton()
    {
        return this.waitForElementPresentByText(By.xpath(numPadButton), "numPadButton not found!", 20);
    }*/


    public WebElement initSpecificTimeElement() {
        return  this.waitForElementPresent(
                weeklyScreenTimeValue, "SpecificTimeElement not found!", 20);
    }
    public String getWeeklyScreenTimeElementText() {
        WebElement specificTime = initSpecificTimeElement();
        return specificTime.getAttribute("text");
    }

    public void initUnlimCheckBoxClick() {
        this.waitForElementAndClick(
                checkBoxElement, "'unlimCheckBox' not found!", 20);
    }

    public boolean waitForUnlimitedCheckboxAttributeChangeToTrue() {
        return this.waitForUnlimitedCheckboxAttributeTrue(
                checkBoxElement, "attribute 'checked' != true", 20);
    }

    public boolean waitForUnlimitedCheckboxAttributeChangeToFalse() {
        return this.waitForUnlimitedCheckboxAttributeFalse(
                checkBoxElement, "attribute 'checked' != false", 20);
    }

    public void initBackSpaceClick() {
        this.waitForElementAndClick(backSpaceButton, "'backSpaceButton' not found!", 20);
    }

    public void setNewScreentimeClick() {
        this.waitForElementAndClick(setNewScreenTimeButton, "'backSpaceButton' not found!", 20);
    }


    public void selectAllDaysToApplyTheseLimits() {
        this.waitForElementAndClick(mondayElement, "mondayElement not found!", 20);
        this.waitForElementAndClick(tuesdayElement, "tuesdayElement not found!", 20);
        this.waitForElementAndClick(wednesdayElement, "wednesdayElement not found!", 20);
        this.waitForElementAndClick(thursdayElement, "thursdayElement not found!", 20);
        this.waitForElementAndClick(fridayElement, "fridayElement not found!", 20);
        this.waitForElementAndClick(sundayElement, "sundayElement not found!", 20);
    }

    public void enterValue010() {
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 20);
        this.waitForElementAndClick(numPadButton1, "'numPadButton1' not found!", 20);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 20);
    }

    public void enter24h00m() {
        this.waitForElementAndClick(numPadButton2, "'numPadButton2' not found!", 20);
        this.waitForElementAndClick(numPadButton4, "'numPadButton4' not found!", 20);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 20);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 20);
    }

    public void enter00h00m() {
        this.waitForElementAndClick(numPadButton0, "'numPadButton2' not found!", 20);
        this.waitForElementAndClick(numPadButton0, "'numPadButton4' not found!", 20);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 20);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 20);
    }

    public void enter99h99m() {
        this.waitForElementAndClick(numPadButton9, "'numPadButton2' not found!", 20);
        this.waitForElementAndClick(numPadButton9, "'numPadButton4' not found!", 20);
        this.waitForElementAndClick(numPadButton9, "'numPadButton0' not found!", 20);
        this.waitForElementAndClick(numPadButton9, "'numPadButton0' not found!", 20);
    }

    public void enter19h19m() {
        this.waitForElementAndClick(numPadButton1, "'numPadButton2' not found!", 20);
        this.waitForElementAndClick(numPadButton9, "'numPadButton4' not found!", 20);
        this.waitForElementAndClick(numPadButton1, "'numPadButton0' not found!", 20);
        this.waitForElementAndClick(numPadButton9, "'numPadButton0' not found!", 20);
    }

    public void waitForAlertAndClickOK() {
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
        CommonNavigationUI.waitForElementPresentByTextContains(
                "Screen Time Allocations cannot set to 0. Please enter a desired time.");
        this.waitForElementAndClick(OKAlertButton, "OKAlertButton not present", 15);
    }








}
