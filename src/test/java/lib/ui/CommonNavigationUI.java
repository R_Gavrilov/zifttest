package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import lib.Platform;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.util.List;

import static io.appium.java_client.touch.offset.PointOption.point;

abstract public class CommonNavigationUI extends MainPageObject {
    public CommonNavigationUI(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

            someElement,
            someElementForContainsMethod1,
            someElementForContainsMethod2,
            backElement,
            ziftElementTextTemplate,
            blurDestination,
            blurSeeMore,
            keepZiftFreeButton,
            calendarButton,
            elementsInCalendarActionSheet,
            templateCellElement,
            cellElement,
            templateBlockButton,
            templateAllowButton,
            blockXpath,
            allowXpath,
            photoSharingIOS,
            locationTrackingIOS,
            matureContentIOS,
            chatIOS,
            liveStreamingIOS,
            strangerDangerIOS,
            inAppPurchasesIOS,
            contentDescElement;


    public void tapOnElement(String elementText){
        String selectedElement = selectedElement(elementText);
        this.waitForElementVisibilityAndClick(selectedElement, "'" + elementText + "'" + " element not found!", 20);
    }

    public String elementTextFromActionSheetWithIndex(int index)
    {
        List<WebElement> elementsFromActionSheet = this.findElements("xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/title']");
        return elementsFromActionSheet.get(index).getAttribute("text");
    }


    //старая реализация в конце класса
    public void keepZiftFree()
    {
        try{
            this.waitForElementVisibilityAndClick(
                    keepZiftFreeButton,
                    "'KEEP ZIFT FREE' button not found!", 6);

        } catch
                (TimeoutException e){
            System.out.println("some braze event not present!");
        }
    }


    public void expandCell(String cell)
    {
        String s = String.format(templateCellElement, cell);
        cellElement = s;
        WebElement element = this.waitForElementPresent(
                cellElement, "cellElement " + cellElement + " not found", 40);
        Dimension size = element.getSize();
        int x = element.getLocation().getX() + size.getWidth() / 2;
        int y = element.getLocation().getY() + size.getHeight() / 2;
        TouchAction action = new TouchAction(driver);
        action.tap(point(x, y)).perform();
        try {
            this.waitForElementPresentByTextContains("Learn About", "App Advisor");
        } catch (TimeoutException e1) {
            System.out.println("text: 'Learn About this app on App Advisor' not present");
            try {
                this.waitForElementPresentByText("We have no review for this app yet");
            } catch (TimeoutException e2) {
                System.out.println("text: 'We have no review for this app yet' not present");
                try {
                    this.waitForElementPresentByText("Not installed");
                } catch (TimeoutException e3) {
                    System.out.println("text: 'Not installed' not present");
                }
            }
        }
    }



//    Boolean isCellExpanded = this.isElementLocatedOnTheScreenByTextIOS("Read the article on Zift Parent Portals");
//        if(!isCellExpanded){
//        System.out.println(isCellExpanded);
//        Thread.sleep(5000);
//        action.tap(x, y).perform();


//    public void hardSwipe(int end_y)
//    {
//        TouchAction action = new TouchAction(driver);
//               action.press(point(863, 1608))
//                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000))
//                        .moveTo(point(863, end_y))
//                        .release().perform();
//    }

    /*TEMPLATES METHODS*/
    private static String selectedElement(String value) {
        return someElement.replace("{value}", value);
    }

    /*TEMPLATES METHODS*/

    private static String ziftElement(String text)
    {
        return ziftElementTextTemplate.replace("{value}", text);
    }

    private static String ziftElementByContentDesc(String text)
    {
        return contentDescElement.replace("{value}", text);
    }

    public void waitForTheElementToAppearInTheAppSettings(String text)
    {
        String selectedElement = selectedElement(text);
        this.waitForElementVisibility(selectedElement, text + " element not found!", 60);
    }



    public void swipeUpToElementPresent(String value) {
        String selectedElement = selectedElement(value);
        if(Platform.getInstance().isAndroid()) {
            this.swipeUpToFindElement(selectedElement, value + " element not found!", 20);
        } else {
            this.swipeUpTillElementAppear(selectedElement, value + " element not found!", 60);
        }
    }


    public void swipeDownToElementPresent(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeDownToFindElement(elementFromAppListTab, value + " element not found!", 20);
    }

    public void swipeToElementNotPresentAccuracy(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpToElementNotPresentAccuracy(elementFromAppListTab, value + " element not found!", 20);
    }
    public void swipeToElementPresentVeryAccuracy(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpToElementPresentVeryAccuracy(elementFromAppListTab, value + " element not found!", 50);
    }

    public void swipeToElementPresentAccuracy(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpToElementPresentAccuracy(elementFromAppListTab, value + " element not found!", 50);
    }

    public void swipeToElementPresentAccuracyIOS(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpTillElementAppearAccuracy(elementFromAppListTab, value + " element not found!", 50);
    }




    public void initBackClick()
    {
        this.waitForElementAndClick(backElement, "'back' button not found!", 20);
    }


    public void waitForBlurDestinationElementPresent()
    {
        this.waitForElementPresent (blurDestination, "'blurDestination not found!'", 61);
    }
    public void waitForBlurDestinationElementNotPresent()
    {
        this.waitForElementNotPresent (blurDestination, "'blurDestination not found!'", 61);
    }

    public void waitForBlurSeeMorePresent()
    {
        this.waitForElementPresent(blurSeeMore, "'blurSeeMore' button not found!");
    }
    public void waitForBlurSeeMoreNotPresent()
    {
        this.waitForElementNotPresent(blurSeeMore, "'blurSeeMore' button not found!", 61);
    }




    public int amountOfElementsInCalendarActionSheet() {
        return this.getAmountOfElements(elementsInCalendarActionSheet);
    }

    public void initCalendarClick()
    {
        this.waitForElementAndClick(calendarButton, "calendarButton not found!", 20);
    }






//метод поиска по contains для iOS
    public void waitForIOSElementPresentByText(String text)
    {
        String selectedElement = selectedElement(text);
        Boolean isElementLocatedOnTheScreenForWait =  this.isElementLocatedOnTheScreenForWait(selectedElement);
        System.out.println(selectedElement + ": isElementLocatedOnTheScreen - "  + isElementLocatedOnTheScreenForWait);
/*
        this.waitForElementVisibility(selectedElement, text + " element not found!", 40);
*/
    }


    // поиск по строгому соответсвию: xpath://*[@text='{value}']
    public void waitForElementPresentByText(String text)
    {
        String selectedElement = selectedElement(text);
        this.waitForElementVisibility(selectedElement, text + " element not found!", 20);
    }
    //  поиск по нестрогому соответсвию: xpath://*[contains(@text,'{value}')]
    public void waitForElementPresentByTextContains(String text)
    {
        String elementText = ziftElement(text);
        this.waitForElementPresent(elementText, "element with " + elementText +  " not found!", 20);
    }

    public void waitForElementPresentByContentDescContains(String text)
    {
        String elementByContentDesc = ziftElementByContentDesc(text);
        this.waitForElementPresent(elementByContentDesc, "element with " + elementByContentDesc +  " not found!", 20);
    }

    public void waitForTheTextOnTheOpenWebPage(String text)
    {
        try{
            String elementText = ziftElement(text);
            this.waitForElementPresent(elementText, "element with " + elementText +  " not found!", 10);
        } catch (TimeoutException e) {
            String elementByContentDesc = ziftElementByContentDesc(text);
            this.waitForElementPresent(elementByContentDesc, "element with " + elementByContentDesc +  " not found!", 10);
        }
    }



    // составной локатор, осуществляющитй поиск по нестрогому соответствию xpath://*[contains(@text,'{value}') and contains(@text,'{value2}')]
    private static String selectedElementForContainsMethod(String text1) {
        return someElementForContainsMethod1.replace("{value}", text1);
    }
    private static String selectedElementForContainsMethod2(String text2) {
        return someElementForContainsMethod2.replace("{value2}", text2);
    }
    public void waitForElementPresentByTextContains(String text1, String text2)
    {
        String someElementForContainsMethod = selectedElementForContainsMethod(text1);
        String someElementForContainsMethod2 = selectedElementForContainsMethod2(text2);
        if (Platform.getInstance().isAndroid()) {
            this.waitForElementPresent((someElementForContainsMethod + someElementForContainsMethod2),
                    "element with " + someElementForContainsMethod + someElementForContainsMethod2 + " not found! ", 20);
        } else {
            Boolean isElementLocatedOnTheScreenForWait =  this.isElementLocatedOnTheScreenForWait((someElementForContainsMethod + someElementForContainsMethod2));
            System.out.println(someElementForContainsMethod + someElementForContainsMethod + ": isElementLocatedOnTheScreen - "  + isElementLocatedOnTheScreenForWait);
        }
    }






    //  так вышло
    public void waitForAfterSchoolDescription() {
        this.waitForElementPresentByTextContains("After School is a semi-anonymous social networking app",
                "Learn About After School on the App Advisor");
        this.waitForElementPresentByTextContains("Category: Social Networking");
        this.waitForElementPresentByTextContains("This App Contains:");
        this.waitForElementPresentByTextContains("Chat");
        this.waitForElementPresentByTextContains("Location Tracking");
        this.waitForElementPresentByTextContains("Mature Content");
        this.waitForElementPresentByTextContains("This App doesn", "t Contain:");
        this.waitForElementPresentByTextContains("In App Purchases");
        this.waitForElementPresentByTextContains("Live Streaming");
        this.waitForElementPresentByTextContains("Photo Sharing");
        this.waitForElementPresentByTextContains("Stranger Danger");
    }
    public void waitForAmazonGroupDescription() {
        this.waitForElementPresentByText("Amazon");
        this.waitForElementPresentByText("Prime Video");
        this.waitForElementPresentByText("Amazon Music");
        this.waitForElementPresentByText("Not installed");
    }

    public void waitForChromeDescription() {
        if (Platform.getInstance().isAndroid()) {
            try {
                this.waitForElementPresentByText("Mature Content");
            }
            catch (TimeoutException e) {
                this.swipeToElementPresentAccuracy("Mature Content");

                this.waitForElementPresentByTextContains("Photo Sharing");
                this.waitForElementPresentByText("In App Purchases");
                this.waitForElementPresentByText("Live Streaming");
                this.waitForElementPresentByText("Location Tracking");
                this.waitForElementPresentByText("Chat");
                this.waitForElementPresentByText("Stranger Danger");
                this.waitForElementPresentByTextContains("This App doesn", "t Contain:");
                this.waitForElementPresentByTextContains(
                        "The Google Chrome app is the mobile extension of the popular Chrome browser",
                        "Learn About Google Chrome: Fast & Secure on the App Advisor");
                this.waitForElementPresentByText("Category: Communication");
            }
        } else {
//             boolean elementVisibility = this.isElementLocatedOnTheScreen(photoSharingIOS);
//                if(elementVisibility == true) {
//                    this.waitForElementPresentByTextContains("Photo Sharing");
//                    this.waitForElementPresentByText("In App Purchases");
//                    this.waitForElementPresentByText("Live Streaming");
//                    this.waitForElementPresentByText("Location Tracking");
//                    this.waitForElementPresentByText("Chat");
//                    this.waitForElementPresentByText("This App doesn’t Contain:");
//                    this.waitForIOSElementPresentByText("Spotify is a music streaming app that");
//                } else if(elementVisibility == false) {
//
//                    this.swipeUpTillElementAppearAccuracy(photoSharingIOS, "photoSharingIOS not found!", 100);
//
//                    this.swipeToElementPresentAccuracyIOS("Stranger Danger");
//                    this.waitForElementPresentByTextContains("Photo Sharing");
//                    this.waitForElementPresentByText("In App Purchases");
//                    this.waitForElementPresentByText("Live Streaming");
//                    this.waitForElementPresentByText("Location Tracking");
//                    this.waitForElementPresentByText("Chat");
//                    this.waitForElementPresentByText("This App doesn’t Contain:");
//                    this.waitForIOSElementPresentByText("Spotify is a music streaming app that");
//                }
            boolean elementVisibility = this.isElementLocatedOnTheScreenByTextIOS("Location Tracking");
            if(!elementVisibility) { this.swipeToIOSElementWithTextAccuracy("Location Tracking"); }
            boolean elementVisibility1 = this.isElementLocatedOnTheScreenByTextIOS("Mature Content");
            if (!elementVisibility1) { this.swipeToIOSElementWithTextAccuracy("Mature Content"); }
            boolean elementVisibility2 = this.isElementLocatedOnTheScreenByTextIOS("Chat");
            if(!elementVisibility2) { this.swipeToIOSElementWithTextAccuracy("Chat"); }
            boolean elementVisibility3 = this.isElementLocatedOnTheScreenByTextIOS("Live Streaming");
            if(!elementVisibility3) { this.swipeToIOSElementWithTextAccuracy("Live Streaming"); }
            boolean elementVisibility4 = this.isElementLocatedOnTheScreenByTextIOS("Stranger Danger");
            if(!elementVisibility4) { this.swipeToIOSElementWithTextAccuracy("Stranger Danger"); }
            boolean elementVisibility5 = this.isElementLocatedOnTheScreenByTextIOS("Photo Sharing");
            if(!elementVisibility5) { this.swipeToIOSElementWithTextAccuracy("Photo Sharing"); }
            boolean elementVisibility6 = this.isElementLocatedOnTheScreenByTextIOS("In App Purchases");
            if(!elementVisibility6) { this.swipeToIOSElementWithTextAccuracy("In App Purchases"); }
            this.waitForElementPresentByTextContains("Location Tracking");
            this.waitForElementPresentByTextContains("Mature Content");
            this.waitForElementPresentByTextContains("Chat");
            this.waitForElementPresentByTextContains("Live Streaming");
            this.waitForElementPresentByTextContains("Stranger Danger");
            this.waitForElementPresentByTextContains("Photo Sharing");
            this.waitForElementPresentByTextContains("In App Purchases");


        }
    }

    public Boolean isElementLocatedOnTheScreenByTextIOS(String text)
    {
        String selectedElement = selectedElement(text);
        System.out.println(selectedElement);
        Boolean isElementLocatedOnTheScreenForWait =  this.isElementLocatedOnTheScreenForWait(selectedElement);
        System.out.println("isElementLocatedOnTheScreen: " + isElementLocatedOnTheScreenForWait);
        return isElementLocatedOnTheScreenForWait;
    }


    public WebElement initSelectedBlockElement()
    {
        return this.waitForElementPresent
                (blockXpath, "'Block' element not found!", 20);
    }
    public String getBlockAttributeValue()
    {
        WebElement selectedElement = initSelectedBlockElement();
        return selectedElement.getAttribute("selected");
    }
    public void waitForBlockElementPresent(String value)
    {
        String s = String.format(templateBlockButton, value);
        blockXpath = s;
        this.waitForElementPresent
                (blockXpath, "'Block' element not found!", 20);
    }



    public void switchToBlock(String value)
    {
        String s = String.format(templateBlockButton, value);
        blockXpath = s;
        System.out.println(blockXpath);
        this.waitForElementVisibilityAndClick(blockXpath, "'block' button with locator" + blockXpath + " not found!", 20);
    }






    public void switchToAllow(String value)
    {
        String s = String.format(templateAllowButton, value);
        allowXpath = s;
        this.waitForElementAndClick(allowXpath, "'allow' button not found!", 20);
    }


    public WebElement initSelectedAllowElement()
    {
        return this.waitForElementPresent
                (allowXpath, "'allow' element not found!", 20);
    }
    public void waitForAllowElementPresent(String value)
    {
        String s = String.format(templateAllowButton, value);
        allowXpath = s;
        this.waitForElementPresent
                (allowXpath, "'Block' element not found!", 20);
    }


    public boolean waitForAttributeOfTheBlockButtonStayTrue(String value)
    {
        String s = String.format(templateBlockButton, value);
        blockXpath = s;
        return this.waitForSelectedAttributeElementTrue(
                blockXpath, "attribute 'Selected' != true (blockButton)", 20);
    }

    public boolean waitForAttributeOfTheAllowButtonStayTrue(String value)
    {

        String s = String.format(templateAllowButton, value);
        allowXpath = s;
        return this.waitForSelectedAttributeElementTrue(
                allowXpath, "attribute 'Selected' != true (allowButton)", 20);
    }


    //swipes for ios
    public void swipeToIOSElementQuick()
    {
        if(Platform.getInstance().isAndroid()) {
            return;
        } else {
            this.swipeUpTillElementAppearAccuracy(
                    photoSharingIOS, "photoSharingIOS not found!", 100);
        }

    }

    public void swipeToIOSElementWithText (String elementText)
    {
        String selectedElement = selectedElement(elementText);
        this.swipeUpTillElementAppear(selectedElement, " element not found!", 60);
    }

    public void swipeToIOSElementWithTextAccuracy (String elementText)
    {
        String selectedElement = selectedElement(elementText);
        this.swipeUpTillElementAppearAccuracy(selectedElement, " element not found!", 60);
    }


    public boolean waitUntilBlockButtonEnabled(String element)
    {
//        String s = String.format(templateBlockButton, element);
//        blockXpath = s;
        String selectedElement = selectedElement(element);
        Boolean enabled = this.waitForElementAttributeEnabledTrue(
                selectedElement,  element + " element attribute not enabled", 20);
        System.out.println("element with locator " + selectedElement + " enabled? : " + enabled);
        return enabled;
    }

//    public void tapOnElement(String value){
//        String selectedElement = selectedElement(value);
//        this.waitForElementVisibilityAndClick(selectedElement, "'" + value + "'" + " element not found!", 20);
//    }



/*    public void keepZiftFree()
    {
        try{
            this.waitForElementVisibilityAndClick(
                    keepZiftFreeButton,
                    "'KEEP ZIFT FREE' button not found!", 6);

        }catch
                (TimeoutException e){
            System.out.println("some braze event not present!");

        }

    }*/


/*  Старая реализация ожидания braze - ивента (связано с class MainPageObject/waitForBrazeEvent(String locator))
        public void keepZiftFree()
    {
        boolean check = this.waitForBrazeEvent(keepZiftFreeButton);
        if(check == true) {
            this.waitForElementAndClick(keepZiftFreeButton, "'KEEP ZIFT FREE' button not found!", 20);
        } else if(check == false){
            System.out.println("Some braze event not present");
        }
    }*/



}