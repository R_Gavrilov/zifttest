package lib.ui;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.factories.CommonNavigationUiFactory;
import okio.Utf8;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;

abstract public class FeedPageObject extends MainPageObject {

    public FeedPageObject(AppiumDriver driver)
    {
        super(driver);
    }

     protected static String

             appAdvisorElement,
             tongueTopElement,
             childProfileElement,
             EllipsesMenuButton,
             generalSettingsElement,
             subscriptionElement,
             helpElement,
             aboutElement,
             parentPortalElement,
             changePasswordElement,
             logoutElement,
             ziftFamilyFeedLabel,
             goPremiumElement,
             feedList,
             fuqComXpath,
             chaikovskySearchItem,
             chaikovskySearchItemIcon;


    public void waitForChaikovskySearchItemIcon()
    {
        this.waitForElementVisibility(chaikovskySearchItemIcon,  chaikovskySearchItemIcon + " element not found!", 60);
    }

    public void swipeToChaikovskySearchItem()
    {
        this.swipeUpTillElementAppear(chaikovskySearchItem, " element not found!", 60);
    }

    public void swipeToFuqCom()
    {
        this.swipeUpTillElementAppear(fuqComXpath, " element not found!", 60);

    }

    public void waitForFeedList()
    {
        this.waitForElementVisibility(feedList, "feedList not present!", 20);
    }

    public void goPremiumClick()
    {
        this.waitForElementAndClick(goPremiumElement, "'goPremiumElement' not present", 20);
    }

    public void waitForEllipsesMenuElement()
    {
        this.waitForElementPresent(EllipsesMenuButton, "EllipsesMenuButton not present", 20);
    }

    public void initLogoutClick()
    {
        this.waitForElementAndClick(logoutElement, "logoutElement not found!", 20);
    }

    public void initChangePasswordClick()
    {
        this.waitForElementAndClick(changePasswordElement, "changePasswordElement not found!", 20);
    }

    public void initChildProfileClick() throws Exception
    {
        String pageSource = driver.getPageSource();
        FileWriter fw = new FileWriter( "/Users/rodion/Documents/projects/NetNanny/test.txt" );
        File file = new File("/Users/rodion/Documents/projects/NetNanny/test.txt");
        FileUtils.writeStringToFile(file, pageSource);
        this.waitForElementVisibilityAndClick(childProfileElement, "Cannot find and click CHILD_PROFILE_ELEMENT", 20);
    }

    public void initTongueTopClick()
    {
        this.waitForElementAndClick(tongueTopElement,"'tongueTop' not found!",20);
    }

    public void initEllipsesMenuElementClick()
    {
        this.waitForElementAndClick(EllipsesMenuButton, "EllipsesMenuButton not found!", 20);
    }

    public void waitForGeneralSettingsElementPresent()
    {
        this.waitForElementPresent(generalSettingsElement, "'General settings' button not found!", 20);
    }

    public void initGeneralSettingsClick()
    {
        this.waitForElementVisibilityAndClick(generalSettingsElement, "'General settings' button not found!", 20);
    }

    public void waitForSubscriptionElementPresent()
    {
        waitForElementPresent(subscriptionElement, "'Subscription' button not found!", 20);
    }
    public void initSubscriptionClick()
    {
        waitForElementAndClick(subscriptionElement, "'Subscription' button not found!", 20);
    }

    public void appAdvisorClick()
    {
        this.waitForElementAndClick(appAdvisorElement, "'appAdvisor' button not found!", 20);
    }

    public void initHelpElementClick()
    {
        this.waitForElementAndClick((helpElement), "'Help' button not found!", 20);
    }
    public void initAboutClick()
    {
        this.waitForElementAndClick((aboutElement), "'About' button not found!", 20);
    }
    public void parentPortalClick()
    {
        this.waitForElementAndClick(parentPortalElement, "'Parent Portal' button not found!", 20);
    }


}
