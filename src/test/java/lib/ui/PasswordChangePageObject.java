package lib.ui;


import io.appium.java_client.AppiumDriver;
import lib.Platform;
import org.openqa.selenium.WebElement;

abstract public class PasswordChangePageObject extends MainPageObject {
    public PasswordChangePageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

        currentPasswordField,
        newPasswordField,
        repeatPasswordField,
        cancelButton,
        doneButton,
        errorElement,
        currentPasswordClearElement,
        newPasswordClearElement,
        repeatPasswordClearElement,
        confirmIOSElementAlert;

    public void  confirmIOSAlert()
    {
        this.waitForElementAndClick(
                confirmIOSElementAlert, "confirmIOSElementAlert not found", 20);
    }


    public void enterCurrentPassword(String currentPassword)
    {
        this.waitForElementAndSendKeys(currentPasswordField, currentPassword, "cannot enter current password", 20);
    }

    public void enterNewPassword(String newPassword)
    {
        this.waitForElementAndSendKeys(newPasswordField, newPassword, "cannot enter current password",20);
    }

    public void repeatNewPassword(String repeatPassword)
    {
        this.waitForElementAndSendKeys(repeatPasswordField, repeatPassword, "cannot enter current password",20);
    }

    public void doneButtonClick()
    {
        this.waitForElementAndClick(doneButton, "cannot press a doneButton", 20);
    }
    public void cancelButtonClick()
    {
        this.waitForElementAndClick(cancelButton, "cannot press a doneButton", 20);
    }

    public void waitForErrorMessage()
    {
        this.waitForElementPresent(errorElement, "errorElement not present!", 20);
    }

    public WebElement errorMessage()
    {
        return this.waitForElementPresent(errorElement, "errorElement not present!", 20);

    }
    public String getTextInErrorMessage()
    {
        if (Platform.getInstance().isAndroid())
        {
            return errorMessage().getAttribute("text");
        }
        else {
            return errorMessage().getAttribute("name");
        }
        }

    public void clearCurrentInputField()
    {
        this.waitForElementAndClick(currentPasswordClearElement, "currentPasswordClearElement not found!", 20);
    }
    public void clearNewPasswordInputField()
    {
        this.waitForElementAndClick(newPasswordClearElement, "newPasswordClearElement not found!", 20);
    }
    public void clearRepeatPasswordInputField()
    {
        this.waitForElementAndClick(repeatPasswordClearElement, "repeatPasswordClearElement not found!", 20);
    }



















}
