package lib.ui;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

abstract public class SubscriptionPageObject extends MainPageObject {

    public SubscriptionPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

        subscribeButton,
        existingAppleId,
        subscribeButtonPopUp,
        inputField,
        unsubscribeButton,
        cancelSubscriptionButton,
        declineToAnswer,
        continueButton,
        cancelSubscriptionButton2,
        restoreButton,
        ziftPremiumElement,
        termsOfUseElement,
        privacyPolicyElement,
        updateElement,
        seeCurrentSubscriptions,
        buyButton;


    public void makePremiumSubscription()
    {
        try {
            this.initSubscribeButtonPopUpClick();
            this.enterPasswordForSamsungtesteight();
            this.initVerifyClick();
        } catch (TimeoutException e) {
            System.out.println();
            try {
                this.initSubscribeButtonPopUpClick();
            } catch (TimeoutException e1) {
                System.out.println();
                try {
                    this.initSubscribeButtonPopUpClick();
                } catch (TimeoutException e2) {
                    System.out.println();
                }
            }
        }
    }
    public void initSubscribeButtonClick()
    {
        this.waitForElementAndClick
                (subscribeButton,
                        "'Subscribe' button not found!",
                        20);
    }

    public void initSubscribeButtonPopUpClick()
    {
        this.waitForElementAndClick
                (subscribeButtonPopUp,
                        "'Subscribe' button on popUp not found!",
                        20);
    }

    public void enterPasswordForSamsungtesteight()
    {
        this.waitForElementAndSendKeys(
                inputField, "6tfc7ygv8uhB",
                        "input field not found!",
                        20);
    }

    public void initVerifyClick()
    {
        this.waitForElementAndClick(
                "xpath://android.widget.Button[@content-desc='Verify']",
                        "'Verify' button not found!",
                        20);
    }

    public void buyButtonClick()
    {
        this.waitForElementAndClick(buyButton, "'Verify' button not found!",
                20);
    }

    public void initUnsubscribeClick()
    {
        this.waitForElementAndClick(unsubscribeButton,
                        "'Unsubscribe' button not found!",
                        20);
    }

    public void waitForUnsubscribeButton()
    {
        this.waitForElementPresent(unsubscribeButton,
                "'Unsubscribe' button not found!",
                20);
    }

    public void swipeToCancelSubscriptionButton()
    {
        this.swipeUpToFindElement(cancelSubscriptionButton,
                        "CANCEL SUBSCRIPTION button not found! swipe",
                        20);
    }

    public void initCancelSubscriptionClick()
    {
        this.waitForElementPresent(updateElement, "updateElement not found!", 20);
        this.waitForElementAndClick(cancelSubscriptionButton,
                        "'CANCEL SUBSCRIPTION' button not found!",
                        20);
    }

    public void initDeclinetoAnswerClick()
    {
        this.waitForElementAndClick(declineToAnswer,
                        "'Decline to answer' radioButton not found!",
                        20);
    }

    public void initContinueButtonClick()
    {
        this.waitForElementAndClick(continueButton,
                        "'CONTINUE' button not found!",
                        20);
    }

    public void initCancelSubscriptionButtonClick()
    {
        this.waitForElementAndClick(cancelSubscriptionButton2,
                        "'CANCEL SUBSCRIPTION' button not found!",
                        20);
    }

    public void waitForRestoreButtonPresent()
    {
        try
        {
            this.waitForElementPresent(restoreButton,
                    "'RESTORE' button not found!",
                    20);
        } catch (TimeoutException e)
        {
            this.waitForElementPresent(seeCurrentSubscriptions,
                    "'RESTORE' button not found!",
                    20);
        }
    }

    public void waitForPageLoad()
    {
        this.waitForElementPresent(ziftPremiumElement, "Element with text 'Zift Premium' not found!");
    }

    public void initTermsOfUseClick()
    {
        this.waitForElementAndClick(termsOfUseElement, "'termsOfUse' button not found!", 20);
    }

    public void initPrivacyPolicyClick()
    {
        this.waitForElementAndClick(privacyPolicyElement, "'privacyPolicy' button not found!", 20);
    }









}
