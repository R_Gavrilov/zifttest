package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

abstract public class AppSettingsPageObject extends MainPageObject {
    public AppSettingsPageObject(AppiumDriver driver)
    {
        super(driver);
    }


    protected static  String

            switchTabInstalledElement,
            tabAlphabetically,
            tabInstallDate,
            tabAll,
            tabInstalled,
            tabAndroid,
            tabIos;






    public void initSwitchTabInstalled()
    {
        this.waitForElementAndClick(switchTabInstalledElement, "'switch' button not found!", 20);
    }


    public void waitForFilterElementsPresent()
    {
/*      this.waitForElementPresentByText((tabAlphabetically),"'tabAlphabetically' button not found!");
        this.waitForElementPresentByText((tabInstallDate),"'tabInstallDate' button not found!");
        this.waitForElementPresentByText((tabAll),"'tabAll' button not found!");
        this.waitForElementPresentByText((tabInstalled),"'tabInstalled' button not found!");*/
        this.waitForElementPresent(tabAndroid,"'tabAll' button not found!");
        this.waitForElementPresent(tabIos,"'tabInstalled' button not found!");
    }

    public void switchToIOSFilter()
    {
        this.waitForElementAndClick(tabIos, "iosFilterElement not found", 20);
    }










/*
    selectedAllowBlock = "//*[@text='Smart Manager']/../../android.widget.TextView";

    public WebElement waitAllowBlockElement()
    {
        return this.waitForElementPresentByText
                (By.xpath(selectedAllowBlock), "'AllowBlock' elements not found!", 20);
    }
    public String getSelectedElement()
    {
        WebElement selectedElement = waitAllowBlockElement();
        return selectedElement.getAttribute("selected");
    }*/


}
