package lib.ui;

import io.appium.java_client.AppiumDriver;

abstract public class RestrictionsPageObject extends MainPageObject{

    public RestrictionsPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

            appSettingsElement,
            contentFilterElement,
            urlSettingsElement;


    public void initAppSettingsClick()
    {
        this.waitForElementAndClick(appSettingsElement, "'appSettings' element not found!", 20);
    }

    public void initContentFilterClick()
    {
        this.waitForElementAndClick
                (contentFilterElement, "ContentFilter button not found!", 20);
    }

    public void initWebsiteSettingsClick()
    {
        this.waitForElementAndClick(urlSettingsElement, "'urlSettings' button not found!");
    }



}
