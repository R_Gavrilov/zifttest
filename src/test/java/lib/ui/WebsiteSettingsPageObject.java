package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import static io.appium.java_client.touch.offset.PointOption.point;

abstract public class WebsiteSettingsPageObject extends MainPageObject {

    public WebsiteSettingsPageObject(AppiumDriver driver) {
        super(driver);
    }

    protected static String

            testComUrl,
            deleteIconTemplate,
            selectedUrl,
            addBlock,
            addAllow,
            okButton,
            pornhubUrl,
            inputField,
            pornhubIsAlreadyAddedToElement,
            moveToAlwaysAllowListButton,
            descriptionElement,
            addBlockElement,
            amountOfBlockElements,
            amountOfAllowElements,
            alwaysBlockList,
            alwaysAllowList,
            deleteButton,
            urlTemplate,
            UrlFromAlwaysAllow;



    private static String selectUrl(String value)
    {
        return urlTemplate.replace("{value}", value);
    }

    public void waitForUrlPresent(String value)
    {
        String selectedUrl = selectUrl(value);
        waitForElementPresent(selectedUrl,  "URL: " + value + " not found!");
    }


    private static String selectedDeleteIcon(String value)
    {
        return deleteIconTemplate.replace("{value}", value);
    }
    public void initDeleteIconClick(String value)
    {
        String selectedDeleteIconXpath = selectedDeleteIcon(value);
        this.waitForElementAndClick(selectedDeleteIconXpath, "'delete' button not found!");
    }

    public void waitForTestComUrlPresent() {
        this.waitForElementPresent(testComUrl, "url 'tests.com' not found!");
    }



    public void waitForUrlToBeMovedToAlwaysAllow()
    {
        this.waitForElementVisibility(UrlFromAlwaysAllow, "", 20);
    }

    public int getAmountOfBlockElements()
    {
        return this.getAmountOfElements(amountOfBlockElements);
    }

    public int getAmountOfAllowElements()
    {
        return this.getAmountOfElements(amountOfAllowElements);
    }


    private static String selectedURL(String value)
    {
        return selectedUrl.replace("{value}", value);
    }
    public void waitForSelectedUrlNotPresent(String value)
    {
        String selectedUrlXpath = selectedDeleteIcon(value);
        this.waitForElementNotPresent(selectedUrlXpath, "selected url is visible", 20);
    }

    public void initAddBlockButtonClick()
    {
        this.waitForElementAndClick(addBlock,
                        "'addBlock' button not found!");
    }

    public void enterUrl(String value)
    {
        this.waitForElementAndSendKeys(inputField, value,
                "cannot enter value 'pornhub.com'", 20);
    }

    public void initOkButtonClick()
    {
        this.waitForElementAndClick(okButton, "'OK' button not found!");
    }

    public void waitForPornhubPresent()
    {
        this.waitForElementPresent(pornhubUrl, "text 'pornhub.com' not found!");
    }

    public void initAddAllowButtonClick()
    {
        this.waitForElementAndClick
                (addAllow, "'addAllow' button not found!");
    }

    public void waitForPornhubIsAlreadyAddedToElement()
    {
        this.waitForElementPresent(pornhubIsAlreadyAddedToElement,
                        "text 'pornhub.com is already added to “Always Block” list. What would you do?' not found!");
    }

    public void initMoveToAlwaysAllowListClick()
    {
        this.waitForElementAndClick(moveToAlwaysAllowListButton,
                        "'Move to “Always Allow” list' button not found!");
    }

    public void deletePornhubFromAlwaysAllow()
    {
        this.waitForElementAndClick(deleteButton, "'Delete' button not found!");
    }

    public void waitForAlwaysAlowListNotPresent()
    {
        this.waitForElementNotPresent(alwaysAllowList,
                        "'alwaysAllow' list is present!", 20);
    }

    public void waitForAlwaysBlockListNotPresent()
    {
        this.waitForElementNotPresent(alwaysBlockList,
                        "'alwaysBlock' list is present!", 20);
    }


    public void waitForDescriptionElement()
    {
        this.waitForElementPresent(descriptionElement,
                "description with text 'You can assign which websites are “Always Blocked”...' not found!'");
    }

    public void waitForAddBlockElement()
    {
        this.waitForElementPresent(addBlockElement,"'addBlock' element not found!");
    }

    public void contentFilterSettingsLinkClick()
    {
        WebElement element = driver.findElement(By.id("com.contentwatch.ghoti.cp2.parent:id/description"));
        Dimension size = element.getSize();
        int x = element.getLocation().getX() + (size.getWidth() / 3) * 2;
        int y = (int) (element.getLocation().getY() + (size.getHeight() / 3) * 2.5);
        TouchAction action = new TouchAction(driver);
        action.tap(point(x, y)).perform();

    }


    public void clickLink()
    {
        MobileElement element = (MobileElement) driver.findElement(MobileBy.id("com.contentwatch.ghoti.cp2.parent:id/description"));
        Dimension size = element.getSize();
        int x = element.getLocation().getX() + (size.getWidth() / 3) * 2;
        int y = (int) (element.getLocation().getY() + (size.getHeight() / 3) * 2.5);
        TouchAction action = new TouchAction(driver);
        action.tap(point(x, y)).perform();

    }



}
