package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

abstract public class ScreenTimeManagementPageObject extends MainPageObject {
    public ScreenTimeManagementPageObject(AppiumDriver driver) {
        super(driver);
    }

    protected static String

            unlimCheckBoxElement,
            screenTimeTodayLeftElement,
            screenTimeTodayUsedElement,
            plusMinusButtonTemplate,
            screenTimeOverride,
            saturdayElement,
            mondayElement,
            tuesdayElement,
            wednesdayElement,
            thursdayElement,
            fridayElement,
            sundayElement,
            minusButton,
            plusButton,
            exhaustedModeElement,
            noInternetExhaustedMode,
            pauseDeviceExhaustedMode;

    private static String plusMinusButton = "";
/*    public static void getXpathPlusMinusElement(String value)
    {
        String s = String.format(plusMinusButtonTemplate, value);
        plusMinusButton = s;
    }*/

    public void selectNoInternetExhaustedMode() throws Exception {
        WebElement actualExhaustedMode = this.waitForElementVisibility(
                exhaustedModeElement, "exhaustedModeElement not visible", 5);
        if (actualExhaustedMode.getAttribute("text").equals("No Internet")) {
            return;
        } else if (actualExhaustedMode.getAttribute("text").equals("Pause Device")) {
            this.waitForElementAndClick(exhaustedModeElement, "exhaustedModeElement not present", 5);
            this.waitForElementAndClick(noInternetExhaustedMode, "noInternetExhaustedMode not present", 5);
        }
    }

    public void selectPauseDeviceExhaustedMode() throws Exception {
        WebElement actualExhaustedMode = this.waitForElementVisibility(
                exhaustedModeElement, "exhaustedModeElement not visible", 5);
        if (actualExhaustedMode.getAttribute("text").equals("Pause Device")) {
            return;
        } else if (actualExhaustedMode.getAttribute("text").equals("No Internet")) {
            this.waitForElementAndClick(exhaustedModeElement, "exhaustedModeElement not present", 5);
            this.waitForElementAndClick(pauseDeviceExhaustedMode, "pauseDeviceExhaustedMode not present", 5);
        }
    }

    public boolean verifyThatPlusMinusDisabled(String value)
    {
        String s = String.format(plusMinusButtonTemplate, value);
        plusMinusButton = s;
        return this.waitForElementAttributeEnabledFalse(plusMinusButton, "attribute 'Enabled' != false", 20);
    }
    public boolean verifyThatPlusMinusEnabled(String value)
    {   String s = String.format(plusMinusButtonTemplate, value);
        plusMinusButton = s;
        return this.waitForElementAttributeEnabledTrue(plusMinusButton, "attribute 'Enabled' != true", 20);
    }

    public void checkThatPlusMinusButtonsEnabled() {
        this.verifyThatPlusMinusEnabled("plus");
        this.verifyThatPlusMinusEnabled("minus");
    }

    public void checkThatPlusMinusButtonsDisabled() {
        this.verifyThatPlusMinusDisabled("plus");
        this.verifyThatPlusMinusDisabled("minus");
    }

    public WebElement initPlusMinusElement() {
        return this.waitForElementPresent(
                plusMinusButton, "'plusMinusButton' element not present!", 20);
    }

    public void initPlusMinusButtonClick() {
        this.waitForElementAndClick(plusMinusButton, "'Plus button' not found!", 20);
    }


    public void turnSwitcherON() {
        try {
            this.waitForCheckboxUnlimitedOn();
        } catch (TimeoutException e) {
            this.unlimCheckBoxClick();
            this.waitForCheckboxUnlimitedOn();
        }
    }

    public void turnSwitcherOFF() {
        try {
            this.waitForCheckboxUnlimitedOff();
        } catch (TimeoutException e) {
            this.unlimCheckBoxClick();
            this.waitForCheckboxUnlimitedOff();
        }
    }

    public boolean waitForCheckboxUnlimitedOn() {
        return this.waitForUnlimitedCheckboxAttributeTrue(unlimCheckBoxElement, "checkbox attribute 'checked' != true", 5);
    }

    public boolean waitForCheckboxUnlimitedOff()
    {
        return this.waitForUnlimitedCheckboxAttributeFalse(unlimCheckBoxElement, "checkbox attribute 'checked' != false", 5);
    }

    public void unlimCheckBoxClick()
    {
        this.waitForElementAndClick(unlimCheckBoxElement, "'unlimCheckBox' not found!", 20);
    }

    public WebElement initScreenTimeTodayLeftElement()
    {
        return  this.waitForElementPresent(screenTimeTodayLeftElement, "screenTimeTodayLeftElement not present!", 20);
    }

    public String getScreenTimeTodayLeftElementText()
    {
        WebElement screenTimeTodayLeftElementText = initScreenTimeTodayLeftElement();
        return screenTimeTodayLeftElementText.getAttribute("text");
    }

//    public String getHoursOfScreenTimeUsed()
//    {
//        WebElement screenTimeTodayUsedElement = this.waitForElementPresent(screenTimeTodayUsedElement, "screenTimeTodayUsedElement not present!", 20);
//        return screenTimeTodayUsedElement.getAttribute("text");
//        String screenTimeUsedElementText = screenTimeTodayUsedElement.getAttribute("text").replaceAll("\\D+","");
//        String min = screenTimeUsedElementText.substring(0, screenTimeUsedElementText.length() - 1);
//        min.split();
//        System.out.println(screenTimeUsedElementText);
//        return screenTimeUsedElementText;
//    }

    public String getHoursOfScreenTimeUsed()  {
        try {
            WebElement screenTimeUsedElement =
                    this.waitForElementPresent(screenTimeTodayUsedElement, "screenTimeTodayUsedElement not present!", 20);
            String screenTimeUsedElementText = screenTimeUsedElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]h|[0-9]h");
            Matcher matcher = pattern.matcher(screenTimeUsedElementText);
            matcher.find();
            String hoursOfScreenTimeUsed = matcher.group().replaceAll("\\D+", "");
            return hoursOfScreenTimeUsed;
        } catch (IllegalStateException e) {
            return null;
        }
    }
    public Integer convertHoursOfScreenTimeUsedToSeconds()  {

        try {
            Integer hours = Integer.parseInt(this.getHoursOfScreenTimeUsed());
            Integer hoursOfScreenTimeUsedToSeconds = hours * 3600;
            System.out.println("hoursOfScreenTimeUsedToSeconds: " + hoursOfScreenTimeUsedToSeconds);
            return hoursOfScreenTimeUsedToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("hoursOfScreenTimeUsed not found!");
            return 0;
        }
    }
    public String getMinutesOfScreenTimeUsed()  {
        try {
            WebElement screenTimeUsedElement = this.waitForElementPresent(screenTimeTodayUsedElement, "screenTimeTodayUsedElement not present!", 20);
            String screenTimeUsedElementText = screenTimeUsedElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]m|[0-9]m");
            Matcher matcher = pattern.matcher(screenTimeUsedElementText);
            matcher.find();
            String minutesOfScreenTimeUsed = matcher.group().replaceAll("\\D+", "");
            return minutesOfScreenTimeUsed;
        } catch (IllegalStateException e) {
            return null;
        }
    }
    public Integer convertMinutesOfScreenTimeUsedToSeconds()  {
        try {
            Integer minutes = Integer.parseInt(this.getMinutesOfScreenTimeUsed());
            Integer minutesOfScreenTimeUsedToSeconds = minutes * 60;
            System.out.println("minutesOfScreenTimeUsed in seconds: " + minutesOfScreenTimeUsedToSeconds);
            return minutesOfScreenTimeUsedToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("minutesOfScreenTimeUsed not found!");
            return 0;
        }
    }

    public Integer actualSecondsOfScreenTimeUsed() throws NullPointerException {
        Integer secondsOfScreenTimeUsed =
                this.convertHoursOfScreenTimeUsedToSeconds() + this.convertMinutesOfScreenTimeUsedToSeconds();
        System.out.println("screenTimeUsed in seconds: " + secondsOfScreenTimeUsed);
        return secondsOfScreenTimeUsed;
    }

    public String getHoursOfScreenTimeOverride() throws IllegalStateException, TimeoutException {
        try {
            WebElement screenTimeOverrideElement =
                    this.waitForElementPresent(screenTimeOverride, "screenTimeOverrideElement not present!", 20);
            String screenTimeOverrideElementText = screenTimeOverrideElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]h|[0-9]h");
            Matcher matcher = pattern.matcher(screenTimeOverrideElementText);
            matcher.find();
            String hoursOfScreenTimeOverride = matcher.group().replaceAll("\\D+", "");
            return hoursOfScreenTimeOverride;
        } catch (IllegalStateException e) {
            return null;
        } catch (TimeoutException e1) {
            return null;
        }
    }
    public Integer convertHoursOfScreenTimeOverrideToSeconds()  {
        try {
            Integer hours = Integer.parseInt(this.getHoursOfScreenTimeOverride());
            Integer hoursOfScreenTimeUsedToSeconds = hours * 3600;
            System.out.println("hoursOfScreenTimeOverride in seconds: " + hoursOfScreenTimeUsedToSeconds);
            return hoursOfScreenTimeUsedToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("hoursOfScreenTimeOverride not found!");
            return 0;
        } catch (NullPointerException e2) {
            return null;
        }
    }

    public String getMinutesOfScreenTimeOverride()  {
        try {
            WebElement screenTimeOverrideElement =
                    this.waitForElementPresent(screenTimeOverride, "screenTimeOverrideElement not present!", 20);
            String screenTimeOverrideElementText = screenTimeOverrideElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]m|[0-9]m");
            Matcher matcher = pattern.matcher(screenTimeOverrideElementText);
            matcher.find();
            String minutesOfScreenTimeOverride = matcher.group().replaceAll("\\D+", "");
            return minutesOfScreenTimeOverride;
        } catch (IllegalStateException e) {
            return null;
        } catch (TimeoutException e2) {
            return null;
        }
    }
    public Integer convertMinutesOfScreenTimeOverrideToSeconds()  {
        try {
            Integer minutes = Integer.parseInt(this.getMinutesOfScreenTimeOverride());
            Integer minutesOfScreenTimeOverrideToSeconds = minutes * 60;
            System.out.println("minutesOfScreenTimeOverride in seconds: " + minutesOfScreenTimeOverrideToSeconds);
            return minutesOfScreenTimeOverrideToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("minutesOfScreenTimeOverride not found!");
            return 0;
        }
    }
    public Integer actualSecondsOfScreenTimeOverride() throws NullPointerException {
        Integer secondsOfScreenTimeOverride =
                this.convertHoursOfScreenTimeOverrideToSeconds() + this.convertMinutesOfScreenTimeOverrideToSeconds();
        System.out.println("screenTimeOverride in seconds : " + secondsOfScreenTimeOverride);
        return secondsOfScreenTimeOverride;
    }


    

    public String getHoursOfScreenTimeLeft() throws IllegalStateException, TimeoutException {
        try {
            WebElement screenTimeLeftElement =
                    this.waitForElementPresent(screenTimeTodayLeftElement, "screenTimeTodayLeftElement not present!", 20);
            String screenTimeLeftElementText = screenTimeLeftElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]h|[0-9]h");
            Matcher matcher = pattern.matcher(screenTimeLeftElementText);
            matcher.find();
            String hoursOfScreenTimeLeft = matcher.group().replaceAll("\\D+", "");
            return hoursOfScreenTimeLeft;
        } catch (IllegalStateException e) {
            return null;
        } catch (TimeoutException e1) {
            return null;
        }
    }
    public Integer convertHoursOfScreenTimeLeftToSeconds()  {
        try {
            Integer hours = Integer.parseInt(this.getHoursOfScreenTimeLeft());
            Integer hoursOfScreenTimeLeftToSeconds = hours * 3600;
            System.out.println("hoursOfScreenTimeLeft in seconds: " + hoursOfScreenTimeLeftToSeconds);
            return hoursOfScreenTimeLeftToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("hoursOfScreenTimeLeft not found!");
            return 0;
        } catch (NullPointerException e2) {
            return 0;
        }
    }

    public String getMinutesOfScreenTimeLeft()  {
        try {
            WebElement screenTimeLeftElement =
                    this.waitForElementPresent(screenTimeTodayLeftElement, "screenTimeTodayLeftElement not present!", 20);
            String screenTimeLeftElementText = screenTimeLeftElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]m|[0-9]m");
            Matcher matcher = pattern.matcher(screenTimeLeftElementText);
            matcher.find();
            String minutesOfScreenTimeLeft = matcher.group().replaceAll("\\D+", "");
            return minutesOfScreenTimeLeft;
        } catch (IllegalStateException e) {
            return null;
        } catch (TimeoutException e2) {
            return null;
        }
    }
    public Integer convertMinutesOfScreenTimeLeftToSeconds()  {
        try {
            Integer minutes = Integer.parseInt(this.getMinutesOfScreenTimeLeft());
            Integer minutesOfScreenTimeLeftToSeconds = minutes * 60;
            System.out.println("minutesOfScreenTimeLeft in seconds: " + minutesOfScreenTimeLeftToSeconds);
            return minutesOfScreenTimeLeftToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("minutesOfScreenTimeLeft not found!");
            return 0;
        } catch (NullPointerException e2) {
            return 0;
        }
    }
    public Integer actualSecondsOfScreenTimeLeft() throws NullPointerException {
        Integer secondsOfScreenTimeLeft =
                this.convertHoursOfScreenTimeLeftToSeconds() + this.convertMinutesOfScreenTimeLeftToSeconds();
        System.out.println("screenTimeLeft in seconds : " + secondsOfScreenTimeLeft);
        return secondsOfScreenTimeLeft;
    }

    public Integer expectedScreenTimeTodayLeftInSeconds() {
        Integer screenTimeTodayLeftInSeconds = this.actualSecondsOfScreenTimeOverride() - this.actualSecondsOfScreenTimeUsed();
        return screenTimeTodayLeftInSeconds;
    }


    public WebElement initElementBetweenPlusMinus()
    {
        return  this.waitForElementPresent(screenTimeOverride, "screenTimeOverride not found!", 15);
    }
    public String getOverrideForTodayText()
    {
        try {
            WebElement textBetweenPlusMinus = initElementBetweenPlusMinus();
            return textBetweenPlusMinus.getAttribute("text");
        } catch (TimeoutException e) {
            String a = "";
            return  a ;
        }
    }

    public void openScreenTimeLimits() {
        this.waitForElementAndClick(saturdayElement, "'mondayElement' not found!", 20);
    }

    public WebElement initDailyAllocationSaturdayElement() {
        return  this.waitForElementPresent(saturdayElement, "saturdayElement in 'Daily Allocation' not found!", 20);
    }

    public String getDailyAllocationSaturdayText() {
        WebElement dailyAllocationSaturdayText = initDailyAllocationSaturdayElement();
        return dailyAllocationSaturdayText.getAttribute("text");
    }

    public WebElement initDailyAllocationSundayElement() {
        return  this.waitForElementPresent(sundayElement, "sundayElement in 'Daily Allocation' not found!", 20);
    }

    public String getDailyAllocationSundayText() {
        WebElement dailyAllocationSundayText = initDailyAllocationSundayElement();
        return dailyAllocationSundayText.getAttribute("text");
    }


    public WebElement initDailyAllocationMondayElement() {
        return  this.waitForElementPresent(mondayElement, "mondayElement in 'Daily Allocation' not found!", 20);
    }



    public String getDailyAllocationMondayText() {
        WebElement dailyAllocationMondayText = initDailyAllocationMondayElement();
        return dailyAllocationMondayText.getAttribute("text");
    }

    public WebElement initDailyAllocationTuesdayElement() {
        return  this.waitForElementPresent(tuesdayElement, "tuesdayElement in 'Daily Allocation' not found!", 20);
    }
    public String getDailyAllocationTuesdayText() {
        WebElement dailyAllocationTuesdayText = initDailyAllocationTuesdayElement();
        return dailyAllocationTuesdayText.getAttribute("text");
    }

    public WebElement initDailyAllocationWednesdayElement() {
        return  this.waitForElementPresent(wednesdayElement, "wednesdayElement in 'Daily Allocation' not found!", 20);
    }

    public String getDailyAllocationWednesdayText() {
        WebElement dailyAllocationWednesdayText = initDailyAllocationWednesdayElement();
        return dailyAllocationWednesdayText.getAttribute("text");
    }


    public WebElement initDailyAllocationThursdayElement() {
        return  this.waitForElementPresent(thursdayElement, "thursdayElement in 'Daily Allocation' not found!", 20);
    }

    public String getDailyAllocationThursdayText() {
        WebElement dailyAllocationThursdayText = initDailyAllocationThursdayElement();
        return dailyAllocationThursdayText.getAttribute("text");
    }


    public WebElement initDailyAllocationFridayElement() {
        return  this.waitForElementPresent(fridayElement, "fridayElement in 'Daily Allocation' not found!", 20);
    }

    public String getDailyAllocationFridayText() {
        WebElement dailyAllocationFridayText = initDailyAllocationFridayElement();
        return dailyAllocationFridayText.getAttribute("text");
    }


    public void longPressAndWaitUntilMinusButtonDisabled() throws Exception {
        WebElement element = waitForElementVisibility(minusButton, "minusButton not visible", 10);
        boolean isEnabled = element.isEnabled();
        while (isEnabled) {
            TouchAction action = new TouchAction(driver);
            action.longPress(element(element)).
                    waitAction(waitOptions(Duration.ofSeconds(2))).
                    release().
                    perform();
            System.out.println("end of cycle");
            if (!element.isEnabled())
                isEnabled = false;
            Thread.sleep(5000);
        }
    }

    public void longPressAndWaitUntilPlusButtonDisabled() throws Exception {
        WebElement element = waitForElementVisibility(plusButton, "plusButton not visible", 10);
        boolean isEnabled = element.isEnabled();
        while (isEnabled) {
            TouchAction action = new TouchAction(driver);
            action.longPress(element(element)).
                    waitAction(waitOptions(Duration.ofSeconds(2))).
                    release().
                    perform();
            System.out.println("end of cycle");
            if (!element.isEnabled())
                isEnabled = false;
            Thread.sleep(5000);
        }
    }

    public void goToTheCurrentDayInWeeklyLimits() {
        String currentDay = LocalDate
                .now()
                .getDayOfWeek()
                .name();
        System.out.println(currentDay);
        if (currentDay.equals("MONDAY")) {
            this.waitForElementAndClick(mondayElement, "tuesdayElement not present", 15);
        } else if (currentDay.equals("TUESDAY")) {
            this.waitForElementAndClick(tuesdayElement, "tuesdayElement not present", 15);
        } else if (currentDay.equals("WEDNESDAY")) {
            this.waitForElementAndClick(wednesdayElement, "tuesdayElement not present", 15);
        } else if (currentDay.equals("THURSDAY")) {
            this.waitForElementAndClick(thursdayElement, "tuesdayElement not present", 15);
        } else if (currentDay.equals("FRIDAY")) {
            this.waitForElementAndClick(fridayElement, "tuesdayElement not present", 15);
        } else if (currentDay.equals("SATURDAY")) {
            this.waitForElementAndClick(saturdayElement, "tuesdayElement not present", 15);
        } else if (currentDay.equals("SUNDAY")) {
            this.waitForElementAndClick(sundayElement, "tuesdayElement not present", 15);
        }
    }




}
