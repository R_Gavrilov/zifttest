package lib.ui;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

abstract public class BlocksAndAlertsObject extends MainPageObject{

    public BlocksAndAlertsObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String
            iconW1,
            iconW2,
            exclamationIconElement1,
            exclamationIconElement2,
            gearIconElement1,
            gearIconElement2,
            gearIcon,
            alwaysBlock,
            alwaysAllow,
            applyButton,
            elementsFromBlocksAlertsList,
            iOSAlertIconW;
//*[@text='Viewed: Porn']/following-sibling::*[contains(@text,'www.playboy.com')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/mark']



    public String getElementTitleTextFromBlocksAlertsList(int index)
    {
        List<WebElement> applicationList = driver.findElements(By.xpath
                ("//*[@resource-id ='com.contentwatch.ghoti.cp2.parent:id/childContent']//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/title']"));
        return applicationList.get(index).getAttribute("text");
    }
    public String getElementLinkTextFromBlocksAlertsList(int index)
    {
    List<WebElement> applicationList = driver.findElements(By.xpath
            ("//*[@resource-id ='com.contentwatch.ghoti.cp2.parent:id/childContent']//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/link']"));
    return applicationList.get(index).getAttribute("text");
    }
    public String getElementSubtitleTextFromBlocksAlertsList(int index)
    {
        List<WebElement> applicationList = driver.findElements(By.xpath
                ("//*[@resource-id ='com.contentwatch.ghoti.cp2.parent:id/childContent']//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/subtitle']"));
        return applicationList.get(index).getAttribute("text");
    }




//Template methods
    private static String elementIconW(String title) {
        return iconW1.replace("{value}", title);
    }
    private static String elementIconW2(String link) {
        return iconW2.replace("{value2}", link);
    }
    public void waitForIconW(String title, String link)
    {
        String elementFromBlockAlertsTabIconW = elementIconW(title);
        String elementFromBlockAlertsTabIconW2 = elementIconW2(link);
        this.waitForElementPresent((elementFromBlockAlertsTabIconW + elementFromBlockAlertsTabIconW2),"icon (w) not found!");
    }

    private static String elementExclamationIcon(String title) {
        return exclamationIconElement1.replace("{value}", title);
    }
    private static String elementExclamationIcon2(String link) {
        return exclamationIconElement2.replace("{value2}", link);
    }
    public void waitForAlertExclamationIcon(String title, String link)
    {
        String elementFromBlockAlertsTabExclamationIcon = elementExclamationIcon(title);
        String elementFromBlockAlertsTabExclamationIcon2 = elementExclamationIcon2(link);
        this.waitForElementPresent((elementFromBlockAlertsTabExclamationIcon +
                elementFromBlockAlertsTabExclamationIcon2),"icon (!) not found!");
    }

    private static String elementGearIcon(String title) {
        return gearIconElement1.replace("{value}", title);
    }
    private static String elementGearIcon2(String link) {
        return gearIconElement2.replace("{value2}", link);
    }
    public void waitForGearIcon(String title, String link)
    {
        String elementFromBlockAlertsTabGearIcon = elementGearIcon(title);
        String elementFromBlockAlertsTabGearIcon2 = elementGearIcon2(link);
        this.waitForElementPresent((elementFromBlockAlertsTabGearIcon +
                elementFromBlockAlertsTabGearIcon2),"gear icon (*) not found!");
    }

    public void gearIconClick(String title, String link)
    {
        String elementFromBlockAlertsTabGearIcon = elementGearIcon(title);
        String elementFromBlockAlertsTabGearIcon2 = elementGearIcon2(link);
        this.waitForElementAndClick((elementFromBlockAlertsTabGearIcon +
                elementFromBlockAlertsTabGearIcon2),"gear icon (*) not found!");
    }
//Template methods





    public void selectAlwaysBlockRadioButton()
    {
        this.waitForElementAndClick(alwaysBlock, "'alwaysBlock' radiobutton not found!");
    }
    public void selectAlwaysAllowRadioButton()
    {
        this.waitForElementAndClick(alwaysAllow, "'alwaysAllow' radiobutton not found!");
    }

    public void initApplyClick()
    {
        this.waitForElementAndClick(applyButton, "'APPLY' button not found!");
    }

    public void waitForBlocksAlertsTabElementsPresent()
    {
        this.waitForElementPresent(elementsFromBlocksAlertsList,"elementsFromBlocksAlertsList not present!", 30);
    }


// ios methods

    public void waitForIOSAlertIconW()
    {
        this.waitForElementPresent(iOSAlertIconW, "iOSAlertIconW not found!");
    }




}
