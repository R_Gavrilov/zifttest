package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.RestrictionsPageObject;

public class AndroidRestrictionsPageObject extends RestrictionsPageObject {
    public AndroidRestrictionsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
                appSettingsElement = "id:com.contentwatch.ghoti.cp2.parent:id/appSettings";
                contentFilterElement = "id:com.contentwatch.ghoti.cp2.parent:id/categorySettings";
                urlSettingsElement= "id:com.contentwatch.ghoti.cp2.parent:id/urlSettings";
    }
}
