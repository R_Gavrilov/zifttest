package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.SubscriptionPageObject;

public class AndroidSubscriptionPageObject extends SubscriptionPageObject {
    public AndroidSubscriptionPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
                subscribeButton = "id:com.contentwatch.ghoti.cp2.parent:id/makeSubscription";
                subscribeButtonPopUp = "xpath://*[(@text='SUBSCRIBE') or (@text='BUY')]";
                inputField = "id:com.android.vending:id/input";
                unsubscribeButton = "id:com.contentwatch.ghoti.cp2.parent:id/cancelSubscription";
                cancelSubscriptionButton = "xpath://*[@text='CANCEL SUBSCRIPTION']";
                declineToAnswer = "xpath://android.widget.RadioButton[@text='Decline to answer']";
                continueButton = "xpath://android.widget.Button[@text='CONTINUE']";
                cancelSubscriptionButton2 = "xpath://android.widget.Button[@text='CANCEL SUBSCRIPTION']";
                restoreButton = "xpath://*[@text='RESTORE']";
                ziftPremiumElement = "xpath://*[@text='Test: Net Nanny Parental Control App']";
                termsOfUseElement = "id:com.contentwatch.ghoti.cp2.parent:id/termsOfUse";
                privacyPolicyElement = "id:com.contentwatch.ghoti.cp2.parent:id/privacyPolicy";
                updateElement = "xpath://*[@text='UPDATE']";
                seeCurrentSubscriptions = "xpath://*[@text='SEE CURRENT SUBSCRIPTIONS']";
    }
}
