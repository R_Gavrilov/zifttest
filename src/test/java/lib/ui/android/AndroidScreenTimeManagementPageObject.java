package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.ScreenTimeManagementPageObject;

public class AndroidScreenTimeManagementPageObject extends ScreenTimeManagementPageObject {
    public AndroidScreenTimeManagementPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
                unlimCheckBoxElement = "id:com.contentwatch.ghoti.cp2.parent:id/unlimited";
                screenTimeTodayLeftElement = "id:com.contentwatch.ghoti.cp2.parent:id/todayLeft";
                screenTimeTodayUsedElement = "id:com.contentwatch.ghoti.cp2.parent:id/todayUsed";
                screenTimeOverride = "id:com.contentwatch.ghoti.cp2.parent:id/text";
                plusMinusButtonTemplate = "id:com.contentwatch.ghoti.cp2.parent:id/%s";
                saturdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay6";
                mondayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay1";
                tuesdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay2";
                wednesdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay3";
                thursdayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay4";
                fridayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay5";
                sundayElement = "id:com.contentwatch.ghoti.cp2.parent:id/pickerDay0";
                minusButton = "id:com.contentwatch.ghoti.cp2.parent:id/minus";
                plusButton = "id:com.contentwatch.ghoti.cp2.parent:id/plus";
                exhaustedModeElement = "id:com.contentwatch.ghoti.cp2.parent:id/runsOutMode";
                noInternetExhaustedMode = "xpath://*[(@class='android.widget.TextView') and (@text='No Internet')]";
                pauseDeviceExhaustedMode = "xpath://*[(@class='android.widget.TextView') and (@text='Pause Device')]";
    }
}
