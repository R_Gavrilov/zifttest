package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.SearchesTabObject;

public class AndroidSearchesTabObject extends SearchesTabObject {
    public AndroidSearchesTabObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
                searchEvent = "xpath://*[@text='{value}']";
                loupeElement = "xpath://*[@text='{value}']/../..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/icon']";
    }
}
