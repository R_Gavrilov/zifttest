package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.CommonNavigationUI;

public class AndroidCommonNavigationUI extends CommonNavigationUI {
    public AndroidCommonNavigationUI(AppiumDriver driver) {
        super(driver);
    }

    static {
                someElement = "xpath://*[contains(@text,'{value}')]";
                someElementForContainsMethod1 = "xpath://*[contains(@text,'{value}')";
                someElementForContainsMethod2 = " and contains(@text,'{value2}')]";
                backElement = "id:com.contentwatch.ghoti.cp2.parent:id/back";
                ziftElementTextTemplate = "xpath://*[contains(@text,'{value}')]";
                blurDestination = "id:com.contentwatch.ghoti.cp2.parent:id/blurDestination";
                blurSeeMore = "id:com.contentwatch.ghoti.cp2.parent:id/blurSeeMore";
                keepZiftFreeButton = "xpath://*[(@text='KEEP ZIFT FREE') or (@content-desc='KEEP ZIFT FREE')]";
                calendarButton = "id:com.contentwatch.ghoti.cp2.parent:id/schedule";

                contentDescElement = "xpath://*[contains(@content-desc,'{value}')]";

                elementsInCalendarActionSheet =
                        "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/design_bottom_sheet']/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout";

                cellElement = "";
                templateCellElement = "xpath://*[@text='%s']";

                templateBlockButton = "xpath://*[@text='%s']/../../android.widget.TextView[@text='Block']";
                templateAllowButton = "xpath://*[@text='%s']/../../android.widget.TextView[@text='Allow']";
                blockXpath = "";
                allowXpath = "";

    }


}
