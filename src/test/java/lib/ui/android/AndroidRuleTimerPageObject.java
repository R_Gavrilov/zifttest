package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.RuleTimerPageObject;

public class AndroidRuleTimerPageObject extends RuleTimerPageObject {
    public AndroidRuleTimerPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
                selectRuleElement = "id:com.contentwatch.ghoti.cp2.parent:id/selectedRuleLayout";
                ruleTemplate = "xpath://*[@text='%s']";
                numPadButton1 = "xpath://*[@text='1']";
                numPadButton2 = "xpath://*[@text='2']";
                numPadButton0 = "xpath://*[@text='0']";
                selectRuleElement = "id:com.contentwatch.ghoti.cp2.parent:id/selectedRule";
                selectedTimeElement = "id:com.contentwatch.ghoti.cp2.parent:id/time";
                startTimerButton = "id:com.contentwatch.ghoti.cp2.parent:id/startTimer";
                ruleXpath = "";
    }
}
