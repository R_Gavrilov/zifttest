package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.ChildProfilePageObject;

public class AndroidChildProfilePageObject extends ChildProfilePageObject {
    public AndroidChildProfilePageObject(AppiumDriver driver) {
        super(driver);
    }

    static {
                avatarElement = "id:com.contentwatch.ghoti.cp2.parent:id/avatar";
                childNameElement = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/back']/following-sibling::android.widget.TextView";
                initGearElement = "xpath://*[@text='{value}']//..//../android.widget.ImageView";
                blockRadioButton = "xpath://*[@text='Block']";
                allowRadioButton = "xpath://*[@text='Allow']";
                okButtonElement = "xpath://android.widget.Button[@text='OK']";
                childSettingsElement = "id:com.contentwatch.ghoti.cp2.parent:id/childSettings";
                calendarButton = "id:com.contentwatch.ghoti.cp2.parent:id/schedule";
                editTimerRunningButton = "xpath://*[contains(@text,'Timer Running') or (@text='Run timer')]/following-sibling::*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/edit']";
                ruleSelectionMethodElement = "id:com.contentwatch.ghoti.cp2.parent:id/origination";
                currentRuleTitle = "id:com.contentwatch.ghoti.cp2.parent:id/currentRuleTitle";
                playPauseButton = "id:com.contentwatch.ghoti.cp2.parent:id/playPause";
                resumeStandardElement = "xpath://*[@text='Resume Standard']";
                playPauseNoInternet = "xpath://*[@text='No Internet']";
                calendarResumeScheduleElement = "xpath://*[@text='Resume Schedule']";
                initPlayPausePauseDeviceElement = "xpath://*[@text='Pause Device']";
                calendarActionSheetRunTimerElement = "xpath://*[contains(@text,'Timer Running') or contains(@text,'Run timer') or contains(@text,'Run timer (No Internet for 12:00)')]";
                ruleSelectionMethodElement2 = "xpath://*[(@resource-id='com.contentwatch.ghoti.cp2.parent:id/origination') and contains(@text, '%s')]";
                selectionCurrentRuleElement = "xpath://*[(@resource-id='com.contentwatch.ghoti.cp2.parent:id/currentRuleTitle') and contains(@text, '%s')]";
                screenTimeTodayLeftElement = "id:com.contentwatch.ghoti.cp2.parent:id/screenTimeLeft";
                screenTimeTodayUsedElement = "id:com.contentwatch.ghoti.cp2.parent:id/screenTimeUsed";

                blocksAlertsTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[3]";
                searchesTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[1]";
                appsTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[1]";
                screenTimeTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[2]";
                locationTab = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/tabsLayout']/android.widget.LinearLayout[4]";
                editScreenTime = "id:com.contentwatch.ghoti.cp2.parent:id/editScreenTime";

                elementsInPlayPauseActionSheet = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/design_bottom_sheet']/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout";
                selectionCurrentRuleElement2 = "";
                selectionMethodElement = "";
                selectedElementFromActionSheet = "";
                elementFromActionSheet = "xpath://*[@text='%s']";
                editScheduleRunningButton = "xpath://*[contains(@text,'Schedule Running') or (@text='Resume Schedule')]/following-sibling::*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/edit']";
    }
}
