package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.FamilyOverviewPageObject;

public class AndroidFamilyOverviewPageObject extends FamilyOverviewPageObject {
    public AndroidFamilyOverviewPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
                screenTimeLeftElement = "id:com.contentwatch.ghoti.cp2.parent:id/screenTimeLeft";
                avatarElement = "id:com.contentwatch.ghoti.cp2.parent:id/avatar";
                calendarButton = "id:com.contentwatch.ghoti.cp2.parent:id/schedule";
                playPauseButton = "id:com.contentwatch.ghoti.cp2.parent:id/playPause";
                tongueButtom = "id:com.contentwatch.ghoti.cp2.parent:id/tongueBottom";
                elementsInPlayPauseActionSheet = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/design_bottom_sheet']/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout";
    }
}
