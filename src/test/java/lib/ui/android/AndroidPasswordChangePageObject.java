package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.PasswordChangePageObject;

public class AndroidPasswordChangePageObject extends PasswordChangePageObject {
    public AndroidPasswordChangePageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
                currentPasswordField = "id:com.contentwatch.ghoti.cp2.parent:id/currentPassword";
                newPasswordField = "id:com.contentwatch.ghoti.cp2.parent:id/newPassword";
                repeatPasswordField = "id:com.contentwatch.ghoti.cp2.parent:id/repeatPassword";
                cancelButton = "id:com.contentwatch.ghoti.cp2.parent:id/cancel";
                doneButton = "id:com.contentwatch.ghoti.cp2.parent:id/done";
                errorElement = "id:com.contentwatch.ghoti.cp2.parent:id/error";
                currentPasswordClearElement = "id:com.contentwatch.ghoti.cp2.parent:id/currentClear";
                newPasswordClearElement = "id:com.contentwatch.ghoti.cp2.parent:id/newClear";
                repeatPasswordClearElement = "id:com.contentwatch.ghoti.cp2.parent:id/repeatClear";
    }
}
