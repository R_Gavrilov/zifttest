package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.BlocksAndAlertsObject;

public class AndroidBlocksAlertsPageObject extends BlocksAndAlertsObject {
    public AndroidBlocksAlertsPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static
    {
        iconW1 = "xpath://*[@text='{value}']";
        iconW2 = "/following-sibling::*[contains(@text, '{value2}')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/icon']";
        exclamationIconElement1 = "xpath://*[@text='{value}']";
        exclamationIconElement2 = "/following-sibling::*[contains(@text, '{value2}')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/mark']";
        gearIconElement1 = "xpath://*[@text='{value}']";
        gearIconElement2 = "/following-sibling::*[contains(@text, '{value2}')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/properties']";
        //*[@text='Viewed: Porn']/following-sibling::*[contains(@text, 'www.playboy.com')]//..//..//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/mark']
        gearIcon = "xpath://*[contains(@text,'www.fuq.com')]/../following-sibling::*[@resource-id= 'com.contentwatch.ghoti.cp2.parent:id/properties']";
        alwaysBlock = "id:com.contentwatch.ghoti.cp2.parent:id/alwaysBlock";
        alwaysAllow = "id:com.contentwatch.ghoti.cp2.parent:id/alwaysAllow";
        applyButton = "xpath://*[@text='APPLY']";
        elementsFromBlocksAlertsList = "xpath://android.support.v7.widget.RecyclerView[@resource-id='com.contentwatch.ghoti.cp2.parent:id/content']//android.widget.TextView";

    }


}
