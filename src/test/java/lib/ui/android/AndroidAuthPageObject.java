package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.AuthPageObject;

public class AndroidAuthPageObject extends AuthPageObject {
    public AndroidAuthPageObject(AppiumDriver driver) {
        super(driver);
    }

    static {
        termsOfServiceElement = "id:com.contentwatch.ghoti.cp2.parent:id/termsOfService";
        privacyPolicyElement = "id:com.contentwatch.ghoti.cp2.parent:id/privacyPolicy";
        getStartedButton = "xpath://*[@text='GET STARTED']";
        signInTabElement = "id:com.contentwatch.ghoti.cp2.parent:id/signInTab";
        emailFieldElement = "xpath://*[@text='Enter e-mail']";
        passwordFieldElement = "id:com.contentwatch.ghoti.cp2.parent:id/signInPassword";
        loginButtonElement = "id:com.contentwatch.ghoti.cp2.parent:id/signIn";
        forgotPasswordElement = "id:com.contentwatch.ghoti.cp2.parent:id/forgotPassword";
        textInEmailField = "xpath://*[(@resource-id='com.contentwatch.ghoti.cp2.parent:id/signInEmail') and (contains(@text,'@esc.tech') or contains(@text,'gmail.com'))]";
        textInPasswordField = "xpath://*[(@resource-id='com.contentwatch.ghoti.cp2.parent:id/signInPassword') and (@text='••••••')]";
        indicatorSpace = "id:com.contentwatch.ghoti.cp2.parent:id/indicatorSpace";
        errorMessageElement = "id:com.contentwatch.ghoti.cp2.parent:id/error";
        someElement = "xpath://*[contains(@text, 'CONTINUE') or contains(@text,'GOT IT!') or contains(@text, 'NO, THANKS')]";
    }

}

