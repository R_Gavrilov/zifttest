package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.SchedulePageObject;

public class AndroidSchedulePageObject extends SchedulePageObject {
    public AndroidSchedulePageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
        noInternetInterval = "id:com.contentwatch.ghoti.cp2.parent:id/addMode1";
        pauseDeviceInterval = "id:com.contentwatch.ghoti.cp2.parent:id/addMode2";
        timeFromPicker = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/timeFrom']/" +
                "*[(@class='android.widget.NumberPicker') and (@index='0')]/*[@resource-id='android:id/numberpicker_input']";
        endPicker12Hours = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/timeTo']/" +
                "*[(@class='android.widget.NumberPicker') and (@index='0')]/*[(@resource-id='android:id/numberpicker_input') and (@text='12')]";
        pickerApply = "id:com.contentwatch.ghoti.cp2.parent:id/editorApply";
    }

}
