package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.AppSettingsPageObject;

public class AndroidAppSettingsPageObject extends AppSettingsPageObject {
    public AndroidAppSettingsPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
                switchTabInstalledElement = "id:com.contentwatch.ghoti.cp2.parent:id/tabInstalled";
                tabAlphabetically = "id:com.contentwatch.ghoti.cp2.parent:id/tabAlphabetically";
                tabInstallDate = "id:com.contentwatch.ghoti.cp2.parent:id/tabInstallDate";
                tabAll = "id:com.contentwatch.ghoti.cp2.parent:id/tabAll";
                tabInstalled = "id:com.contentwatch.ghoti.cp2.parent:id/tabInstalled";
                tabAndroid = "id:com.contentwatch.ghoti.cp2.parent:id/tabAndroid";
                tabIos = "id:com.contentwatch.ghoti.cp2.parent:id/tabIos";





    }
}
