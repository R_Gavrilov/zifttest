package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.FeedPageObject;

public class AndroidFeedPageObject extends FeedPageObject {
    public AndroidFeedPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
                tongueTopElement = "id:com.contentwatch.ghoti.cp2.parent:id/tongueTop";
                childProfileElement = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/toolbarChildren']//android.view.View";
                EllipsesMenuButton = "id:com.contentwatch.ghoti.cp2.parent:id/menu";
                generalSettingsElement = "xpath://*[@text='General Settings']";
                subscriptionElement = "id:com.contentwatch.ghoti.cp2.parent:id/subscription";
                appAdvisorElement = "id:com.contentwatch.ghoti.cp2.parent:id/appAdvisor";
                helpElement = "xpath://*[@text='Help']";
                aboutElement = "xpath://*[@text='About']";
                parentPortalElement = "xpath://android.widget.TextView[@text='Parent Portal']";
                changePasswordElement = "id:com.contentwatch.ghoti.cp2.parent:id/changePassword";
                logoutElement = "id:com.contentwatch.ghoti.cp2.parent:id/logout";
                ziftFamilyFeedLabel = "xpath://*[@text='Zift Family Feed']";
                feedList = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/list']/android.widget.LinearLayout";

    }
}
