package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.ContentFilterPageObject;

public class AndroidContentFilterPageObject extends ContentFilterPageObject {
    public AndroidContentFilterPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static
    {
                templateAllowButton = "xpath://*[@text='%s']/following-sibling::android.widget.TextView[@text='Allow']";
                templateBlockButton = "xpath://*[@text='%s']/following-sibling::android.widget.TextView[@text='Block']";
                templateAlertButton = "xpath://*[@text='%s']/following-sibling::android.widget.TextView[@text='Alert']";
                howItWorksLink = "id:com.contentwatch.ghoti.cp2.parent:id/howItWorks";
                closeAlertElement = "id:com.contentwatch.ghoti.cp2.parent:id/close";
        allowXpath = "";
        blockXpath = "";
        alertXpath = "";

    }
}
