package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.ProfileSettingsPageObject;

public class AndroidProfileSettingsPageObject extends ProfileSettingsPageObject {
    public AndroidProfileSettingsPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    static {
                femaleGenderButton = "id:com.contentwatch.ghoti.cp2.parent:id/sexFemale";
                textField = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/avatar']/following-sibling::android.widget.EditText";
                doneButtonElement = "id:com.contentwatch.ghoti.cp2.parent:id/done";
                selectedGenderElement = "xpath://*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/sexMale']/../android.widget.Button[@selected='true']";
                maleGenderButton = "id:com.contentwatch.ghoti.cp2.parent:id/sexMale";
                ageButton = "id:com.contentwatch.ghoti.cp2.parent:id/age";
                elementFromDropDownList_TPL = "xpath://*[@text='{value}']";
                avatarElement = "id:com.contentwatch.ghoti.cp2.parent:id/avatar";
                elementFromDropDownListWithScroll_TPL = "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector().text(\"{value2}\"))";
    }
}




