package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import lib.Platform;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

abstract public class ContentFilterPageObject extends MainPageObject {

    public ContentFilterPageObject(AppiumDriver driver)
    {
        super(driver);
    }


    protected static  String

            templateAllowButton,
            templateBlockButton,
            templateAlertButton,
            howItWorksLink,
            allowXpath,
            blockXpath,
            alertXpath,
            closeAlertElement,
            weaponsElement,
            weaponsBlockElement;



/*    public void swipeToWeapons()
    {
        boolean elementVisibility = this.isElementLocatedOnTheScreen(weaponsElement);

        if(Platform.getInstance().isAndroid()) {
            System.out.println("test");
        } else {
            if (elementVisibility == true){
                this.swipeUpTillElementAppear(weaponsElement, "", 60);
            }
            else {
                System.out.println("weaponsElement is on the screen");
            }

        }


       public void swipeToWeapons()
        {
            boolean elementVisibility = this.isElementLocatedOnTheScreen(weaponsElement);
            System.out.println(elementVisibility);
            System.out.println(weaponsElement);
            this.swipeUpTillElementAppear(weaponsElement, "", 5);
         }  */

    public void swipeToWeapons() throws Exception
    {
        Thread.sleep(3000);
        MobileElement el = (MobileElement) driver.findElementByAccessibilityId("Weapons");
        this.scrollToDirection_iOS_XCTest(el, "u");
    }

//    public void swipe() throws InterruptedException {
//        Dimension size = driver.manage().window().getSize();
//        int startx = (int) (size.width * 0.90);
//        int endx = (int) (size.width * 0.10);
//        int starty = size.height / 2;
//        driver.swipe(startx, starty, endx, starty, 1000);
//         Thread.sleep(1500);
//    }






    public WebElement initSelectedBlockElement()
    {
        return this.waitForElementPresent
                (blockXpath, "'Block' element not found!", 20);
    }
    public String getBlockAttributeValue()
    {
        WebElement selectedElement = initSelectedBlockElement();
        return selectedElement.getAttribute("selected");
    }



    public void switchToBlock(String value)
    {
        if (Platform.getInstance().isAndroid()) {
            String s = String.format(templateBlockButton, value);
            blockXpath = s;
            this.waitForElementAndClick(blockXpath, "'block' button not found!", 20);
        } else {
            this.waitForElementAndClick(weaponsBlockElement, "", 30);
        }
    }

    public boolean waitForSelectedAttributeChangeToTrueAllowButton(String element)
    {
        String s = String.format(templateAllowButton, element);
        allowXpath = s;
        return this.waitForSelectedAttributeElementTrue(allowXpath, "attribute 'Selected' != true (allowButton)", 20);
    }
    public boolean waitForSelectedAttributeChangeToTrueBlockButton(String element)
    {
        String s = String.format(templateBlockButton, element);
        blockXpath = s;
        return this.waitForSelectedAttributeElementTrue(blockXpath, "attribute 'Selected' != true (blockButton)", 20);
    }

    public boolean waitForSelectedAttributeChangeToTrueAlertButton(String element)
    {
        String s = String.format(templateAlertButton, element);
        alertXpath = s;
        return this.waitForSelectedAttributeElementTrue(alertXpath, "attribute 'Selected' != true (alertButton)", 20);
    }

    public boolean waitUntilBlockButtonEnabled(String element)
    {
        String s = String.format(templateBlockButton, element);
        blockXpath = s;
        Boolean enabled = this.waitForElementAttributeEnabledTrue(
                blockXpath,  element + " element attribute not enabled", 20);
        System.out.println("element with locator " + blockXpath + " enabled? : " + enabled);
        return enabled;
    }





    public void switchToAllow(String value)
    {
        String s = String.format(templateAllowButton, value);
        allowXpath = s;
        this.waitForElementAndClick(allowXpath, "'allow' button not found!", 20);
    }


    public WebElement initSelectedAlertElement()
    {
        return this.waitForElementPresent
                (alertXpath, "'alert' element not found!", 20);
    }
    public String getAlertAttributeValue()
    {
        WebElement selectedElement = initSelectedAlertElement();
        return selectedElement.getAttribute("selected");
    }


    public void switchToAlert(String value)
    {
        String s = String.format(templateAlertButton, value);
        alertXpath = s;
        this.waitForElementAndClick(alertXpath, "'alert' button not found!", 20);
    }

    public void initHowItWorksLinkClick()
    {
        this.waitForElementAndClick(howItWorksLink, "'howItWorksLink' not found!");
    }

    public void initCloseAlert()
    {
        this.waitForElementAndClick(closeAlertElement, "'closeAlertElement' not found!");
    }











}
//*[@text='Abortion']/following-sibling::android.widget.TextView[@text='Alert']
