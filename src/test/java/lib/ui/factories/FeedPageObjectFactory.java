package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.FeedPageObject;
import lib.ui.android.AndroidFeedPageObject;
import lib.ui.iOS.iOSFeedPageObject;

public class FeedPageObjectFactory {
    public static FeedPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidFeedPageObject(driver);
        } else {
            return new iOSFeedPageObject(driver);
        }
    }
}
