package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.AppSettingsPageObject;
import lib.ui.android.AndroidAppSettingsPageObject;
import lib.ui.iOS.iOSAppSettingsPageObject;

public class AppSettingsPageObjectFactory {
    public static AppSettingsPageObject get(AppiumDriver driver) {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidAppSettingsPageObject(driver);
        } else {
            return new iOSAppSettingsPageObject(driver);
        }
    }
}
