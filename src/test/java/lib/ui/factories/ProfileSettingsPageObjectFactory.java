package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ProfileSettingsPageObject;
import lib.ui.android.AndroidProfileSettingsPageObject;
import lib.ui.iOS.iOSProfileSettingsPageObject;

public class ProfileSettingsPageObjectFactory {
    public static ProfileSettingsPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidProfileSettingsPageObject(driver);
        } else {
            return new iOSProfileSettingsPageObject(driver);
        }
    }
}
