package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.CommonNavigationUI;
import lib.ui.android.AndroidCommonNavigationUI;
import lib.ui.iOS.iOSCommonNavigationUI;

public class CommonNavigationUiFactory {
    public static CommonNavigationUI get(AppiumDriver driver) {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidCommonNavigationUI(driver);
        } else {
            return new iOSCommonNavigationUI(driver);
        }
    }
}
