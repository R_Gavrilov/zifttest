package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.SearchesTabObject;
import lib.ui.android.AndroidSearchesTabObject;
import lib.ui.iOS.iOSSearchesTabObject;

public class SearchesTabObjectFactory {
    public static SearchesTabObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidSearchesTabObject(driver);
        } else {
            return new iOSSearchesTabObject(driver);
        }
    }
}
