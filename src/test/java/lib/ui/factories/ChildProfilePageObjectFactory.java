package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ChildProfilePageObject;
import lib.ui.android.AndroidChildProfilePageObject;
import lib.ui.iOS.iOSChildProfilePageObject;

public class ChildProfilePageObjectFactory {
    public static ChildProfilePageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidChildProfilePageObject(driver);
        } else {
            return new iOSChildProfilePageObject(driver);
        }
    }
}
