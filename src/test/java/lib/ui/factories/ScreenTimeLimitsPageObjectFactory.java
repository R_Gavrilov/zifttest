package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ScreenTimeLimitsPageObject;
import lib.ui.android.AndroidScreenTimeLimitsPageObject;
import lib.ui.iOS.iOSScreenTimeLimitsPageObject;

public class ScreenTimeLimitsPageObjectFactory {
    public static ScreenTimeLimitsPageObject get (AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidScreenTimeLimitsPageObject(driver);
        } else {
            return new iOSScreenTimeLimitsPageObject(driver);
        }
    }
}
