package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.SchedulePageObject;
import lib.ui.android.AndroidSchedulePageObject;
import lib.ui.iOS.iOSSchedulePageObject;

public class SchedulePageObjectFactory {
    public static SchedulePageObject get (AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidSchedulePageObject(driver);
        } else {
            return new iOSSchedulePageObject(driver);
        }
    }
}
