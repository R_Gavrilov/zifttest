package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ScreenTimeManagementPageObject;
import lib.ui.android.AndroidScreenTimeManagementPageObject;
import lib.ui.iOS.iOSScreenTimeManagementPageObject;

public class ScreenTimeManagementPageObjectFactory {
    public static ScreenTimeManagementPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidScreenTimeManagementPageObject(driver);
        } else {
            return new iOSScreenTimeManagementPageObject(driver);
        }
    }
}
