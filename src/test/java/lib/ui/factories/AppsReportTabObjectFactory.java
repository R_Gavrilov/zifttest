package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.AppsReportTabObject;
import lib.ui.android.AndroidAppsReportTabObject;
import lib.ui.iOS.iOSAppsReportTabObject;

public class AppsReportTabObjectFactory {
    public static AppsReportTabObject get(AppiumDriver driver) {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidAppsReportTabObject(driver);
        } else {
            return new iOSAppsReportTabObject(driver);
        }
    }
}
