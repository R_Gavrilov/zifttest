package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.RuleTimerPageObject;
import lib.ui.android.AndroidRuleTimerPageObject;
import lib.ui.iOS.iOSRuleTimerPageObject;

public class RuleTimerPageObjectFactory {
    public static RuleTimerPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidRuleTimerPageObject(driver);
        } else {
            return new iOSRuleTimerPageObject(driver);
        }
    }
}
