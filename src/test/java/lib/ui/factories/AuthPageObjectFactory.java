package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.AuthPageObject;
import lib.ui.android.AndroidAuthPageObject;
import lib.ui.iOS.iOSAuthPageObject;

public class AuthPageObjectFactory {
    public static AuthPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidAuthPageObject(driver);
        } else {
            return new iOSAuthPageObject(driver);
        }
    }
}
