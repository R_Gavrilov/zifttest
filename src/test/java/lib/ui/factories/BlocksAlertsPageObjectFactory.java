package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.BlocksAndAlertsObject;
import lib.ui.android.AndroidBlocksAlertsPageObject;
import lib.ui.iOS.iOSBlocksAlertsPageObject;

public class BlocksAlertsPageObjectFactory {

    public static BlocksAndAlertsObject get (AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidBlocksAlertsPageObject(driver);
        } else {
            return new iOSBlocksAlertsPageObject(driver);
        }
    }
}
