package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.RestrictionsPageObject;
import lib.ui.android.AndroidRestrictionsPageObject;
import lib.ui.iOS.iOSRestrictionsPageObject;

public class RestrictionsPageObjectFactory {
    public static RestrictionsPageObject get(AppiumDriver driver)
    {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidRestrictionsPageObject(driver);
        } else {
            return new iOSRestrictionsPageObject(driver);
        }
    }
    }

