package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.WebsiteSettingsPageObject;
import lib.ui.android.AndroidWebsiteSettingsPageObject;
import lib.ui.iOS.iOSWebsiteSettingsPageObject;

public class WebsiteSettingsPageObjectFactory {
    public static WebsiteSettingsPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidWebsiteSettingsPageObject(driver);
        } else {
            return new iOSWebsiteSettingsPageObject(driver);
        }
    }
}
