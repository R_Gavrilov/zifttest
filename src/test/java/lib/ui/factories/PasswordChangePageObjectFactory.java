package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.PasswordChangePageObject;
import lib.ui.android.AndroidPasswordChangePageObject;
import lib.ui.iOS.iOSPasswordChangePageObject;

public class PasswordChangePageObjectFactory {
    public static PasswordChangePageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidPasswordChangePageObject(driver);
        } else {
            return new iOSPasswordChangePageObject(driver);
        }
    }
}
