package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.SubscriptionPageObject;
import lib.ui.android.AndroidSubscriptionPageObject;
import lib.ui.iOS.iOSSubscriptionPageObject;

public class SubscriptionPageObjectFactory {
    public static SubscriptionPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidSubscriptionPageObject(driver);
        } else {
            return new iOSSubscriptionPageObject(driver);
        }
    }
}
