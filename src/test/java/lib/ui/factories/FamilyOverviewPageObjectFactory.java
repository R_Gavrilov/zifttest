package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.FamilyOverviewPageObject;
import lib.ui.android.AndroidFamilyOverviewPageObject;
import lib.ui.iOS.iOSFamilyOverviewPageObject;

public class FamilyOverviewPageObjectFactory {
    public static FamilyOverviewPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidFamilyOverviewPageObject(driver);
        } else {
            return new iOSFamilyOverviewPageObject(driver);
        }
    }

}
