package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ContentFilterPageObject;
import lib.ui.android.AndroidContentFilterPageObject;
import lib.ui.iOS.iOSContentFilterPageObject;

public class ContentFilterPageObjectFactory {
    public static ContentFilterPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidContentFilterPageObject(driver);
        } else {
            return new iOSContentFilterPageObject(driver);
        }
    }

}
