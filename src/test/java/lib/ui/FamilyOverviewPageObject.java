package lib.ui;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

abstract public class FamilyOverviewPageObject extends MainPageObject {

    public FamilyOverviewPageObject(AppiumDriver driver) {
        super(driver);
    }

    protected static  String

            screenTimeLeftElement,
            avatarElement,
            calendarButton,
            playPauseButton,
            tongueButtom,
            elementsInPlayPauseActionSheet;


    public WebElement initScreenTimeLeftValueElement()
    {
        return this.waitForElementPresent(screenTimeLeftElement, "ScreenTimeLeftValueElement not present!", 20);
    }

    public String getScreenTimeLeftElementText()
    {
        WebElement screenTimeLeftElementText = initScreenTimeLeftValueElement();
        return screenTimeLeftElementText.getAttribute("text");
    }

    public void initAvatarClick()
    {
        this.waitForElementAndClick((avatarElement), "avatarElement not found!", 20);
    }

    //testFeatureAvailabilityFreePremium

    public void initCalendarClick()
    {
        this.waitForElementAndClick(calendarButton, "'schedule' not found!");
    }



    public void initPlayPauseClick()
    {
        this.waitForElementAndClick((playPauseButton), "playPause button not found!");
    }

    public int amountOfElementsInPlayPauseActionSheet() {
        return this.getAmountOfElements(elementsInPlayPauseActionSheet);
    }


    public void initTongueButtomClick()
    {
        this.waitForElementAndClick((tongueButtom), "'tongueButtom' not found!");
    }







/*    public WebElement waitForChildNameElement()
    {
        return this.waitForElementPresentByText
                (By.xpath(childNameElement), "cannot find element with child name", 20);
    }
    public String getNewChildName()
    {
        WebElement nameElement = waitForChildNameElement();
        return  nameElement.getAttribute("text");
    }*/
}
