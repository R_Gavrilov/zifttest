package lib.ui;

import io.appium.java_client.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.time.LocalDate;

import static io.appium.java_client.touch.offset.PointOption.point;

abstract public class SchedulePageObject extends MainPageObject {
    public SchedulePageObject(AppiumDriver driver) {
        super(driver);
    }
    protected static String
            noInternetInterval,
            pauseDeviceInterval,
            timeFromPicker,
            endPicker12Hours,
            pickerApply;



    public void setTimeBlockTo24Hours(Duration timeOfSwipe) {
        TouchAction action = new TouchAction(driver);
        int x = 661;
        int start_y =  1454;
        int end_y = 1603;
        action.press(point(x, start_y)).waitAction(WaitOptions.waitOptions(timeOfSwipe)).moveTo(point(x, end_y)).release().perform();
        this.waitForElementVisibility(endPicker12Hours, "endPicker12Hours not present", 10);
        this.waitForElementAndClick(pickerApply,"pickerApply button not present", 10);
    }

    public void deleteTimeBlock() throws Exception {
        Thread.sleep(1000);
        TouchAction action = new TouchAction(driver);
        action.tap(point(1035, 521)).perform();
        Thread.sleep(1000);
    }


    public void goToTheCurrentDaySchedule() {
        String currentDay = LocalDate
                .now()
                .getDayOfWeek()
                .name();
        System.out.println(currentDay);
        TouchAction action = new TouchAction(driver);
        if (currentDay.equals("MONDAY")) {
            action.tap(point(335,462)).release().perform();
        } else if (currentDay.equals("TUESDAY")) {
            action.tap(point(469,462)).release().perform();
        } else if (currentDay.equals("WEDNESDAY")) {
            action.tap(point(598,462)).release().perform();
        } else if (currentDay.equals("THURSDAY")) {
            action.tap(point(732,462)).release().perform();
        } else if (currentDay.equals("FRIDAY")) {
            action.tap(point(862,462)).release().perform();
        } else if (currentDay.equals("SATURDAY")) {
            action.tap(point(996,462)).release().perform();
        } else if (currentDay.equals("SUNDAY")) {
            action.tap(point(205,462)).release().perform();
        }
    }

    public void addTimeBlockNoInternet() throws Exception {
        this.waitForElementAndClick(noInternetInterval, "noInternetInterval not present", 15);
        Thread.sleep(5000);
    }

    public void testScroll() {
        WebElement rt = driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).className(android.widget.NumberPicker).index(0))" +
                        ".scrollIntoView(new UiSelector().text(\"5\"))"));
    }

    public void testScroll1() {
        MobileElement el = ((AndroidDriver<MobileElement>)driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).className(android.widget.NumberPicker).index(0))" +
                ".scrollIntoView(new UiSelector().text(\"5\"))");
        el.click();
        System.out.println(el.getText());

    }

    public void testScroll2() {
        WebElement el = this.waitForElementVisibility(endPicker12Hours, "", 10);
        el.clear();
        el.sendKeys("12");
        el.click();

        System.out.println(el.getText());
        this.waitForElementAndClick(pickerApply,"", 10);
    }










    }
