package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import lib.Platform;
import org.openqa.selenium.WebElement;

abstract public class ProfileSettingsPageObject extends MainPageObject {
    public ProfileSettingsPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static  String

            femaleGenderButton,
            textField,
            doneButtonElement,
            selectedGenderElement,
            maleGenderButton,
            ageButton ,
            elementFromDropDownList_TPL,
            avatarElement ,
            elementFromDropDownListWithScroll_TPL,
            iosPicker,
            pickerDoneButton;



    /*TEMPLATES METHODS*/
    private static String selectElementFromDropDownList(String value)
    {
        return elementFromDropDownList_TPL.replace("{value}", value);
    }

    private static String selectElementFromDropDownList2(String value2)
    {
        return elementFromDropDownListWithScroll_TPL.replace("{value2}", value2);
    }

    public void tapOnElementFromDropdownListWithScroll(String value2)
    {
        if(Platform.getInstance().isAndroid()) {
            String elementFromDropDownListUIAutomatorText = selectElementFromDropDownList2(value2);
            driver.findElement(MobileBy.AndroidUIAutomator(elementFromDropDownListUIAutomatorText)).click();
            this.waitChildAvatarElement();
        } else {
            WebElement picker = this.waitForElementPresent(iosPicker, "iosPicker not present", 30);
            picker.sendKeys(value2);
            waitForElementAndClick(pickerDoneButton, "doneButton from picker not found", 30);
        }
    }

    public void tapOnElementFromDropdownList(String value)
    {
        if(Platform.getInstance().isAndroid()) {
            String elementFromDropDownListXpath = selectElementFromDropDownList(value);
            waitForElementAndClick(elementFromDropDownListXpath, value + "button not found!");
            this.waitChildAvatarElement();
        } else {
            WebElement picker = this.waitForElementPresent(iosPicker, "iosPicker not present", 30);
            picker.sendKeys(value);
            waitForElementAndClick(pickerDoneButton, "doneButton from picker not found", 30);
        }
    }






    /*TEMPLATES METHODS*/

    public void clearOldProfileName()
    {
        this.waitForElementAndClear(textField,
                "name of child don't clear", 20);
    }

    public void enterNewChildName(String newProfileName)
    {
            this.waitForElementAndSendKeys(textField, newProfileName,
                    "no value entered");
    }

    public void tapDoneButton()
    {
        this.waitForElementAndClick(doneButtonElement, "done button not found!");
    }

    public void tapGenderFButton()
    {
        this.waitForElementAndClick(femaleGenderButton,
                "sexFemale button not found!", 20);
    }

    public void waitChildAvatarElement()
    {
        this.waitForElementPresent(avatarElement,
                "avatar not visible");
    }

    public WebElement waitSelectedAgeElement()
    {
       return this.waitForElementPresent(ageButton,"'elementAge1' not found!");
    }

    public String getSelectedAgeElement()
    {
        if(Platform.getInstance().isAndroid()) {
            WebElement ageElement = waitSelectedAgeElement();
            return ageElement.getAttribute("text");
        } else {
            WebElement ageElement = waitSelectedAgeElement();
            return ageElement.getAttribute("value");
        }

    }


    public WebElement waitSelectedGenderElement()
    {
        return this.waitForElementPresent(selectedGenderElement,
                "'elementSexButtonTrue' not found!");
    }

    public String getSelectedGender()
    {
        WebElement nameElement = waitSelectedGenderElement();
        return nameElement.getAttribute("text");
    }
    public void tapGenderMButton()
    {
        this.waitForElementAndClick(maleGenderButton, "sexMale button not found!", 20);
    }

    public void tapEditAgeButton()
    {
        this.waitForElementAndClick(ageButton, "edit age button not found!");
    }

/*    public void tapOnElementFromDropdownList(String value)
    {
        String elementFromDropDownListXpath = selectElementFromDropDownList(value);
        waitForElementAndClick(By.xpath(elementFromDropDownListXpath), value + "button not found!");
    }*/






}
