package lib.ui;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

abstract public class AppsReportTabObject extends MainPageObject {
    public AppsReportTabObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

            appListElement,
            tabAlphabetically,
            elementsFromAppsList;


    public String getElementTextFromAppsList(int index)
    {
        List<WebElement> applicationList = driver.findElements
                (By.xpath("//*[@resource-id ='com.contentwatch.ghoti.cp2.parent:id/list']//*[@resource-id='com.contentwatch.ghoti.cp2.parent:id/title']"));
        return applicationList.get(index).getAttribute("text");
    }



//Template Methods
    private static String getElementFromAppList(String value) {
        return appListElement.replace("{value}", value);
    }
    public void swipeUpToElementNotPresentAccuracy(String value)
    {
        String elementFromAppListTab = getElementFromAppList(value);
        this.swipeUpToElementNotPresentAccuracy((elementFromAppListTab), "Swipe:" + elementFromAppListTab +  "is visible", 30);
    }






    public void switchToAlphabetically()
    {
        this.waitForElementAndClick(tabAlphabetically, "'Alphabetically' button not found!", 20);
    }

    public void waitForAppsTabElementsPresent()
    {
        this.waitForElementPresent(elementsFromAppsList,"elementsFromAppsList not present!");
    }











}
