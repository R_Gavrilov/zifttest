package lib.ui;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import org.openqa.selenium.WebElement;

abstract public class RuleTimerPageObject extends MainPageObject {

    public RuleTimerPageObject(AppiumDriver driver) {
        super(driver);
    }

    protected static String

            selectRuleElement,
            ruleTemplate,
            numPadButton1,
            numPadButton2,
            numPadButton0,
            selectedTimeElement,
            startTimerButton,
            ruleXpath;





    public void selectRule(String value) {
        String s = String.format(ruleTemplate, value);
        ruleXpath = s;
        this.waitForElementAndClick(ruleXpath, "rule button not found!", 30);
    }
/*  объединение в один метод

    public static void giveXpathRuleElement(String value) {
        String s = String.format(ruleTemplate, value);
        ruleXpath = s;
    }
    public void selectRule() {
        this.waitForElementAndClick(By.xpath(ruleXpath), "rule button not found!", 20);
    }*/


    public void initRuleSelection() {
        this.waitForElementAndClick(selectRuleElement, "'Rule Selection button' not found!'", 30);
    }

    public void enterValue12h00m() {
        this.waitForElementAndClick(numPadButton1, "'numPadButton1' not found!", 30);
        this.waitForElementAndClick(numPadButton2, "'numPadButton2' not found!", 30);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 30);
        this.waitForElementAndClick(numPadButton0, "'numPadButton0' not found!", 30);
    }


    public String getSelectedRuleText()
    {
         WebElement SelectedRuleElementEL = this.waitForElementPresent(
                    selectRuleElement, "selectedRuleElement not found!", 30);
        if(Platform.getInstance().isAndroid()) {
            return SelectedRuleElementEL.getAttribute("text");
        } else {
            return SelectedRuleElementEL.getAttribute("name");
        }

    }



    public String getSelectedTimeText()
    {
            WebElement selectedTimeElementEl = this.waitForElementPresent(
                    selectedTimeElement, "selectedTimeElement not found!", 30);
        if(Platform.getInstance().isAndroid()) {
            return selectedTimeElementEl.getAttribute("text");
        } else {
            return selectedTimeElementEl.getAttribute("name");
        }
    }

    public void initStartTimerClick()
    {
        this.waitForElementAndClick(startTimerButton, "startTimerButton not found!", 20);
    }






}
