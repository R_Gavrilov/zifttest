package lib.ui;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

abstract public class SearchesTabObject extends MainPageObject {

    public SearchesTabObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

        searchEvent,
        loupeElement;

        private By blurSeeMore = By.id("com.contentwatch.ghoti.cp2.parent:id/blurSeeMore");
        private By searchesList = By.id("com.contentwatch.ghoti.cp2.parent:id/list");


    private static String actualSearchEvent(String value)
    {
        return searchEvent.replace("{value}", value);
    }
    public void swipeToSearchEvent(String value)
    {
        String searchEventXpath = actualSearchEvent(value);
        this.swipeUpToFindElement(searchEventXpath, "desired search event not found!", 20);
    }

    private static String actualLoupeElement(String value)
    {
        return loupeElement.replace("{value}", value);
    }
    public WebElement waitForLoupeElementPresent(String value)
    {
        String actualLoupeElementXpath = actualLoupeElement(value);
        return this.waitForElementPresent(actualLoupeElementXpath, "'LoupeElement' not found!");
    }


    public WebElement elementWithIndex0()
    {
        return this.waitForElementPresent
                (("xpath://android.support.v7.widget.RecyclerView/android.widget.FrameLayout[@index='0']/android.widget.TextView"),
                        "'ElementFromSearchesTabListWithIndex0BeforeSwipe' not found!");
    }
    public String getElementTextWithIndex0()
    {
        return elementWithIndex0().getAttribute("text");
    }


    public WebElement elementWithIndex1()
    {
        return this.waitForElementPresent
                (("xpath://android.support.v7.widget.RecyclerView/android.widget.LinearLayout[@index='1']//android.widget.TextView[@index='0']"),
                        "'ElementFromSearchesTabListWithIndex1BeforeSwipe' not found!");
    }
    public String getElementTextWithIndex1()
    {
        return elementWithIndex1().getAttribute("text");
    }


    public WebElement elementWithIndex1DateDevice()
    {
        return this.waitForElementPresent
                (("xpath://android.support.v7.widget.RecyclerView/android.widget.LinearLayout[@index='1']//android.widget.TextView[@index='1']"),
                        "'ElementFromSearchesTabListWithIndex1BeforeSwipeDateDevice' not found!");
    }
    public String getElementTextWithIndex1DateDevice()
    {
        return elementWithIndex1DateDevice().getAttribute("text");
    }




    public WebElement elementWithIndex8()
    {
        return this.waitForElementPresent
                (("xpath://android.support.v7.widget.RecyclerView/android.widget.LinearLayout[@index='8']//android.widget.TextView[@index='0']"),
                        "'ElementFromSearchesTabListWithIndex1BeforeSwipe' not found!");
    }
    public String getElementTextWithIndex8AfterSwipe()
    {
        return elementWithIndex8().getAttribute("text");
    }


    public WebElement elementWithIndex6DateDevice()
    {
        return this.waitForElementPresent
                ("xpath://android.support.v7.widget.RecyclerView/android.widget.LinearLayout[@index='6']//android.widget.TextView[@index='1']",
                        "'ElementFromSearchesTabListWithIndex1BeforeSwipeDateDevice' not found!");
    }
    public String getElementTextWithIndex6DateDeviceAfterSwipe()
    {
        return elementWithIndex6DateDevice().getAttribute("text");
    }

/*  Удалить паходу! после доделывания теста на доступность функционала поатная/бесплатная версия

    public void waitForBlurSeeMorePresent()
    {
        this.waitForElementPresentByText((blurSeeMore), "'blurSeeMore' button not found!");
    }

    public WebElement searchesList()
    {
        return this.waitForElementPresentByText
                ((searchesList), "'searchesList' element not found!", 20);
    }
    public String getSearchesListFocusedAttributeValue()
    {
        WebElement selectedElement = searchesList();
        return selectedElement.getAttribute("focused");
    }*/


}
