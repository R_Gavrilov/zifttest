package lib.ui;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.factories.CommonNavigationUiFactory;
import lib.ui.factories.ScreenTimeManagementPageObjectFactory;
import org.junit.ComparisonFailure;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

abstract public class ChildProfilePageObject extends MainPageObject {

    public ChildProfilePageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String
            avatarElement,
            childNameElement,
            initGearElement,
            blockRadioButton,
            allowRadioButton,
            okButtonElement,
            childSettingsElement,
            calendarButton,
            editTimerRunningButton,
            ruleSelectionMethodElement,
            currentRuleTitle,
            playPauseButton,
            resumeStandardElement,
            playPauseNoInternet,
            calendarResumeScheduleElement,
            initPlayPausePauseDeviceElement,
            calendarActionSheetRunTimerElement,
            ruleSelectionMethodElement2,
            selectionCurrentRuleElement,
            screenTimeTodayLeftElement,
            screenTimeTodayUsedElement,


            blocksAlertsTab,
            searchesTab,
            appsTab,
            screenTimeTab,
            locationTab,
            editScreenTime,

            elementsInPlayPauseActionSheet,
            selectionCurrentRuleElement2,
            selectionMethodElement,

            elementFromActionSheet,
            selectedElementFromActionSheet,
            editScheduleRunningButton;



    public void waitForDataLoading() {
            this.waitForElementVisibility(screenTimeTodayUsedElement, "", 10);
    }


    public void setDefaultScreenTimeValue()
    {
        try{
            this.waitForElementVisibility(
                    screenTimeTodayLeftElement,
                    "'screenTimeTodayLeftElement'  not found!", 20);
            String screenTimeLeftText = this.getScreenTimeLeftElementText();
            assertEquals("we see unexpected text in screenTimeLeft",
                    "\u200BUnlimited", screenTimeLeftText);
        } catch (ComparisonFailure e) {
            this.editScreenTimeClick();
            ScreenTimeManagementPageObject ScreenTimeManagementPageObject = ScreenTimeManagementPageObjectFactory.get(driver);
            ScreenTimeManagementPageObject.turnSwitcherON();
            CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
            CommonNavigationUI.initBackClick();
        }
    }

    public String getHoursOfScreenTimeUsed()  {
        try {
            WebElement screenTimeUsedElement =
                    this.waitForElementPresent(screenTimeTodayUsedElement, "screenTimeTodayUsedElement not present!", 20);
            String screenTimeUsedElementText = screenTimeUsedElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]h|[0-9]h");
            Matcher matcher = pattern.matcher(screenTimeUsedElementText);
            matcher.find();
            String hoursOfScreenTimeUsed = matcher.group().replaceAll("\\D+", "");
            return hoursOfScreenTimeUsed;
        } catch (IllegalStateException e) {
            return null;
        }
    }
    public Integer convertHoursOfScreenTimeUsedToSeconds()  {

        try {
            Integer hours = Integer.parseInt(this.getHoursOfScreenTimeUsed());
            Integer hoursOfScreenTimeUsedToSeconds = hours * 3600;
            System.out.println("hoursOfScreenTimeUsedToSeconds: " + hoursOfScreenTimeUsedToSeconds);
            return hoursOfScreenTimeUsedToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("hoursOfScreenTimeUsed not found!");
            return 0;
        }
    }
    public String getMinutesOfScreenTimeUsed()  {
        try {
            WebElement screenTimeUsedElement = this.waitForElementPresent(screenTimeTodayUsedElement, "screenTimeTodayUsedElement not present!", 20);
            String screenTimeUsedElementText = screenTimeUsedElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]m|[0-9]m");
            Matcher matcher = pattern.matcher(screenTimeUsedElementText);
            matcher.find();
            String minutesOfScreenTimeUsed = matcher.group().replaceAll("\\D+", "");
            return minutesOfScreenTimeUsed;
        } catch (IllegalStateException e) {
            return null;
        }
    }
    public Integer convertMinutesOfScreenTimeUsedToSeconds()  {
        try {
            Integer minutes = Integer.parseInt(this.getMinutesOfScreenTimeUsed());
            Integer minutesOfScreenTimeUsedToSeconds = minutes * 60;
            System.out.println("minutesOfScreenTimeUsed in seconds: " + minutesOfScreenTimeUsedToSeconds);
            return minutesOfScreenTimeUsedToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("minutesOfScreenTimeUsed not found!");
            return 0;
        }
    }

    public Integer secondsOfScreenTimeUsed() throws NullPointerException {
        Integer secondsOfScreenTimeUsed =
                this.convertHoursOfScreenTimeUsedToSeconds() + this.convertMinutesOfScreenTimeUsedToSeconds();
        System.out.println("screenTimeUsed in seconds: " + secondsOfScreenTimeUsed);
        return secondsOfScreenTimeUsed;
    }


    public String getHoursOfScreenTimeLeft() throws IllegalStateException, TimeoutException {
        try {
            WebElement screenTimeLeftElement =
                    this.waitForElementPresent(screenTimeTodayLeftElement, "screenTimeTodayLeftElement not present!", 20);
            String screenTimeLeftElementText = screenTimeLeftElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]h|[0-9]h");
            Matcher matcher = pattern.matcher(screenTimeLeftElementText);
            matcher.find();
            String hoursOfScreenTimeLeft = matcher.group().replaceAll("\\D+", "");
            return hoursOfScreenTimeLeft;
        } catch (IllegalStateException e) {
            return null;
        } catch (TimeoutException e1) {
            return null;
        }
    }
    public Integer convertHoursOfScreenTimeLeftToSeconds()  {
        try {
            Integer hours = Integer.parseInt(this.getHoursOfScreenTimeLeft());
            Integer hoursOfScreenTimeLeftToSeconds = hours * 3600;
            System.out.println("hoursOfScreenTimeLeft in seconds: " + hoursOfScreenTimeLeftToSeconds);
            return hoursOfScreenTimeLeftToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("hoursOfScreenTimeLeft not found!");
            return 0;
        } catch (NullPointerException e2) {
            return 0;
        }
    }

    public String getMinutesOfScreenTimeLeft()  {
        try {
            WebElement screenTimeLeftElement =
                    this.waitForElementPresent(screenTimeTodayLeftElement, "screenTimeTodayLeftElement not present!", 20);
            String screenTimeLeftElementText = screenTimeLeftElement.getAttribute("text");
            Pattern pattern = Pattern.compile("[0-9][0-9]m|[0-9]m");
            Matcher matcher = pattern.matcher(screenTimeLeftElementText);
            matcher.find();
            String minutesOfScreenTimeLeft = matcher.group().replaceAll("\\D+", "");
            return minutesOfScreenTimeLeft;
        } catch (IllegalStateException e) {
            return null;
        } catch (TimeoutException e2) {
            return null;
        }
    }
    public Integer convertMinutesOfScreenTimeLeftToSeconds()  {
        try {
            Integer minutes = Integer.parseInt(this.getMinutesOfScreenTimeLeft());
            Integer minutesOfScreenTimeLeftToSeconds = minutes * 60;
            System.out.println("minutesOfScreenTimeLeft in seconds: " + minutesOfScreenTimeLeftToSeconds);
            return minutesOfScreenTimeLeftToSeconds;
        } catch (NumberFormatException e) {
            System.out.println("minutesOfScreenTimeLeft not found!");
            return 0;
        } catch (NullPointerException e2) {
            return 0;
        }
    }
    public Integer actualSecondsOfScreenTimeLeft() throws NullPointerException {
        Integer secondsOfScreenTimeLeft =
                this.convertHoursOfScreenTimeLeftToSeconds() + this.convertMinutesOfScreenTimeLeftToSeconds();
        System.out.println("screenTimeLeft in seconds : " + secondsOfScreenTimeLeft);
        return secondsOfScreenTimeLeft;
    }

    public void waitForSomeElementInActionSheet(String value)
    {
        String s = String.format(elementFromActionSheet, value);
        selectedElementFromActionSheet = s;
        this.waitForElementPresent(selectedElementFromActionSheet, "elementFromActionSheet not present", 20);
    }


    public void waitForSelectionMethodElementPresent(String value)
    {
        String s = String.format(ruleSelectionMethodElement2, value);
        selectionMethodElement = s;
        this.waitForElementPresent(selectionMethodElement,
                "ruleSelectionMethodElement not present! " + selectionMethodElement, 61);
    }


/*
    private static String selectionCurrentRuleElement2 = "";
*/
    public void waitForSelectionCurrentRule(String value)
    {
        String s = String.format(selectionCurrentRuleElement, value);
        selectionCurrentRuleElement2 = s;
        this.waitForElementPresent(selectionCurrentRuleElement2,
                "SelectionCurrentRuleElement not present! " + selectionCurrentRuleElement2, 61);
    }



    public void initAvatarClick()
    {
        this.waitForElementAndClick
                (avatarElement,
                        "Cannot find and click avatarElement",
                        20);
    }

    public void editScreenTimeClick()
    {
        this.waitForElementAndClick
                (editScreenTime, "'editScreenTime' button not found!", 20);
    }

    public WebElement waitForChildNameElement()
    {
        return this.waitForElementPresent
                (childNameElement, "cannot find element with child name", 20);
    }
    public String getNewChildName()
    {
        if (Platform.getInstance().isAndroid()) {
        WebElement nameElement = waitForChildNameElement();
        return nameElement.getAttribute("text");
    } else {
            WebElement nameElement = waitForChildNameElement();
            return nameElement.getAttribute("name");
        }
    }

    private static String selectGearElement(String value)
    {
        return initGearElement.replace("{value}", value);
    }
    public void initGearElementClick(String value)
    {
        String GearElementXpath = selectGearElement(value);
        waitForElementAndClick(GearElementXpath,"'gear element' not found!", 20);
    }

    public void initBlockRadioButtonSelect()
    {
        this.waitForElementAndClick
                (blockRadioButton, "'BlockRadioButton' not found!", 20);

    }

    public void initAllowRadioButtonSelect()
    {
        this.waitForElementAndClick
                (allowRadioButton, "'AllowRadioButton' not found!", 20);

    }
    public void confirmRadioButtonSelection()
    {
        this.waitForElementAndClick
                (okButtonElement, "'OK' button from allow/block dropdown not found!", 20);
    }

    public void initRestrictionsClick()
    {
        this.waitForElementAndClick(childSettingsElement, "'childSettings' button not found!", 20);
    }

    public void initCalendarClick()
    {
        this.waitForElementAndClick(calendarButton,
                "calendarButton not found!", 20);
    }

    public void editRunTimerClick()
    {
        this.waitForElementAndClick(editTimerRunningButton, "TimerRunning button not found!", 20);
    }


    public WebElement initRuleSelectionMethodElement()
    {
        return this.waitForElementPresent(ruleSelectionMethodElement,
                "ruleSelectionMethodElement not found!", 20);
    }
    public String getRuleSelectionMethodElementText()
    {
        WebElement RuleSelectionMethodElement = initRuleSelectionMethodElement();
        return RuleSelectionMethodElement.getAttribute("text");
    }

    public void editScheduleClick()
    {
        this.waitForElementAndClick(editScheduleRunningButton, "editScheduleRunningButton not present", 20);
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
        CommonNavigationUI.waitForElementPresentByTextContains("Child", "Schedule");
    }


    public WebElement initCurrentRuleTitleElement()
    {
        return this.waitForElementPresent(currentRuleTitle, "ruleSelectionMethodElement not found!", 20);
    }
    public String getCurrentRuleTitleElementText()
    {
        WebElement RuleSelectionMethodElement = initCurrentRuleTitleElement();
        return RuleSelectionMethodElement.getAttribute("text");
    }



    public String getCalendarActionSheetRunTimerElementText()
    {
        WebElement calendarActionSheetRunTimerElementText = this.waitForElementPresent(calendarActionSheetRunTimerElement,
                    "calendarActionSheetRunTimerElement not found!", 20);
        if(Platform.getInstance().isAndroid()) {
            return calendarActionSheetRunTimerElementText.getAttribute("text");
        } else {
            return calendarActionSheetRunTimerElementText.getAttribute("name");
        }
    }




    public void initPlayPauseClick()
    {
        this.waitForElementAndClick(playPauseButton,
                "playPauseButton not found!", 20);
    }
    public void initPlayPausePauseDeviceClick()
    {
        this.waitForElementAndClick(initPlayPausePauseDeviceElement, "playPausePauseDeviceElement not found!", 20);
    }

    public void initPlayPauseResumeStandardClick()
    {
        this.waitForElementAndClick(resumeStandardElement, "resumeStandardElement not found!");
    }

    public void initCalendarResumeScheduleClick()
    {
        this.waitForElementAndClick(calendarResumeScheduleElement, "CalendarResumeScheduleElement not found", 20);
    }

    public void initCalendarActionSheetRunTimerClick()
    {
        this.waitForElementAndClick(calendarActionSheetRunTimerElement,
                "calendarActionSheetRunTimerElement not found!", 20);
    }



    public WebElement initScreenTimeLeftValueElement()
    {
        return  this.waitForElementPresent(screenTimeTodayLeftElement, "ScreenTimeLeftValueElement not present!", 20);
    }

    public String getScreenTimeLeftElementText()
    {
        WebElement screenTimeLeftElementText = initScreenTimeLeftValueElement();
        return screenTimeLeftElementText.getAttribute("text");
    }

    public void switchToBlocksAlertsTab()
    {
        this.waitForElementAndClick(blocksAlertsTab, "'BlocksAndAlerts' tab icon not found!");
    }
    public void switchToSearchesTab()
    {
        this.waitForElementAndClick(searchesTab, "'SearchesTab button' not found!");
    }
    public void switchToAppsTab()
    {
        this.waitForElementAndClick(appsTab, "'appsTab button' not found!");
    }
    public void switchToScreenTimeTab()
    {
        this.waitForElementAndClick((screenTimeTab), "'screenTimeTab button' not found!");
    }
    public void switchToLocationTab()
    {
        this.waitForElementAndClick(locationTab, "'locationTab button' not found!");
    }


   public int amountOfElementsInPlayPauseActionSheet() {
        return this.getAmountOfElements(elementsInPlayPauseActionSheet);
    }
















}
